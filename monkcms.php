<?php
$version = '8.7.0';

if (isset($_GET['404'])) {
    header('HTTP/1.0 404 Not Found');
    exit;
}

$monkAlivePath = $_SERVER['DOCUMENT_ROOT'] . '/.monkalive';

// If the garbage collector is off, turn it on.
$gcProbability=ini_get('session.gc_probability');
if ($gcProbability==0) {
  ini_set('session.gc_probability',1);
  ini_set('session.gc_divisor',100);
}

# Don't bother with sessions on images.
if ($_SERVER['PHP_SELF'] != '/monkimage.php') {
  session_start();
}

  // RC4 is a registered trademark of the RSA Data Security Inc.
    // This is an implementation of the original algorithm.
    // note: this is a symetric cypher, use 'decrypt()' as 'encrypt()' they are the same
function decrypt($pwd, $data)
{
    $key[] = '';
    $box[] = '';
    $pwd_length = strlen($pwd);
    $data_length = strlen($data);
    for ($i = 0; $i < 256; $i++) {
        $key[$i] = ord($pwd[$i % $pwd_length]);
        $box[$i] = $i;
    }
    for ($j = $i = 0; $i < 256; $i++) {
        $j = ($j + $box[$i] + $key[$i]) % 256;
        $box[$i] ^= $box[$j];
        $box[$j] ^= $box[$i];
        $box[$i] ^= $box[$j];
    }
    for ($a = $j = $i = 0; $i < $data_length; $i++) {
        $a = ($a + 1) % 256;
        $j = ($j + $box[$a]) % 256;
        $box[$a] ^= $box[$j];
        $box[$j] ^= $box[$a];
        $box[$a] ^= $box[$j];
        $k = $box[(($box[$a] + $box[$j]) % 256)];
        $cipher .= chr(ord($data[$i]) ^ $k);
    }
    return $cipher;
}

//
// End session handling, RC4 code. POST support is at bottom of this file, other output may begin
//

define('MONKISOK', 0x00000001);
define('MONKLETS', 0x00000010);
define('MONKCODE', 0x00000100);
define('MONKPRIV', 0x00001000);
define('MONKDATE', 0x00010000);

$configVars = getConfig();

$TIMEZONE = empty($configVars['TIMEZONE']) ? 'UTC' : $configVars['TIMEZONE'];
$CACHE_ADAPTER = empty($configVars['CACHE_ADAPTER']) ? 'file' : $configVars['CACHE_ADAPTER'];

date_default_timezone_set($TIMEZONE);

$monkCmsCache = new MonkCmsCache(array('adapter' => $CACHE_ADAPTER));

if (isset($_GET['mcmsInfo']) && basename(__FILE__) == 'monkcms.php') {
  require_once 'monkimage.php';

  $cacheCheck = null;

  if ($configVars) {
    $cacheCheck = $monkCmsCache->isWorking() ? 'True' : 'False';
  }

  header('Content-Type: application/json');
  echo json_encode(array(
    'cacheAdapter'      => empty($configVars['CACHE_ADAPTER']) ? null : $configVars['CACHE_ADAPTER'],
    'cacheCheck'        => $cacheCheck,
    'cacheCount'        => $monkCmsCache->getCount(),
    'cacheSize'         => $monkCmsCache->getSize(),
    'imageCacheCheck'   => cacheCheck(),
    'imageCacheVersion' => IMAGE_CACHE_VERSION,
    'mcmsApiVersion'    => $version,
    'phpConfig'         => array(
      'allow_url_fopen' => (bool) ini_get('allow_url_fopen'),
      'sqlite3'         => in_array('sqlite3', get_loaded_extensions())
    ),
    'phpVersion'        => phpversion()
  ));
  exit;
}

if (isset($_GET['cacheexpire'])) {
  if ($_GET['cacheexpire'] > 0) {
    $expireTime = $_GET['cacheexpire'];
    $eventExpireTime = $expireTime;
  }
  else {
    // Delete cache files older than 30 days.
    $expireTime = 60 * 60 * 24 * 30;
    // Delete event cache files older than 2 days.
    $eventExpireTime = 60 * 60 * 24 * 2;
  }

  $monkCmsCache->delete(array('createdAt' => "{$expireTime} seconds ago"));
  $monkCmsCache->delete(array(
    'module'    => 'event',
    'createdAt' => "{$eventExpireTime} seconds ago"
  ));
  $monkCmsCache->compress();
  exit;
}

if (!empty($_GET['cacheupdate'])) {
  $key1 = $_GET['key1'];
  $key2 = isset($_GET['key2']) ? $_GET['key2'] : null;

  if ($key1 == '.alive') {
    delfile("{$monkAlivePath}/{$key2}");
    // In some cases, the username is encoded in the .alive file.
    $key2 = urlencode($key2);
    delfile("{$monkAlivePath}/{$key2}");
    exit;
  }

  $modules = explode('|', $key1);

  foreach ($modules as $module) {
    if (!$module) {
      continue;
    }

    if ($key2) {
      $finds = explode('|||', $key2);

      foreach ($finds as $find) {
        if (!$find) {
          continue;
        }

        $monkCmsCache->delete(array(
          'module' => $module,
          'find'   => $find
        ));
      }

      $monkCmsCache->delete(array(
        'module' => $module,
        'find'   => 'list'
      ));
    }
    else {
      $monkCmsCache->delete(array('module' => $module));
    }
  }

  $monkCmsCache->delete(array('hasMonklet' => true));
  $monkCmsCache->delete(array(
    'module' => 'page',
    'find'   => 'search-results'
  ));
  $monkCmsCache->delete(array('module' => 'search'));
  exit;
}

if (!empty($_GET['emptycart'])) {
  unset($_SESSION['monk_cart']['items']);
  $_SESSION['monk_cart']['sessid'] = session_id();
  if ($_GET['reload']) {
    header("Location:/");
    exit;
  }
}

if (!empty($_GET['cacheclear'])) {
  $monkCmsCache->delete();
  delStaleAliveFiles();
  exit;
}

// Remove unnecessary files.
if (!empty($_GET['filecleanup'])) {
  $monkConfig  = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'monkconfig.php';
  $monkCode    = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'monkcode.txt';
  $monkInstall = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'monkinstall.php';

  if (file_exists($monkConfig) && file_exists($monkCode)) {
    @unlink($monkCode);
  }

  if (file_exists($monkInstall)) {
    @unlink($monkInstall);
  }

  $files = array(
    basename($monkCode) => file_exists($monkCode),
    basename($monkInstall) => file_exists($monkInstall)
  );

  // Whether all files were removed.
  $success = !in_array(true, array_values($files));

  header('Content-Type: application/json');
  echo json_encode(array('success' => $success, 'files' => $files));
  exit;
}

$REF = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
$THISURI = $_SERVER['REQUEST_URI'];

$MONKCODE=$configVars['SITEID'];
$MONKACCESS=$configVars['ACCESS'];
$CMS_CODE=$configVars['CMSCODE'];
$CMS_TYPE=$configVars['CMSTYPE'];
$MONKHOME = empty($configVars['API']) ? 'https://api.monkcms.com/Clients' : $configVars['API'];
$MCMS_SITENAME=$configVars['MCMS_SITENAME'];
$EASY_EDIT=$configVars['EASY_EDIT'];
$CMS_URL=$configVars['CMS_URL'];
$SITE_SECRET=$configVars['SECRET'];

if (isset($_GET['l']) && $_GET['l'] == 'g') {
  setSession($_GET['e'],$_GET['ticket'],$_GET['i'], $_GET['tplid']);
}

if (isset($_GET['pt'])) {
  setPassphraseSession($_GET['pt']);
}

if (isset($_GET['checksession'])) {
  $testString = $configVars['SECRET'];
  if ($_GET['v'] == $testString) {
    echo inSession($_GET['username']);
  }
  exit;
}

if (!empty($_SESSION['session_ticket'])) {
  $LOGGEDIN=1;
  $MCMS_USERNAME=$_SESSION['session_username'];
  $MCMS_FIRSTNAME=$_SESSION['session_firstname'];
  $MCMS_LASTNAME=$_SESSION['session_lastname'];
  $MCMS_FULLNAME=$MCMS_FIRSTNAME.' '.$MCMS_LASTNAME;
  $MCMS_MID=$_SESSION['session_i'];
  $MCMS_LOGGEDIN=1;
}

// Logout
if (isset($_GET['m']) && $_GET['m'] == 'o') {
  clearSession($MCMS_USERNAME);
  header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"'); // P3P stuff for IE6
  $cmsUrl = getCmsUrl();
  $siteId = getSiteId();
  $redirect = urlencode("/{$_GET['logoutLocation']}");
  $monkIdLogOut = "{$cmsUrl}/Clients/monkIdLogOut.php?userId={$MCMS_MID}&siteId={$siteId}&redirect={$redirect}";
  header("Location: {$monkIdLogOut}");
  return 1;
  exit;
}

$orderType = isset($_GET['t']) ? $_GET['t'] : null;
$orderTypes = array('o', 'd', 'e');
if (in_array($orderType, $orderTypes)) {
  unset($_SESSION['monk_cart']['items']);
  header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"'); // P3P stuff for IE6

  if (isset($MCMS_USERNAME) && isset($_GET['orderid'])) {
    header("Location:/me/{$MCMS_USERNAME}/orders/{$_GET['orderid']}/?thanks=1&orderType={$orderType}");
  } else {
    header("Location:/");
  }

  return 1;
  exit;
}

if (!empty($PROTECTED)) {
  if (!$LOGGEDIN) {
    echo "<script>top.location='$BADREDIRECT';</script>";
    return 0;
  }
}

if ($_SERVER['PHP_SELF']=='/mcms_me.php') {
  if (!$_GET['viewuserslug']) {
    if ($LOGGEDIN) {
      header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"'); // P3P stuff for IE6
      header("Location:/me/$MCMS_USERNAME/");
      return 1;
      exit;
    }
  }
  else {
    $MCMS_VIEWUSER=ltrim($_GET['viewuserslug'],'/');
  }
}
if ($_SERVER['PHP_SELF']=='/mcms_group.php') {
  $MCMS_VIEWGROUP=ltrim($_GET['viewgroupslug'],'/');
}

  if (isset($_GET['exitDebug'])) {
    setcookie('mcmsUseCache', 0, time() - 3600, '/');
    setcookie('mcmsShowCacheStatus', 0, time() - 3600, '/');
    setcookie('mcmsShowLoadTimes', 0, time() - 3600, '/');
    unset($devTools);
  }
  if (isset($_GET['disableCache'])) {
    setcookie('mcmsUseCache', 0, time() + 31536000, '/');
    $devTools['useCache'] = false;
  }
  if (isset($_GET['forceCache'])) {
    setcookie('mcmsUseCache', 1, time() + 31536000, '/');
    $devTools['useCache'] = true;
  }
  if (isset($_GET['showCacheStatus'])) {
    setcookie('mcmsShowCacheStatus', 1, time() + 31536000, '/');
    $devTools['showCacheStatus'] = true;
  }
  if (isset($_GET['showLoadTimes'])) {
    setcookie('mcmsShowLoadTimes', 1, time() + 31536000, '/');
    $devTools['showLoadTimes'] = true;
  }
  if (isset($_COOKIE['mcmsUseCache']) && !isset($_GET['exitDebug'])) {
    $devTools['useCache'] = $_COOKIE['mcmsUseCache'];
  }
  if (isset($_COOKIE['mcmsShowCacheStatus']) && !isset($_GET['exitDebug'])) {
    $devTools['showCacheStatus'] = true;
  }
  if (isset($_COOKIE['mcmsShowLoadTimes']) && !isset($_GET['exitDebug'])) {
    $devTools['showLoadTimes'] = true;
  }

  if (!empty($_GET['wildcard']) && count(explode('/', $_GET['wildcard'])) > 9) {
    header('HTTP/1.0 404 Not Found');
    echo "There seems to be a problem with this page.";
    $key = "api:wildcard_too_long";
    $value = $_GET['wildcard'];
    $wildcardLogUrl = $CMS_URL . "/Clients/logFromClientSite.php?siteId=" . $MONKCODE . "&siteSecret=" . $SITE_SECRET . "&key=" . $key . "&value=" . $value;
    if ($MONKACCESS=='readfile') {
      $response = file_get_contents($wildcardLogUrl);
    }
    elseif ($MONKACCESS=='cURL') {
      $ch = curl_init();
      $timeout = 5; // set to zero for no timeout
      curl_setopt($ch, CURLOPT_URL, $wildcardLogUrl);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
      $response = curl_exec($ch);
      curl_close($ch);
    }
    echo $response;
    exit;
  }

$MONKQUERY = http_build_query(array(
  'mid'           => isset($_SESSION['session_i']) ? $_SESSION['session_i'] : '',
  'pp'            => isset($_SESSION['session_p']) ? $_SESSION['session_p'] : '',
  'thisPage'      => isset($_GET['errorDocument']) ? $_GET['errorDocument'] : $THISURI,
  'CMSTYPE'       => $CMS_TYPE,
  'CMSCODE'       => $CMS_CODE,
  'SITEID'        => $MONKCODE,
  'nav'           => isset($_GET['nav']) ? $_GET['nav'] : '',
  'wildcard'      => isset($_GET['wildcard']) ? $_GET['wildcard'] : '',
  'clickpath'     => isset($_GET['clickpath']) ? $_GET['clickpath'] : '',
  'thanks'        => isset($_GET['thanks']) ? $_GET['thanks'] : '',
  'thanksForm'    => isset($_GET['thanksForm']) ? $_GET['thanksForm'] : '',
  'sessionticket' => isset($_SESSION['session_ticket']) ? $_SESSION['session_ticket'] : '',
  'username'      => isset($_SESSION['session_username']) ? $_SESSION['session_username'] : '',
  'vu'            => isset($MCMS_VIEWUSER) ? $MCMS_VIEWUSER : '',
  'sessid'        => isset($_GET['sessid']) ? $_GET['sessid'] : '',
  'sr'            => isset($_GET['show_results']) ? $_GET['show_results'] : '',
  'kw'            => isset($_GET['keywords']) ? $_GET['keywords'] : '',
  'page'          => isset($_GET['page']) ? $_GET['page'] : '',
  'rest'          => isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : ''
));


////////////////////////////////////////////////////////////////////////////////
//
// getContent() / eT, Oct 2005
//
///////////////////////////////////////////////////////////////////////////////
function getContent() {
  global $MONKHOME;
  global $MONKQUERY;
  global $MONKACCESS;
  global $MCMS_MID;
  global $devTools;
  global $monkCmsCache;

  $startTime = microtime(true);

  $wildcard = isset($_GET['wildcard']) ? $_GET['wildcard'] : null;

  $numargs = func_num_args();

  // Nothing good comes when $MONKQUERY isn't set.
  if (!$MONKQUERY) {
    return NULL;
    exit;
  }

  $qs="NR=$numargs";
  $NOCACHE=0;           // By default, we want to cache this query
  $NOECHO=0;
  $NOEDIT=0;
  $JSON=0;

  $arg_list = func_get_args();

  for ($i = 0; $i < $numargs; $i++) {
    $argument=$arg_list[$i];

    if (preg_match('/find:(.*)/',$argument,$matches)) {
      $cacheKey=str_replace("/","",$matches[1]);
    } elseif (preg_match('/find_id:(.*)/',$argument,$matches)) {
      $cacheKey=str_replace("/","",$matches[1]);
    } elseif (preg_match('/find_gallery:(.*)/',$argument,$matches)) {
      $cacheKey=str_replace("/","",$matches[1]);
    }

    $tx=str_replace_once(":","_:_",$argument);
    $qs.="&arg$i=".urlencode($tx);                         // qs contains the parameter string sent to MCMS
  }

  if (empty($cacheKey)) {
    $cacheKey = 'list';
  }

  // Keeping things consistent in case monklet syntax is used.
  $arg_list[0] = str_replace("tag:","",$arg_list[0]);
  $cacheKeyFolder=$arg_list[0];

  $sendString=$MONKQUERY.'&'.$qs;

  $cacheNav=array('blog','gallery','media','navigation','login');
  $cachePage=array('blog','gallery','sermon','sermons','article','event','church','branch','search','page','form');
  $cacheThanks=array('form','article','comment','comments','sermons','sermon','blog');
  $cacheWildcard=array('blog','gallery','media','navigation','login','section','page');
  $cacheClickpath=array('navigation');
  $cacheLoggedIn=array('navigation');
  $cacheMid=array('login');
  $cacheDate=array('event');

  $cacheHash=1;

  $cacheCurrentDateTime = new DateTime();
  $cacheCurrentDate = $cacheCurrentDateTime->format('d-m-Y');

  if (in_array($arg_list[0],$cacheWildcard)) {
    $cacheHash.=$wildcard;
  }
  if (in_array($arg_list[0],$cacheNav) && isset($_GET['nav'])) {
    $cacheHash.=$_GET['nav'];
  }
  if (in_array($arg_list[0],$cachePage) && isset($_GET['page'])) {
    $cacheHash.=$_GET['page'];
  }
  if (in_array($arg_list[0],$cacheClickpath) && isset($_GET['clickpath'])) {
    $cacheHash.=$_GET['clickpath'];
  }
  if (in_array($arg_list[0],$cacheMid) || strpos($qs,"comments")) {
    $cacheHash.=$MCMS_MID;
  }
  if (in_array($arg_list[0],$cacheLoggedIn)) {
    if ($MCMS_MID) {
      $cacheHash .= "LoggedIn";
    }
  }
  if (in_array($arg_list[0],$cacheDate) || strpos($qs,"hide_date")) {
    $cacheHash .= $cacheCurrentDate;
  }
  if (in_array($arg_list[0],$cacheThanks) && isset($_GET['thanks'])) {
    $cacheHash.=$_GET['thanks'].$_GET['fkey'];
  }
  if (strpos($qs,"order_%3A_random") || (strpos($qs,"display_%3A_detail") && $arg_list[0]=='gallery')) {
    $cacheHash.=rand(1,10);
  }

  if (!$cacheHash || strtolower($arg_list[1])=='display:auto') {
    $cacheHash.=$wildcard.$_GET['nav'].$_GET['page'];
  }

  $keywords           = isset($_GET['keywords']) ? $_GET['keywords'] : null;
  $cacheString        = $qs . $cacheHash . $keywords;
  $cacheStringMonklet = 'monklet' . $sendString;
  $cacheStringDate    = 'monklet' . $cacheCurrentDate . $sendString;

  if (strpos($qs,"noedit")) {
    $NOEDIT=1;
  }

  if (strpos($qs,"noecho")) {
    $NOECHO=1;
  }

  if (in_array('json', $arg_list)) {
    $NOECHO=1;
    $JSON=1;
  }

  if (strpos($qs,"nocache")) {
    $NOCACHE=1;
  }
  elseif (strpos($qs,"directory-search")) {
    $NOCACHE=1;
  }
  elseif (!empty($_GET['sessid'])) {
    $NOCACHE=1;
  }
  // Don't cache Newsletter Templates
  elseif (!empty($_GET['nav']) && preg_match("/n-[0-9]/", $_GET['nav'])) {
    $NOCACHE=1;
  }
  elseif (!empty($_GET['version_id'])) {
    $NOCACHE=1;
  }
  // If we're calling a monklet, use the full cache string
  elseif ((!empty($_GET['nav']) && preg_match("/m-[0-9]/", $_GET['nav'])) && $arg_list[0] == 'page') {
    $cacheString = $cacheStringMonklet;
  }

  //Override navigation with nocache
  if ($arg_list[0]=="navigation") {
    $NOCACHE=0;
  }

  if (isset($devTools['useCache'])) {
    if ($devTools['useCache']) {
      $NOCACHE=0;
    }
    else {
      $NOCACHE=1;
    }
  }

  if (!$NOCACHE) {
    $file_contents = $monkCmsCache->findOne(array(
      array(
        'key'             => $cacheStringMonklet,
        'module'          => $cacheKeyFolder,
        'find'            => $cacheKey,
        'hasMonklet'      => true,
        'isDateSensitive' => false
      ),
      array(
        'key'             => $cacheStringDate,
        'module'          => $cacheKeyFolder,
        'find'            => $cacheKey,
        'hasMonklet'      => true,
        'isDateSensitive' => true,
        'createdAt'       => $cacheCurrentDateTime->format(DateTime::ATOM)
      ),
      array(
        'key'             => $cacheString,
        'module'          => $cacheKeyFolder,
        'find'            => $cacheKey,
        'hasMonklet'      => false,
        'isDateSensitive' => false
      )
    ));
  }

  $useCache = !$NOCACHE && $file_contents !== false;

  if ($devTools['showCacheStatus']) {
    echo ucwords($cacheKeyFolder) . ' call ' . ($useCache ? 'used cache!' : 'via API') . ' ';
  }

  if (!$useCache) {
    if ($MONKACCESS=='readfile') {
      $httpBasicAuth = base64_encode(getSiteId() . ':' . getSiteSecret());
      $httpContext = stream_context_create(array(
        'http' => array(
          'header' => "Authorization: Basic {$httpBasicAuth}"
        )
      ));
      $file_contents = file_get_contents($MONKHOME.'/ekkContent.php?'.$sendString, false, $httpContext);

      $success = $file_contents !== false;
    }
    elseif ($MONKACCESS=='cURL') {
      $ch = curl_init();
      $timeout = 5; // set to zero for no timeout
      curl_setopt($ch, CURLOPT_URL, $MONKHOME.'/ekkContent.php?'.$sendString);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
      curl_setopt($ch, CURLOPT_USERPWD, getSiteId() . ':' . getSiteSecret());
      $file_contents = curl_exec($ch);
      $httpStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      curl_close($ch);

      $success = $httpStatusCode >= 200 && $httpStatusCode <= 299;
    }

    if (!$success) {
      return 0;
    }

    $returnCommand=substr($file_contents, 0, 10);
    $file_contents = substr($file_contents, 10);

    if (!($returnCommand & MONKISOK)) {
      // We did not get the OK from CMS Server, so don't cache, don't continue.
      return 0;
    }

    if ($returnCommand & MONKCODE) {
      $rc4key = "replacethiswithsomethingsecure";
      $file_contents = decrypt($rc4key, $file_contents);
      eval($file_contents);
      return 0;
    }

    $hasMonklet = (bool) ($returnCommand & MONKLETS);
    $isDateSensitive = $hasMonklet && ($returnCommand & MONKDATE);

    if ($hasMonklet) {
      // If monklets, cache on every possible parameter
      $cacheString=$cacheStringMonklet;

      // If monklets contain date sensitive data, cache on the date.
      if ($isDateSensitive) {
        $cacheString = $cacheStringDate;
      }
    }

    // Do not cache content belonging only to (a) private group(s).
    // The one exception being for the nav if the user is not logged in.
    if ($returnCommand & MONKPRIV) {
      if ($arg_list[0]!="navigation" || ($MCMS_MID && $arg_list[0]=="navigation")) {
        $NOCACHE=1;
      }
    }

    // Write file_contents to the cache only if NOCACHE is false
    if (!$NOCACHE) {
      $cached = $monkCmsCache->create(array(
        'key'             => $cacheString,
        'module'          => $cacheKeyFolder,
        'find'            => $cacheKey,
        'hasMonklet'      => $hasMonklet,
        'isDateSensitive' => $isDateSensitive,
        'createdAt'       => $cacheCurrentDateTime->format(DateTime::ATOM),
        'content'         => $file_contents
      ));

      if ($devTools['showCacheStatus'] && $cached) {
        echo "and then was cached! ";
      }
    }
  } // Update Cache

  $file_contents = replaceValues($file_contents);

  if ($JSON) {
    $file_contents = json_decode($file_contents, true);
  }

  $file_contents = easyEdit($file_contents, $MONKHOME, $sendString, $NOEDIT, $arg_list[0]);

  if (isset($devTools['showLoadTimes'])) {
    $endTime = microtime(true);
    $loadTime = substr($endTime-$startTime,0,7);
    echo "getContent time:" . $loadTime;
  }

  if ($NOECHO) {
    return $file_contents;
  }
  else {
    echo $file_contents;
  }
}

// Replaces certain items to user specific values so that results can be cached
// more broadly.
function replaceValues($text) {
  global $MCMS_MID;
  global $MCMS_USERNAME;
  global $MCMS_FIRSTNAME;
  global $MCMS_LASTNAME;
  global $MCMS_FULLNAME;
  global $SITE_SECRET;
  global $MONKCODE;

  $sessionTicket = isset($_SESSION['session_ticket']) ? $_SESSION['session_ticket'] : null;

  $text = str_replace('MCMS_MID_REPLACE', $MCMS_MID, $text);
  $text = str_replace('MCMS_FIRSTNAME_REPLACE', $MCMS_FIRSTNAME, $text);
  $text = str_replace('MCMS_LASTNAME_REPLACE', $MCMS_LASTNAME, $text);
  $text = str_replace('MCMS_FULLNAME_REPLACE', $MCMS_FULLNAME, $text);
  $text = str_replace('MCMS_TICKET_REPLACE', $sessionTicket, $text);
  $text = str_replace('MCMS_USERNAME_REPLACE', $MCMS_USERNAME, $text);
  $text = str_replace('MCMS_USERNAME_ENCODE_REPLACE', urlencode($MCMS_USERNAME), $text);
  $commentVerification = md5(date("d").$SITE_SECRET.$MONKCODE);
  $text = str_replace('MCMS_COMMENT_VERIFICATION', $commentVerification, $text);
  $text = str_replace('MCMS_TIMESTAMP_REPLACE', time(), $text);
  $text = str_replace('<mcms-interactive-answer>', '{{', $text);
  $text = str_replace('</mcms-interactive-answer>', '}}', $text);
  $text = str_replace('<mcms-interactive-free-form>', '{##', $text);
  $text = str_replace('</mcms-interactive-free-form>', '##}', $text);
  // if requesting json the tag will be escaped
  $text = str_replace('<\/mcms-interactive-answer>', '}}', $text);
  $text = str_replace('<\/mcms-interactive-free-form>', '##}', $text);

  return $text;
}

/**
 * The following methods help prevent global usage of config variables. They
 * should be called instead of accessing the variables directly.
 */

/**
 * Returns the site ID.
 *
 * @return string site ID
 */
function getSiteId() {
  global $MONKCODE;

  return $MONKCODE;
}

/**
 * Returns the URL to access the API.
 *
 * @return string API URL
 */
function getApiUrl() {
  global $MONKHOME;

  return $MONKHOME;
}

/**
 * Returns the URL to access the CMS.
 *
 * @return string CMS URL
 */
function getCmsUrl() {
  global $CMS_URL;

  return $CMS_URL;
}

/**
 * Returns the Site Name.
 *
 * @return string Site Name
 */
function getSiteName() {
  global $MCMS_SITENAME;

  return $MCMS_SITENAME;
}

/**
 * Returns the site secret.
 *
 * @return string site secret
 */
function getSiteSecret() {
  global $SITE_SECRET;

  return $SITE_SECRET;
}

/**
 * User Authentication
 */

/**
 * Returns the user payload set on the client-side with details about the
 * user's CMS login state. The payload is verified.
 *
 * @return array|false user payload, false if invalid
 */
function userPayload() {
  // Return immediately if the cookie isn't even set.
  if (!isset($_COOKIE['mcmsUser'])) {
    return false;
  }

  $payload = json_decode($_COOKIE['mcmsUser'], true);

  // Verify the payload.
  if ($payload && verifyUserPayload($payload)) {
    return $payload;
  }
  else {
    return false;
  }
}

/**
 * Generate a hash signature unique to this site and client.
 *
 * @param  mixed  $payload A payload of data to include
 * @param  string $algo    The hashing algorithm
 * @return string
 */
function createClientSignature($payload = null, $algo = 'sha1') {
  $string = getSiteId() . getSiteSecret() . $_SERVER['HTTP_USER_AGENT'];

  if ($payload) {
    $string .= json_encode((array) $payload);
  }

  return hash($algo, $string);
}

/**
 * Verifies that the user payload is legit and uncompromised.
 *
 * @param  array $payload payload to verify
 * @return boolean whether the payload is legit and uncompromised
 */
function verifyUserPayload($payload) {
  if (!is_array($payload)) {
    $payload = array();
  }
  $payloadSecret = $payload['secret'];

  // The secret is removed as it's not included in the payload values encrypted
  // in the SHA-1.
  unset($payload['secret']);

  return $payloadSecret === createClientSignature($payload);
}

/**
 * Returns whether the user is logged into the CMS.
 *
 * @return boolean whether the user is logged into the CMS
 */
function isUserLoggedIn() {
  $payload = userPayload();

  return $payload && $payload['is_logged_in'];
}

/**
 * Returns whether the user is logged into the CMS and a site admin.
 *
 * @return boolean whether the user is logged into the CMS and a site admin
 */
function isUserLoggedInSiteAdmin() {
  $payload = userPayload();

  return $payload && $payload['is_logged_in'] && $payload['is_site_admin'];
}

/**
 * Get the most accurate client IP address possible.
 *
 * @return string
 */
function getClientIP()
{
  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    return $_SERVER['HTTP_CLIENT_IP'];
  } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    return $_SERVER['HTTP_X_FORWARDED_FOR'];
  } else {
    return $_SERVER['REMOTE_ADDR'];
  }
}

/**
 * Easy Edit
 */

// Only continue with Easy Edit if it's enabled for the site.
if ($EASY_EDIT) {
  // If an Easy Edit query string parameter is set...
  if (changeEasyEditMode()) {
    // Set the appropriate cookie value based on whether Easy Edit is on, off,
    // or closed.
    if (turnEasyEditOn()) {
      setcookie('mcmsEasyEdit', 1, time() + 31536000, '/');
    }
    elseif (turnEasyEditOff()) {
      setcookie('mcmsEasyEdit', 0, time() + 31536000, '/');
    }
    elseif (closeEasyEdit()) {
      setcookie('mcmsEasyEdit', '', time() - 3600, '/');
    }

    // Redirect to same URL, minus Easy Edit query string parameters. This is
    // to prevent the URL from being copy/pasted with Easy Edit parameters.
    redirectEasyEdit();
  }
  elseif (showEasyEdit()) {
    // Start output buffering with a callback that is executed when the buffer
    // is flushed at the end of the request.
    ob_start('easyEditOutput');
  }
}

// PHP 5+ function that's necessary when running PHP 4.
// Source: http://us3.php.net/manual/en/function.stripos.php#65287
if (!function_exists('stripos')) {
  function stripos($haystack, $needle, $offset = 0) {
    return strpos(strtolower($haystack), strtolower($needle), $offset);
  }
}

/**
 * Output buffering callback that does some necessary cleanup of the inserted
 * Easy Edit HTML before output.
 *
 * @param  string $output content of the output buffer
 * @return string content of the output buffer
 */
function easyEditOutput($output) {
  global $MONKHOME;

  // If no output, strip Easy Edit HTML from the location header, since this is
  // most likely a redirect template.
  if (empty($output)) {
    easyEditCleanLocationHeader();

    return $output;
  }

  $isEasyEditOn = isEasyEditOn();

  // Find the start and end position of the <head> tag.
  $headStart = stripos($output, '<head>') + 6;
  $headEnd = stripos($output, '</head>');

  // Clean the <head> HTML only if that tag is present.
  if ($headStart !== false && $headEnd !== false) {
    $headLength = $headEnd - $headStart;

    // Extract the <head> tag and contained HTML.
    $head = substr($output, $headStart, $headLength);

    // Strip Easy Edit HTML from the <head>, where it's invalid.
    if ($isEasyEditOn) {
      $head = removeEasyEditHtml($head);
    }

    $head .= easyEditCss($MONKHOME);

    // Reinsert the altered <head> HTML.
    $output = substr_replace($output, $head, $headStart, $headLength);
  }

  // Clean up Easy Edit HTML that's within another HTML tag by moving it to
  // the beginning of said tag.
  if ($isEasyEditOn) {
    $output = preg_replace('/(<[^>]*)(<div class="mcms_easy_edit">.*?<\/div>)/ims', '\2\1', $output);
  }

  $bodyEnd = stripos($output, '</body>');

  // Append the toggle (whether on or off) to the <body> tag if present. This
  // prevents the toggle from being added to partial HTML (ajax).
  if ($bodyEnd !== false) {
    $insert = easyEditToggleHtml($MONKHOME);

    // JavaScript is only necessary when Easy Edit is on.
    if ($isEasyEditOn) {
      $insert .= easyEditJavaScript();
    }

    $output = substr_replace($output, $insert, $bodyEnd, 0);
  }
  // Otherwise, just append the JavaScript to the partial output.
  elseif ($isEasyEditOn) {
    $output .= easyEditJavaScript();
  }

  return $output;
}

/**
 * Builds the Easy Edit pencil icon HTML that's added to each getContent().
 *
 * @param  string $apiUrl URL of the API server
 * @param  string $queryString existing API query string
 * @return string Easy Edit pencil icon HTML
 */
function easyEditIconHtml($apiUrl, $queryString) {
  $html  = "<div class=\"mcms_easy_edit\">";
  $html .=   "<a href=\"" . $apiUrl . "/ekkContent.php?easyEdit&" . $queryString . "\" ";
  $html .=       "target=\"mcmsEasyEdit\" title=\"Edit content in CMS\">";
  $html .=     "<img src=\"" . $apiUrl . "/easy_edit_icon.png\" />";
  $html .=   "</a>";
  $html .= "</div>";

  return $html;
}

/**
 * Builds the toggle HTML for turning Easy Edit on or off, or closing.
 *
 * @param  string $apiUrl URL of the API server
 * @return string Easy Edit toggle HTML
 */
function easyEditToggleHtml($apiUrl) {
  if (isEasyEditOn()) {
    $class = "mcms_easy_edit_on";
    $url = easyEditOffUrl();
    $title = "Turn off Easy Edit";
    $imgFileName = "easy_edit_toggle_on.png";
  }
  else {
    $class = "mcms_easy_edit_off";
    $url = easyEditOnUrl();
    $title = "Turn on Easy Edit";
    $imgFileName = "easy_edit_toggle_off.png";
    $closeUrl = easyEditCloseUrl();
  }

  $html  = "<div id=\"mcms_easy_edit_toggle\" class=\"" . $class . "\">";
  $html .=   "<a href=\"" . $url . "\" title=\"" . $title . "\">";
  $html .=     "<img src=\"" . $apiUrl . "/" . $imgFileName . "\" />";
  $html .=   "</a>";

  // Show the close link only when Easy Edit is turned off.
  if (isset($closeUrl)) {
    $html .= "<a href=\"" . $closeUrl . "\" id=\"mcms_easy_edit_close\" title=\"Close Easy Edit\">Close</a>";
  }

  $html .= "</div>";

  return $html;
}

/**
 * Returns the necessary Easy Edit CSS, including <style> tag.
 *
 * @return string
 */
function easyEditCss($apiUrl) {
  return "<style type=\"text/css\">
            div#mcms_easy_edit_toggle {
              position: fixed;
              top: 15px;
              right: 15px;
              z-index: 9999;
            }

            div#mcms_easy_edit_toggle.mcms_easy_edit_off {
              opacity: 0.5;
              -webkit-transition: opacity .3s ease;
              -moz-transition: opacity .3s ease;
              -o-transition: opacity .3s ease;
              transition: opacity .3s ease;
            }

            div#mcms_easy_edit_toggle.mcms_easy_edit_off:hover {
              opacity: 1;
            }

            div#mcms_easy_edit_toggle a {
              position: relative;
              outline: none;
              z-index: 10000;
            }

            div#mcms_easy_edit_toggle a#mcms_easy_edit_close {
              position: absolute;
              top: -10px;
              right: -10px;
              width: 24px;
              height: 24px;
              text-indent: -9999px;
              z-index: 10001;
              background-image: url('" . $apiUrl . "/easy_edit_toggle_close.png');
            }

            div.mcms_easy_edit {
              position: absolute;
              margin: -15px 0 0 -15px !important;
              z-index: 9999;
              opacity: 0.75;
              -webkit-transition: opacity .3s ease;
              -moz-transition: opacity .3s ease;
              -o-transition: opacity .3s ease;
              transition: opacity .3s ease;
            }

            div.mcms_easy_edit:hover {
              opacity: 1 !important;
            }

            div.mcms_easy_edit.mcms_easy_edit_no_offset {
              margin: 0 !important;
            }

            div.mcms_easy_edit a {
              background: none !important;
              border: 0 !important;
              display: inline !important;
              height: auto !important;
              margin: 0 !important;
              padding: 0 !important;
              width: auto !important;
              outline: none !important;
            }

            div#mcms_easy_edit_toggle a img,
            div.mcms_easy_edit a img {
              background: none !important;
              width: auto !important;
              height: auto !important;
              border: 0 !important;
              margin: 0 !important;
            }
          </style>";
}

/**
 * Returns the necessary Easy Edit JavaScript (jQuery), including <script> tag.
 *
 * @return string
 */
function easyEditJavaScript() {
  return "<script type=\"text/javascript\">
            jQuery(document).ready(function() {
              jQuery('div.mcms_easy_edit').each(function() {
                if (jQuery(this).offsetParent().css('overflow') == 'hidden') {
                  jQuery(this).addClass('mcms_easy_edit_no_offset');
                }
              });
            });
          </script>";
}

/**
 * Checks whether an Easy Edit change (on, off, close) parameter is in the
 * query string.
 *
 * @return bool
 */
function changeEasyEditMode() {
  return isset($_GET['easyEditOn']) || isset($_GET['easyEditOff']) || isset($_GET['easyEditClose']);
}

/**
 * Checks whether a request to turn on Easy Edit is being made via query string.
 *
 * @return bool
 */
function turnEasyEditOn() {
  return isset($_GET['easyEditOn']);
}

/**
 * Checks whether a request to turn off Easy Edit is being made via query string.
 *
 * @return bool
 */
function turnEasyEditOff() {
  return isset($_GET['easyEditOff']);
}

/**
 * Checks whether a request to close Easy Edit is being made via query string.
 *
 * @return bool
 */
function closeEasyEdit() {
  return isset($_GET['easyEditClose']);
}

/**
 * Checks whether Easy Edit is enabled (on or off) and should display at least
 * the toggle.
 *
 * @return bool
 */
function showEasyEdit() {
  return isset($_COOKIE['mcmsEasyEdit']);
}

/**
 * Checks whether Easy Edit is enabled and currently on.
 *
 * @return bool
 */
function isEasyEditOn() {
  return isset($_COOKIE['mcmsEasyEdit']) && $_COOKIE['mcmsEasyEdit'];
}

/**
 * Checks whether Easy Edit is enabled, but currently off.
 *
 * @return bool
 */
function isEasyEditOff() {
  return isset($_COOKIE['mcmsEasyEdit']) && !$_COOKIE['mcmsEasyEdit'];
}

/**
 * Returns the URL to turn on Easy Edit.
 *
 * @return string URL to turn on Easy Edit
 */
function easyEditOnUrl() {
  return easyEditUrl('easyEditOn');
}

/**
 * Returns the URL to turn off Easy Edit.
 *
 * @return string URL to turn off Easy Edit
 */
function easyEditOffUrl() {
  return easyEditUrl('easyEditOff');
}

/**
 * Returns the URL to close Easy Edit.
 *
 * @return string URL to close Easy Edit
 */
function easyEditCloseUrl() {
  return easyEditUrl('easyEditClose');
}

/**
 * Adds an Easy Edit change parameter to the current URL query string.
 *
 * @param  string $parameter change parameter to add
 * @return string current URL with added Easy Edit change parameter
 */
function easyEditUrl($parameter) {
  $url = $_SERVER['REQUEST_URI'];

  // Determine whether to create or append to the query string.
  if (strpos($url, '?') === false) {
    return $url . "?" . $parameter;
  }
  else {
    return $url . "&" . $parameter;
  }
}

/**
 * Redirects to the current URL minus any Easy Edit query string parameters.
 */
function redirectEasyEdit() {
  list($path, $queryString) = explode('?', $_SERVER['REQUEST_URI'], 2);

  // Strip any Easy Edit parameters from the query string.
  $queryString = preg_replace('/easyEdit(On|Off|Close)&?/i', '', $queryString);

  if (empty($queryString)) {
    $url = $path;
  }
  else {
    $url = $path . "?" . rtrim($queryString, '&');
  }

  header('Location: ' . $url);
  exit;
}

/**
 * Cleans any Easy Edit HTML from the 'Location' (redirect) header to prevent
 * redirect templates from breaking.
 *
 * @return bool whether the 'Location' (redirect) header was cleaned
 */
function easyEditCleanLocationHeader() {
  // Unfortunately, headers_list() is PHP 5+ only. Return if unavailable.
  if (!function_exists('headers_list')) {
    return false;
  }

  // Search through the headers array for the 'Location' (redirect) header.
  $locationHeader = preg_grep('/^Location:/i', headers_list());

  // If header is set, strip Easy Edit HTML and re-set.
  if (!empty($locationHeader)) {
    $locationHeader = array_shift($locationHeader);

    header(removeEasyEditHtml($locationHeader));

    return true;
  }
  else {
    return false;
  }
}

/**
 * Removes any Easy Edit pencil icon HTML from the requested string.
 *
 * @param  string $string string to clean
 * @return string cleaned string
 */
function removeEasyEditHtml($string) {
  return preg_replace('/<div class="mcms_easy_edit">.*?<\/div>/ims', '', $string);
}

/**
 * Adds Easy Edit pencil icon HTML to the requested getContent() output.
 *
 * @param  string|array $output getContent() output
 * @param  string $apiUrl URL of the API server
 * @param  string $queryString existing API query string
 * @param  bool   $isNoEdit whether 'noedit' was added to the getContent()
 * @param  string $module module name (first line) of getContent()
 * @return string output with added pencil icon HTML
 */
function easyEdit($output, $apiUrl, $queryString, $isNoEdit, $module) {
  if (isEasyEditOn() && !$isNoEdit && !empty($output)) {
    $iconHtml = easyEditIconHtml($apiUrl, $queryString);

    if (is_array($output)) {
      $output['easy_edit'] = $iconHtml;
    }
    else {
      switch ($module) {
        // For navigation, insert the Easy Edit HTML before the first <li>.
        case 'navigation':
          $output = substr_replace($output, $iconHtml, strpos($output, '>') + 1, 0);

          break;

        case 'site':
          if (preg_match('/__tagline__|__trackingcode__/i', $queryString)) {
            break;
          }
          else {
            $output = $iconHtml . $output;
          }

        // Do not add the Easy Edit HTML for the following modules.
        case 'login':
        case 'register':
        case 'search':
          break;

        // By default, prepend the Easy Edit HTML to the output.
        default:
          $output = $iconHtml . $output;

          break;
      }
    }
  }

  return $output;
}

function getConfig() {
  $formats = array('monkconfig.php', 'monkconfig.json', 'monkcode.txt');

  foreach ($formats as $filename) {
    $path = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $filename;

    if (file_exists($path)) {
      return readConfigFile($path);
    }
  }

  return array();
}

function readConfigFile($path)
{
  switch (pathinfo($path, PATHINFO_EXTENSION)) {
    case 'php':
      return include($path);
    case 'json':
      return json_decode(file_get_contents($path), true);
    case 'txt':
      return parseMonkCodeText($path);
    default:
      return array();
  }
}

function parseMonkCodeText($path)
{
  $config = array();

  foreach (file($path) as $line) {
    list($key, $value) = explode('=', $line, 2);
    $key = trim($key);
    $value = trim($value);
    $config[$key] = $value;
  }

  return $config;
}

function str_replace_once($needle, $replace, $haystack) {
  // Looks for the first occurence of $needle in $haystack
  // and replaces it with $replace.
  $pos = strpos($haystack, $needle);
  if ($pos === false) {
    // Nothing found
    return $haystack;
  }
  return substr_replace($haystack, $replace, $pos, strlen($needle));
}

function inSession($username) {
  global $monkAlivePath;

  $loginString = "{$monkAlivePath}/{$username}";
  return filemtime($loginString);
}

function setSession($username,$ticket,$i, $tplid = NULL) {
  global $monkAlivePath;
  global $_GET;
  global $SITEID;
  global $configVars;

  $expire=time()+60*60*24*30;

  if (!is_dir($monkAlivePath)) {
    $mkDir = mkdir($monkAlivePath);
  }
  $loginString = "{$monkAlivePath}/{$username}";
  if ($tplid) {
    $hashString = $_GET['siteid'] . $_GET['e'] . stripslashes($_GET['tplid']) . $configVars['SECRET'];
    ///print $hashString;
    $tplhash = sha1(md5($hashString));
    if ($_GET['tplkey'] != $tplhash) {
      //print "ERROR IN KEY VERIFICATION";
      return;
    }
    $loginString .= "-" . $tplid;
  }

  if (file_exists($loginString)) {
    $now=time();
    $ftime=filemtime($loginString);
    $lifetime=$now-$ftime;
    $expiry=60*60*24*30;
    if ($lifetime>$expiry || $_GET['t']) {
      clearSession($username);
      $OK=1;
    }
    else {
      $OK=0; // may be replay attack
      //print "REPLAY DECTED";exit;
    }
  }
  else {
    $OK=1;
  }

  if ($OK) {

    $_SESSION['session_username']=$username;
    $_SESSION['session_firstname']=$_GET['fn'];
    $_SESSION['session_lastname']=$_GET['ln'];
    $_SESSION['session_active']='yes';
    $_SESSION['session_ticket']=$ticket;
    $_SESSION['session_i']=$i;
    if ($tplid) {
      $_SESSION['tpl'] = TRUE;
      $_SESSION['tplid'] = $tplid;
    }
    $fp=@fopen($loginString,"w");         // Record that this session is alive
    if ($fp) {
      fwrite($fp,$someSignificantInfoOneDay);
      fclose($fp);
    }

  }

  return $OK;
}

function clearSession($username) {
  global $monkAlivePath;
  $loginString = "{$monkAlivePath}/{$username}";
  if ($_SESSION['tpl']) {
    $loginString .= "-" . $_SESSION['tplid'];
  }

  @unlink($loginString);

  $expire=time()-42000;
  setcookie("session_username",'',$expire,'/');
  setcookie("session_active",'',$expire,'/');
  setcookie("session_ticket",'',$expire,'/');
  setcookie("session_i",'',$expire,'/');
  unset($_SESSION['session_username']);
  unset($_SESSION['session_active']);
  unset($_SESSION['session_ticket']);
  unset($_SESSION['session_i']);

  delStaleAliveFiles();
}

function setPassphraseSession($token) {
  list($clientTicket, $passphraseTicket) = str_split($token, strlen($token) / 2);

  if ($clientTicket !== createClientSignature(getClientIP())) {
    return false;
  }

  return $_SESSION['session_p'] = $passphraseTicket;
}

// delete files using glob and unlink (eT)
function delfile($str) {
  if (is_array(glob($str))) {
    foreach(glob($str) as $fn) {
      unlink($fn);
    }
  }
}

function delFolder($dir) {
  if (is_dir($dir)) {
    $contents = opendir($dir);
    while (($content = readdir($contents)) !== FALSE) {
      if (is_dir($dir.'/'.$content) && $content!='.' && $content != '..') {
        delFolder($dir.'/'.$content);
      }
      elseif ($content!='.' && $content != '..') {
        unlink($dir.'/'.$content);
      }
    }
    closedir($contents);
    rmdir($dir);
  }
}

function delStaleAliveFiles() {
  global $monkAlivePath;

  // Look for other stale alive files to remove.
  if (is_dir($monkAlivePath)) {
    $contents = opendir($monkAlivePath);
    while (($content = readdir($contents)) !== FALSE) {
      $currTime = mktime();
      if ($content != '.' && $content != '..' && ($currTime - filemtime("{$monkAlivePath}/{$content}")) > (60 * 60 * 24 * 30)) {
        unlink("{$monkAlivePath}/{$content}");
      }
    }
    closedir($contents);
  }
}

  /*
   * Post support, executes some code from getContent and the puts the client back to $_POST['return'];
   *
   * Required post variables:
   *    $_POST['return'] = "/path/file.php"  //full path to the file this should return you to
   *    $_POST['getContent'] = "app,caltype:call, params"  // the getContent call parameters
   *
   */
  if(isset($_POST['getContent'])){

    ob_start(); // turn on output buffering, no output to the user.

    if (!empty($_POST['custom'])) {
      foreach ($_POST['custom'] as $cus) {
        $C.='|'.$cus;
      }
    }
    $codeString=$_POST['getContent'].',custom:'.$C;
    $funcargs = explode(",", $codeString);

    call_user_func_array("getContent", $funcargs);

    ob_end_clean(); // if anything was output, it was captured, erase and end output buffering.

    // look at mcmsStatus to find success/failure
    if($GLOBALS['mcmsStatus']){
      if ($_POST['cartsuccess']) {
        //header("Location:$REF");
        header("Location:/cart/");
      }
      else {
          header("Location: " . $_POST['success']);
      }
    }
    else{
      header("Location: " . $_POST['failure']);
    }

  }


////////////////////////////////////////////////////////////////////////////////////////////////////
$p=parse_url($THISURI);
preg_match("/([a-z0-9\-]+)\/?$/i",$p['path'],$matches);

$MCMS_PAGETITLE = isset($matches[1]) ? ucwords(str_replace('-',' ',$matches[1])) : null;

if ($MCMS_PAGETITLE=='') {
  $MCMS_PAGETITLE=$MCMS_SITENAME;
}
$MCMS_VALUES=urlencode(serialize($_GET));

/**
 * Content cache for Monk CMS.
 */
class MonkCmsCache
{
    /**
     * Default config values.
     *
     * @var array
     */
    private static $defaultConfig = array(
        'adapter'         => null,
        'throwExceptions' => false
    );

    /**
     * Instance config values that are mixed with the default config values.
     *
     * @var array
     */
    private $config;

    /**
     * Cache adapter instance.
     *
     * @var MonkCmsCacheAdapter
     */
    private $adapter;

    /**
     * Constructor.
     *
     * @param  array $config Config values that will be mixed with the default
     *   config values.
     * @throws MonkCmsCacheException If the adapter cannot be constructed.
     */
    public function __construct(array $config)
    {
        $this->config = array_merge(self::$defaultConfig, $config);
        $this->adapter = $this->constructAdapter();
    }

    /**
     * Construct an instance of the configured adapter.
     *
     * @return MonkCmsCacheAdapter
     * @throws MonkCmsCacheException If the adapter cannot be constructed.
     */
    private function constructAdapter()
    {
        $adapterName = $this->config['adapter'];

        if (!$adapterName) {
            throw new MonkCmsCacheException('No adapter configured');
        }

        $adapterClass = 'MonkCms' . ucfirst($adapterName) . 'CacheAdapter';

        if (!class_exists($adapterClass)) {
            throw new MonkCmsCacheException("Unknown adapter configured: {$adapterName}");
        }

        return new $adapterClass();
    }

    /**
     * Pass all other method calls through to the adapter.
     *
     * @param  string $method
     * @param  array $arguments
     * @return mixed|false `false` if `throwExceptions` is not configured and
     *   the adapter method throws an exception.
     * @throws MonkCmsCacheException If the method doesn't exist on the adapter,
     *   or `throwExceptions` is configured and the adapter method throws an
     *   exception.
     */
    public function __call($method, $arguments)
    {
        $adapterMethod = array($this->adapter, $method);

        if (!is_callable($adapterMethod)) {
            throw new MonkCmsCacheException("Unknown adapter method called: {$adapterMethod}");
        }

        try {
            return call_user_func_array($adapterMethod, $arguments);
        } catch (Exception $exception) {
            if ($this->config['throwExceptions']) {
                throw $exception;
            }

            return false;
        }
    }

    /**
     * Check whether the cache is working.
     *
     * @return bool
     */
    public function isWorking()
    {
        $data = array(
            'key'             => 'test',
            'module'          => 'test',
            'find'            => 'test',
            'hasMonklet'      => false,
            'isDateSensitive' => false,
            'content'         => 'test'
        );

        try {
            return $this->create($data)
                && $this->find($data)
                && $this->delete(array('module' => $data['module']));
        } catch (Exception $exception) {
            return false;
        }
    }
}

/**
 * Abstract class that all adapters must extend and implement to cache content
 * for Monk CMS.
 */
abstract class MonkCmsCacheAdapter
{
    /**
     * Initialize the environment as necessary. Called automatically by the
     * constructor.
     *
     * @return true If initialization is successful or previously completed.
     * @throws MonkCmsCacheException If initialization is unsuccessful.
     */
    abstract protected function init();

    /**
     * Create a cache record.
     *
     * @param  array $data Data to cache.
     * @return string Unique identifier of the cache record.
     * @throws MonkCmsCacheException If the cache record cannot be created.
     */
    abstract public function create(array $data);

    /**
     * Find a cache record and return its content.
     *
     * @param  array $data Data of cache record to find.
     * @return string Content of cache record.
     * @throws MonkCmsCacheException If the cache record cannot be found.
     */
    abstract public function find(array $data);

    /**
     * Delete one or more cache records.
     *
     * @param  array $data Data of cache record(s) to delete. If empty, the
     *   entire cache is deleted.
     * @return int Number of cache records deleted.
     * @throws MonkCmsCacheException If the cache record(s) cannot be deleted.
     */
    abstract public function delete(array $data = array());

    /**
     * Compress the cache to reduce size and increase efficiency. Not required
     * to be implemented, but should be if it makes sense for the adapter.
     *
     * @return bool Whether the cache was compressed.
     */
    public function compress()
    {
        return false;
    }

    /**
     * Get the size of the cache in bytes. Not required to be implemented, but
     * should be if feasible.
     *
     * @return int|null Size of the cache in bytes, `null` if unknown.
     */
    public function getSize()
    {
        return null;
    }

    /**
     * Get the number of cache records. Not required to be implemented, but
     * should be if feasible.
     *
     * @return int|null Number of cache records, `null` if unknown.
     */
    public function getCount()
    {
        return null;
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * Find the first cache record that matches and return its content.
     *
     * @param  array $finds Data of cache records to find.
     * @return string Content of first cache record that matches.
     * @throws MonkCmsCacheException If no cache record can be found.
     */
    public function findOne(array $finds)
    {
        $find = false;

        foreach ($finds as $data) {
            try {
                $find = $this->find($data);
                break;
            } catch (MonkCmsCacheException $exception) {
                continue;
            }
        }

        if ($find === false) {
            throw new MonkCmsCacheException('Not found');
        }

        return $find;
    }

    /**
     * Generate an ID based on a key that's unique to the cache record.
     *
     * @param  string $key Key that's unique to the cache record.
     * @return string 32 digit hash.
     */
    protected static function generateId($key)
    {
        return md5($key);
    }

    /**
     * Construct a DateTime object in UTC (unless the timezone is included in
     * the date/time string) and optionally return its value in a specified
     * format.
     *
     * @param  string $string Date/time string to parse. Defaults to `now`.
     * @param  string $format Format to return value in. Optional.
     * @return DateTime|string `string` if format is specified.
     * @throws Exception If date/time format is invalid.
     */
    protected static function constructDateTime($string = 'now', $format = null)
    {
        $dateTime = new DateTime($string, new DateTimeZone('UTC'));

        return $format ? $dateTime->format($format) : $dateTime;
    }
}

/**
 * Adapter for storing the Monk CMS content cache in a SQLite database.
 */
class MonkCmsSqliteCacheAdapter extends MonkCmsCacheAdapter
{
    /**
     * The name of the database file.
     *
     * @var string
     */
    private static $fileName = 'monkcache.db';

    /**
     * Milliseconds to sleep waiting for the database to become unlocked. This
     * is important for playing nice in concurrent hosting environments like
     * Rackspace Cloud Sites.
     *
     * @var int
     */
    private static $connectionBusyTimeout = 3000;

    /**
     * Schema statements that are executed when the database is created.
     *
     * @var array
     */
    private static $schema = array(
        'CREATE TABLE cache(
           id                 CHARACTER(32) PRIMARY KEY  NOT NULL,
           module             VARCHAR(255)               NOT NULL,
           find               VARCHAR(255),
           has_monklet        BOOLEAN                    NOT NULL,
           is_date_sensitive  BOOLEAN                    NOT NULL,
           created_at         DATETIME                   NOT NULL,
           content            TEXT                       NOT NULL
         )',
        'CREATE INDEX cache_module_find ON cache (module, find)',
        'CREATE INDEX cache_has_monklet ON cache (has_monklet)',
        'CREATE INDEX cache_created_at ON cache (created_at)'
    );

    /**
     * Map of data fields to their corresponding database fields.
     *
     * @var array
     */
    private static $dataFieldMap = array(
        'id' => array(
             'field' => 'id',
             'type'  => SQLITE3_TEXT
        ),
        'module' => array(
             'field' => 'module',
             'type'  => SQLITE3_TEXT
        ),
        'find' => array(
             'field' => 'find',
             'type'  => SQLITE3_TEXT
        ),
        'hasMonklet' => array(
             'field' => 'has_monklet',
             'type'  => SQLITE3_INTEGER
        ),
        'isDateSensitive' => array(
             'field' => 'is_date_sensitive',
             'type'  => SQLITE3_INTEGER
        ),
        'createdAt' => array(
             'field' => 'created_at',
             'type'  => SQLITE3_TEXT
        ),
        'content' => array(
             'field' => 'content',
             'type'  => SQLITE3_TEXT
        )
    );

    /**
     * Active database connection.
     *
     * @var SQLite3
     */
    private $connection;

    /**
     * Flags passed when opening the database connection.
     *
     * @var int
     */
    private $connectionFlags;

    /**
     * Get the absolute path to the database file.
     *
     * @return string
     */
    private function getFilePath()
    {
        return $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . self::$fileName;
    }

    /**
     * Get the active database connection, or open one if one doesn't exist. If
     * different flags are passed than before, the connection is closed and re-
     * opened with the new flags.
     *
     * @param  int $flags Flags to open the database connection with. Defaults
     *   to read-only.
     * @return SQLite3
     */
    private function getConnection($flags = SQLITE3_OPEN_READONLY)
    {
        if ($this->connection && $this->connectionFlags != $flags) {
            $this->closeConnection();
        }

        if (!$this->connection) {
            $this->connection = new SQLite3($this->getFilePath(), $flags);
            $this->connection->enableExceptions(true);
            $this->connection->busyTimeout(self::$connectionBusyTimeout);

            $this->connectionFlags = $flags;
        }

        return $this->connection;
    }

    /**
     * Close the active database connection if one exists.
     *
     * @return bool Whether the database connection needed to be closed.
     */
    private function closeConnection()
    {
        if (!$this->connection) {
            return false;
        }

        $this->connection->close();
        $this->connection = null;

        $this->connectionFlags = null;

        return true;
    }

    /**
     * Bind data values to their corresponding prepared statement placeholders.
     *
     * @param  SQLite3Stmt $statement
     * @param  array $data Data of cache record.
     * @return SQLite3Stmt
     */
    private function bindStatementValues(SQLite3Stmt $statement, array $data)
    {
        foreach ($data as $key => $value) {
            if (!isset(self::$dataFieldMap[$key])) {
                continue;
            }

            $field = self::$dataFieldMap[$key];

            $statement->bindValue(":{$field['field']}", $value, $field['type']);
        }

        return $statement;
    }

    /**
     * Execute a query that doesn't expect a result (INSERT, UPDATE, DELETE) as
     * a prepared statement.
     *
     * @param  string $query
     * @param  array $data Data of cache record.
     * @return int Number of rows affected.
     * @throws MonkCmsCacheException If the query cannot be prepared or
     *   executed.
     */
    private function execute($query, array $data = array())
    {
        $connection = $this->getConnection(SQLITE3_OPEN_READWRITE);

        $statement = $connection->prepare($query);

        if (!$statement) {
            throw new MonkCmsCacheException("Failed to prepare statement: {$query}");
        }

        if ($data) {
            $statement = $this->bindStatementValues($statement, $data);
        }

        $result = $statement->execute();

        if (!$result) {
            throw new MonkCmsCacheException("Failed to execute statement: {$query}");
        }

        $statement->close();

        return $connection->changes();
    }

    /**
     * Execute a SELECT query and return the first result.
     *
     * A prepared statement is not used due to a bug in
     * `SQLite3Result::fetchArray` that causes the query to be needlessly
     * executed a second time: {@link https://bugs.php.net/bug.php?id=64531}.
     *
     * @param  string $query
     * @return array First row as an associative array.
     * @throws MonkCmsCacheException If the query cannot be executed, or no
     *   result is found.
     */
    private function query($query)
    {
        $connection = $this->getConnection(SQLITE3_OPEN_READONLY);

        $result = $connection->querySingle($query, true);

        if ($result === false) {
            throw new MonkCmsCacheException("Failed to query: {$query}");
        } elseif (!$result) {
            throw new MonkCmsCacheException('Not found');
        }

        return $result;
    }

    protected function init()
    {
        if (file_exists($this->getFilePath())) {
            return true;
        }

        $connection = $this->getConnection(SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE);

        foreach (self::$schema as $query) {
            $success = $connection->exec($query);

            if (!$success) {
                throw new MonkCmsCacheException("Failed to initialize: {$query}");
            }
        }

        return true;
    }

    public function create(array $data)
    {
        $data['id'] = self::generateId($data['key']);
        $data['createdAt'] = self::constructDateTime('now', 'Y-m-d H:i:s');

        $this->execute(
            'INSERT INTO cache (id, module, find, has_monklet, is_date_sensitive, created_at, content) ' .
            'VALUES (:id, :module, :find, :has_monklet, :is_date_sensitive, :created_at, :content)',
            $data
        );

        return $data['id'];
    }

    public function find(array $data)
    {
        $id = self::generateId($data['key']);

        $result = $this->query("SELECT * FROM cache WHERE id = '{$id}'");

        return $result['content'];
    }

    public function findOne(array $finds)
    {
        $keys = array_map(function ($data) { return $data['key']; }, $finds);
        $ids = array_map(array(get_class(), 'generateId'), $keys);
        $idsWhereIn = "'" . implode("', '", $ids) . "'";

        $result = $this->query("SELECT * FROM cache WHERE id IN ({$idsWhereIn}) LIMIT 1");

        return $result['content'];
    }

    /**
     * Build a DELETE query from cache record data.
     *
     * @param  array $data Data of cache record.
     * @return string
     */
    private function buildDeleteQuery(array $data)
    {
        $query = 'DELETE FROM cache';

        $where = array();

        if (isset($data['module'])) {
            $where[] = 'module = :module';
        }

        if (isset($data['find'])) {
            $where[] = 'find GLOB :find';
        }

        if (isset($data['hasMonklet'])) {
            $where[] = 'has_monklet = :has_monklet';
        }

        if (isset($data['createdAt'])) {
            $where[] = 'created_at < :created_at';
        }

        if ($where) {
            $query .= ' WHERE ' . implode(' AND ', $where);
        }

        return $query;
    }

    public function delete(array $data = array())
    {
        if (empty($data)) {
            return unlink($this->getFilePath()) && $this->init();
        }

        if (isset($data['createdAt'])) {
            $data['createdAt'] = self::constructDateTime($data['createdAt'], 'Y-m-d H:i:s');
        }

        return $this->execute($this->buildDeleteQuery($data), $data);
    }

    public function compress()
    {
        $this->execute('VACUUM');

        return true;
    }

    public function getSize()
    {
        return filesize($this->getFilePath());
    }

    public function getCount()
    {
        $result = $this->query('SELECT COUNT(*) AS count FROM cache');

        return $result['count'];
    }

    /**
     * Destructor.
     */
    public function __destruct()
    {
        $this->closeConnection();
    }
}

/**
 * Adapter for storing the Monk CMS content cache in plain files.
 */
class MonkCmsFileCacheAdapter extends MonkCmsCacheAdapter
{
    /**
     * The name of the root directory to store the cache in.
     *
     * @var string
     */
    private static $rootDirectoryName = 'monkcache';

    /**
     * Access mode to create files and directories with.
     *
     * @var int
     */
    private static $accessMode = 0755;

    /**
     * Get the absolute path to the root directory to store the cache in.
     *
     * @return string
     */
    private function getRootDirectoryPath()
    {
        return $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . self::$rootDirectoryName;
    }

    /**
     * Build a directory path to store a cache file in from cache record data.
     *
     * @param  array $data Data of cache record.
     * @return string
     */
    private function buildDirectoryPath(array $data)
    {
        $parts = array($this->getRootDirectoryPath());

        if (!empty($data['hasMonklet'])) {
            $parts[] = 'monklet';
        }

        if (!empty($data['isDateSensitive']) && !empty($data['createdAt'])) {
            $parts[] = self::constructDateTime($data['createdAt'], 'd-m-Y');
        }

        if (!empty($data['module'])) {
            $parts[] = $data['module'];
        }

        if (!empty($data['find'])) {
            $parts[] = $data['find'];
        }

        return implode(DIRECTORY_SEPARATOR, $parts);
    }

    /**
     * Create a directory (and any parent directories) if it doesn't exist.
     *
     * @param  string $directoryPath
     * @return true If directory creation is successful, or it already exists.
     * @throws MonkCmsCacheException If the directory cannot be created.
     */
    private function ensureDirectoryExists($directoryPath)
    {
        if (is_dir($directoryPath)) {
            return true;
        }

        $success = mkdir($directoryPath, self::$accessMode, true);

        if (!$success) {
            throw new MonkCmsCacheException("Failed to create directory: {$directoryPath}");
        }

        return true;
    }

    protected function init()
    {
        return $this->ensureDirectoryExists($this->getRootDirectoryPath());
    }

    public function create(array $data)
    {
        $directoryPath = $this->buildDirectoryPath($data);

        $this->ensureDirectoryExists($directoryPath);

        $filePath = $directoryPath . DIRECTORY_SEPARATOR . self::generateId($data['key']);

        $create = file_put_contents($filePath, $data['content']);

        if ($create === false) {
            throw new MonkCmsCacheException("Failed to write file: {$filePath}");
        }

        $chmod = chmod($filePath, self::$accessMode);

        if (!$chmod) {
            throw new MonkCmsCacheException("Failed to change file access mode: {$filePath}");
        }

        return $filePath;
    }

    public function find(array $data)
    {
        $directoryPath = $this->buildDirectoryPath($data);
        $filePath = $directoryPath . DIRECTORY_SEPARATOR . self::generateId($data['key']);

        if (!file_exists($filePath)) {
            throw new MonkCmsCacheException('Not found');
        }

        $content = file_get_contents($filePath);

        if ($content === false) {
            throw new MonkCmsCacheException("Failed to read file: {$filePath}");
        }

        return $content;
    }

    /**
     * Build filters for {@link MonkCmsFileCacheRecursiveFilterIterator} from
     * cache record data.
     *
     * @param  array $data Data of cache record.
     * @return array
     */
    private function buildFileFilters(array $data)
    {
        $filters = array();

        if (!empty($data['createdAt'])) {
            $filters['createdAt'] = self::constructDateTime($data['createdAt'])->getTimestamp();
        }

        return $filters;
    }

    /**
     * Construct a directory/file iterator based on directory path and filters.
     *
     * @param  string $directoryPath
     * @param  array $filters Filters for {@link MonkCmsFileCacheRecursiveFilterIterator}.
     * @return RecursiveIteratorIterator
     */
    private function constructFileIterator($directoryPath, array $filters)
    {
        $iterator = new RecursiveDirectoryIterator(
            $directoryPath,
            FilesystemIterator::KEY_AS_PATHNAME |
            FilesystemIterator::CURRENT_AS_FILEINFO |
            FilesystemIterator::SKIP_DOTS
        );

        if ($filters) {
            $iterator = new MonkCmsFileCacheRecursiveFilterIterator($iterator, $filters);
        }

        $iterator = new RecursiveIteratorIterator(
            $iterator,
            // Only include files if there are filters; otherwise, list files
            // before their parent directories to make it easy to delete all.
            $filters ? RecursiveIteratorIterator::LEAVES_ONLY : RecursiveIteratorIterator::CHILD_FIRST
        );

        return $iterator;
    }

    /**
     * Delete all files and directories contained in an iterator.
     *
     * @param  RecursiveIteratorIterator $files
     * @return int Number of files deleted.
     * @throws MonkCmsCacheException If a file or directory cannot be deleted.
     */
    private function deleteFilesAndDirectories(RecursiveIteratorIterator $files)
    {
        $deleted = 0;

        foreach ($files as $file) {
            $pathname = $file->getPathname();

            if ($file->isDir()) {
                $success = rmdir($pathname);
            } else {
                $success = unlink($pathname);
                $deleted++;
            }

            if (!$success) {
                throw new MonkCmsCacheException("Failed to delete file/directory: {$pathname}");
            }
        }

        return $deleted;
    }

    public function delete(array $data = array())
    {
        $deleted = 0;

        $directoryPaths = glob($this->buildDirectoryPath($data), GLOB_BRACE);
        $filters = $this->buildFileFilters($data);

        foreach ($directoryPaths as $directoryPath) {
            $files = $this->constructFileIterator($directoryPath, $filters);

            $deleted += $this->deleteFilesAndDirectories($files);

            // Don't delete the directory that stores the entire cache.
            $deleteDirectoryPath = !$filters && $directoryPath != $this->getRootDirectoryPath();

            if ($deleteDirectoryPath && !rmdir($directoryPath)) {
                throw new MonkCmsCacheException("Failed to delete directory: {$directoryPath}");
            }
        }

        return $deleted;
    }
}

/**
 * A recursive file iterator used by {@link MonkCmsFileCacheAdapter} to filter
 * out unwanted files based on cache record data.
 */
class MonkCmsFileCacheRecursiveFilterIterator extends RecursiveFilterIterator
{
    /**
     * Filters to apply against files for inclusion or exclusion.
     *
     * @var array
     */
    private $filters;

    /**
     * Constructor.
     *
     * @param RecursiveIterator $iterator Iterator to be filtered.
     * @param array $filters Filters to apply against files for inclusion or
     *   exclusion.
     */
    public function __construct(RecursiveIterator $iterator, array $filters)
    {
        parent::__construct($iterator);

        $this->filters = $filters;
    }

    /**
     * Whether to include or exclude a specific file or directory.
     *
     * @return bool
     */
    public function accept()
    {
        // Necessary for recursion.
        if ($this->hasChildren()) {
            return true;
        }

        $current = $this->current();
        $filters = $this->filters;

        if (isset($filters['createdAt']) && $filters['createdAt'] < $current->getMTime()) {
            return false;
        }

        return true;
    }

    /**
     * Get children wrapped in this same iterator type.
     *
     * @return MonkCmsFileCacheRecursiveFilterIterator
     */
    public function getChildren()
    {
        return new self($this->getInnerIterator()->getChildren(), $this->filters);
    }
}

/**
 * Exception thrown by {@link MonkCmsCache} and {@link MonkCacheAdapter}.
 */
class MonkCmsCacheException extends Exception
{
}
