<?php
/*
*  ===========================================================================
*
*                            CLASS Monk_Directory
* 
*  ===========================================================================
*  
*  @public method output_filter()      - Outputs an A-Z filter based on populated letters in the "Directory" category
*  @public method output_pagination()  - Outputs the articles pagination
*  @public method output_articles()    - Outputs formatted articles in the "Directory" category
*  @private method output_category()   - Outputs articles for a desired category
*  @private method output_grouped()    - Outputs grouped articles
*  @private method split_by_initials() - Groups exploded getContent query by first letter
*  @description - This class provides methods to implement an A-Z Directory for articles
*  @todo        - Support additional module types 
*  @version 0.9.0
*
*/
class Monk_Directory { 
	
	/*
	*  ==========================================================================
	*
	*  output_filter() - Outputs A-Z filter for articles in "Directory" category
	* 
	*  ==========================================================================
	*  
	*  @param string $category - The current category (A-Z)
	*/
	
	public function output_filter($category)
	{
			
		$articlesQuery = getContent(
			'article',
			'display:list',
			'order:title',
			'find_category:Directory',
			'show:__title__',
			'show:|~', // |~ is a flag used to seperate articles
			'noecho'
		);
		
		// Isolate Articles
		$articles = explode('|~', $articlesQuery);
		
		
		// Get populated letters
		foreach( $articles as $key=>$value )
		{
			$letters[$key] = substr($value, 0, 1);
		}
		$letterCount = array_count_values($letters);
		
		print '<ul class="directoryFilter">';
		
		// Output 
		$letter = 'A';
		for($i = 0; $i<= 25; $i++)
		{	
			// Is this letter populated?
			if($letterCount[$letter] != 0)
			{
				// If category is current page
				if($category == $letter){
				
					print '<li class="current"><a href="?cat='.$letter.'">'.$letter.'</a></li>';
					
				} else { // Regular Output
				
					print '<li><a href="?cat='.$letter.'">'.$letter.'</a></li>';
					
				}
				
			} else {
			
				print '<li><span class="inactive">'.$letter.'</span></li>';
			}
			$letter++;
		} 
		print '</ul>';		
	}
	
	/*
	*  ==========================================================================
	*
	*  output_pagination() - Outputs the articles pagination
	* 
	*  ==========================================================================
	*  
	*/
	public function output_pagination()
	{
		$category = false;
		$pagination = true;
		
		$pagination = $this->output_articles($category, $pagination);
		
		print $pagination;
	}
	
	/*
	*  ==========================================================================
	*
	*  output_articles() - Outputs formatted articles in the "Directory" category
	* 
	*  ==========================================================================
	*  
	*  @param string $category - The current category (A-Z)
	*  @param boolean $pagination - True outputs only pagination, false outputs everything else
	*  @param int $howmany - How many articles to display
	*
	*/
	public function output_articles($category, $pagination, $howmany)
	{
		
		// Declarations
		$articlesQuery = getContent(
			'article',
			'display:list',
			'order:title',
			'find_category:Directory',
			'before_show:__pagination__|*', // |* is a flag that seperates pagination
			'howmany:'.$howmany,
			'show:__title__||__customazurl__|^__summary__',
			'show:__title__||__url__|^__summary__', // || seperates articles & URL. |^ Seperates description & URL
			'show:|~', // |~ is a flag that seperates articles
			'noecho'
		);
		
		$articles = explode('|~', $articlesQuery);
		$total = count($articles);
		
		// Handle Category Display
		if($category){
			
			print '<a href="/a-z-directory/" class="fullDirectory">Show Full Directory</a>';
			
			$this->output_category($category);
		}
		else{ // Displays full list
		
			// Handle Pagination
			if($total > 9){
				
				$articlesPagination = explode('|*', $articlesQuery);
				
			} 
					
			if($pagination)
			{
				//Return pagination
				return $articlesPagination[0];	
				
				
			} else {
				
				// Output grouped articles
				$articles = explode('|~', $articlesPagination[1]);
				$articles = $this->split_by_initials($articles);

				//print '<pre>';
				$this->output_grouped($articles);
				//print_r($articles);
				//print '</pre>';
				
			}
			
		}
		
	
	}


	/*
	*  ==========================================================================
	*
	*  output_category() - Outputs articles for a desired category
	* 
	*  ==========================================================================
	*  
	*  @param string $category - The category (A-Z) to display
	*
	*/
	private function output_category($category)
	{
		
		$articlesQuery = getContent(
			'article',
			'display:list',
			'order:title',
			'find_category:Directory',
			'show:__title__||__customazurl__|^__summary__',
			'show:__title__||__url__|^__summary__', // || seperates articles & URL. |^ Seperates description & URL
			'show:|~', // |~ seperate articles
			'noecho'
		);
		
		// Isolate Articles
		$articles = explode('|~', $articlesQuery);
		$articles = $this->split_by_initials($articles);
		
		foreach( $articles as $letter=>$articleArray)
		{
			if($letter == $category)
			{
				print '<h3>'.$letter.'</h3>';
				print '<ul>';
				foreach( $articleArray as $article)
				{
					$article = explode('||', $article);
					$descrition = explode('|^', $article[1]);
					print '<li><a href="'.$descrition[0].'">'.$article[0].'</a><p>'.$descrition[1].'</p></li>';
				}
				print '</ul>';
				break;
			}
		}
		
	}
	
	
	/*
	*  ==========================================================================
	*
	*  output_grouped() - Outputs grouped articles
	* 
	*  ==========================================================================
	*  
	*  @param array $articles - An array of articles
	*
	*/
	private function output_grouped(array $articles)
	{	
		foreach( $articles as $letter=>$articleArray)
		{
			print '<h3>'.$letter.'</h3>';
			print '<ul>';
			foreach( $articleArray as $article)
			{
				$article = explode('||', $article);
				$descrition = explode('|^', $article[1]);
				
				print '<li><a href="'.$descrition[0].'">'.$article[0].'</a><p>'.$descrition[1].'</p></li>';
			}
			print '</ul>';
		}
	}
	
	
	/*
	*  ==========================================================================
	*
	*  split_by_initials() - Groups exploded getContent query by first letter
	* 
	*  ==========================================================================
	*  
	*  @param array $articles - An array of articles
	*
	*/
	private function split_by_initials(array $articles)
	{
		//Variables
		$total = count($articles);
		$result = '';
		
		// Sort by initial
		foreach($articles as $article) {
	        $result[$article[0]][] = $article;
	    }
	
	    return $result;
	}
	
}
?>