<?php
/**
 *  ===========================================================================
 *
 *                            CLASS Monk_Site_Status
 *
 *  ===========================================================================
 *
 *  Provides site launch status booleans.
 *
 *  Pass $MCMS_SITENAME and easyEditOn() via monkcms.php when creating object.
 *
 *  Example:
 *  $site_status = new Monk_Site_Status(array(
 *  	'site_name' => $MCMS_SITENAME,
 *  	'easy_edit' => isEasyEditOn()
 *  ));
 *  if($site_status->is_live){
 *  	echo 'This site live!';
 *  }
 *
 *  @author Chris Ullyott
 *  @date Nov 2014
 *  @copyright 2014 Monk Development
 *  @dependencies monkcms.php
 */

class Monk_Site_Status {


	public function __construct($params){

		$this->is_live         = true;
		$this->is_staging      = false;
		$this->is_demo         = false;

		$this->wardrobe_show   = false;
		$this->wardrobe_enable = false;

		$this->site_name       = $params['site_name'];
		$this->easy_edit       = $params['easy_edit'];

		// set the launch or demo status
		// a demo site must also be in staging
		if(self::launch_status()=='staging'){
			$this->is_live = false;
			$this->is_staging = true;
			if(self::is_demo($this->site_name)){
				$this->is_demo = true;
			}
		}

		// show/enable the color picker
		if ($this->easy_edit || $this->is_demo){
			$this->wardrobe_show = true;
			if ($this->easy_edit && !$this->is_demo){
				$this->wardrobe_enable = true;
			}
		}

	}


	// query the API for the site's launch status
	public function launch_status(){
		$launch_status = getcontent(
			'site',
			'display:detail',
			'show:__isstaging__staging',
			'show:__islive__live',
			'noecho',
			'noedit'
		);
		return trim($launch_status);
	}


	// check by the site name if this is a demo
	public function is_demo($site_name){
		$is_demo = false;
		$demo_pattern = '^(mk|ekk)\d{3,}$'; // matches "ekk125", "MK021"...
		if(preg_match("/$demo_pattern/i", trim($site_name))){
			$is_demo = true;
		}
		return $is_demo;
	}


	// outputs the robots meta tag for a client site in staging
	public function robots_meta_tag(){
		if($this->is_staging && !$this->is_demo){
			echo "<meta name=\"robots\" content=\"noindex, nofollow\" />\n";
		}
	}


	// returns class names for the color picker
	public function wardrobe_class(){
		$wardrobe_class = '';
		if($this->wardrobe_show){
			$wardrobe_class .= ' wardrobe-show';
		}
		if($this->wardrobe_enable){
			$wardrobe_class .= ' wardrobe-enable';
		}
		return $wardrobe_class;
	}


	// returns a class name for the setup guide notice
	public function setupguide_class(){
		if($this->is_staging && $this->wardrobe_show && !$this->is_demo){
			return ' setup-guide-notice';
		}
	}


}

?>