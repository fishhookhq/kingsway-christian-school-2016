<?php 

/**
*
* Abstracts generic helper functions
* 
*/
class Helper
{
	
	/**
   * Returns Base Url, useful for sermons and articles.
   * 
   * @return string - base url
   */
   public static function baseUrl()
   {
     	$wildcard_arr = explode(':',$_GET['wildcard'],2);
     	$root = rtrim($wildcard_arr[0],'/');

     	return $root;
   }
   
   /**
   * Get "src" attribute from html string
   * @param  html $string html string
   * @return string       src attribute
   */
   public static function srcAttribute($string)
   {
    	$result = array();
    	preg_match( '/src="([^"]*)"/i', $string, $result ) ;
    	return $result[1];
   }
  
  
	/**
	 * Truncates a string and adds ellipse
	 * 
	 * @param  string  $string       - String to Operate on
	 * @param  int 	$length 	     - Desired character amount
	 * @param  boolean $stopanywhere - Stop on any character
	 * @return string                - Truncated String
	 */
	public static function truncate($string, $length, $stopanywhere=false)
	{
		// truncates a string to a certain char length, stopping on a word if not specified otherwise.
	    if (strlen($string) > $length) {
	        // limit hit!
	        $string = substr($string,0,($length -3));
	        if ($stopanywhere) {
	            // stop anywhere
	            $string .= '...';
	        } else{
	            // stop on a word.
	            $string = substr($string,0,strrpos($string,' ')).'...';
	        }
	    }
	    return $string;
	}
  
	/**
	 * Adds a unique time stamp to a file to prevent caching
	 * 
	 * @param  string $file - path to the file relative to root
	 * @return string       - file path with appended timestamp
	 */
	public static function fileTimestamp($file)
	{
		$file_path = $_SERVER['DOCUMENT_ROOT'] . $file;
		if(file_exists($file_path))
		{
			return $file . '?t=' . date('YmdHis',filemtime($file_path));
		} 
		else 
		{
			return $file;
		}
	}

	/**
   * Slugifies a string
   * 
   * @param  string $string - String to be slugified
   * @return string         - Slug
   */
  public static function createSlug($string) {

    // remove spaces, tabs, linebreaks from the begining and the end
    $string = trim($string);

    // array of patters to change characters with the following symbols
    $pattern = array('$(à|á|â|ã|ä|å|À|Á|Â|Ã|Ä|Å|æ|Æ)$',
            '$(è|é|é|ê|ë|È|É|Ê|Ë)$',


            '$(ì|í|î|ï|Ì|Í|Î|Ï)$',
            '$(ò|ó|ô|õ|ö|ø|Ò|Ó|Ô|Õ|Ö|Ø|œ|Œ)$',
            '$(ù|ú|û|ü|Ù|Ú|Û|Ü)$',
            '$(ñ|Ñ)$',
            '$(ý|ÿ|Ý|Ÿ)$',
            '$(ç|Ç)$',
            '$(ð|Ð)$',
            '$(ß)$');
    // array of replacements
    $replacement = array('a',
            'e',
            'i',
            'o',
            'u',
            'n',
            'y',
            'c',
            'd',
            's');

    // Convert special chars to ascii and remove them
    $string = htmlentities($string, ENT_QUOTES, "UTF-8");
    $search = array (
      '@(&(#?))[a-zA-Z0-9]{1,7}(;)@'   // Strip out any ascii
    );

    $replace = array (
      ''
    );

    $string = preg_replace($search, $replace, $string);
    // all funny characters in the string get replaced with the
    // equivalent in ascii.
    $string =  preg_replace($pattern, $replacement, $string);

    $search = array (
      '@<script[^>]*?>.*?</script>@si', // Strip out javascript
      '@<[\/\!]*?[^<>]*?>@si',          // Strip out HTML tags
      '@(\s-\s)@',                      // Strip out space dash space
      '@[\s]{1,99}@',                   // Strip out white space
      '@—@',                            // Replace em dash with regular one
      '@[\\\/)({}^\@\[\]|!#$%*+=~`?.,_;:]@' // Things not covered by htmlentities
    );

    $replace = array (
      '',
      '',
      '-',
      '-',
      '-',
      ''
    );

    $string = preg_replace($search, $replace, $string);

    $string = strtolower($string);

    return $string;
  }
  
  /**
   * Converts a slug to a capitalized string
   * 
   * @param  string $string - slug
   * @return string         - deslugified string
   */
  public static function deSlugify($string)
  {
    $string = str_replace("-", " ", $string);
    $string = ucwords($string);
    return $string;
  }
   
  
}//end class

?>