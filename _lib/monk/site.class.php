<?
class site
{
	/**
    * Get client logo campus
    *
    * @return string         - logo url
    */
  	public static function logo()
	  {
	   $string = getContent(
			"site",
			"display:detail",
			"show:__logourl__",
			"noecho",
			"noedit"
		 );
		return $string;
	  }
  
 	/**
    * Get client timezone campus
    *
    * @return string         - client timezone
    */
	 public static function timezone()
	  {
	   $string = getContent(
			"site",
			"display:detail",
			"show:__timezone__",
			"noecho",
			"noedit"
		 );
		return $string;
	  }


}//end class

?>