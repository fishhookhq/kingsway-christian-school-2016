<?php
/*
*  ===========================================================================
*
*                            CLASS Monk_HTTPS
* 
*  ===========================================================================
*  
*  @public method toHTTPS()      - Rewrite a string with http:// to https//
*  @public method getSContent()  - Get the secure version of values returned by a getContent call.
*  @public method Sinclude()     - Includes a file but re-writes source calls to http to https
*  @description - This class provides methods to output CMS content in https
*  @version 1.0.1
*  @authors      - Kenny Kaye, Ricky Ybarra
*
*  WARNING - If you have variables set in your script that are used in included files (e.g. a page title in 
*  the head) those aren't gonna be avaiable unless you make them globals :(
*
*/
class Monk_HTTPS { 
	
	/*
	*  ==========================================================================
	*
	*  toHTTPS() - Rewrite a string with http:// to https//
	* 
	*  ==========================================================================
	*  
    *
    * @param string - the string to do the operation on.
    * @param check - boolean - whether to check if currently on a secure page or not, defaults to true
    *
    * @return - a string with http replaced with https for all page content (but not link destinations)
 	*/
	
	public function toHTTPS($string, $check = true){
	 
		$proceed = false;
		if($check){
			$proceed = ($_SERVER["HTTPS"] == "on") ? true : false;
		}
		else{
		 	$proceed = true;  
		}
		 
		if($proceed){
			$result = str_replace('src="http://', 'src="https://', $string); // src="  - images, js, files, misc...
		 	$result = str_replace("src='http://", "src='https://", $result); // src=' - images, js, files, misc...
		 	$result = str_replace("url('http://", "url('https://", $result); // url('  - for css
		 	$result = str_replace('url("http://', 'url("https://', $result); // url("  - for css
		}
		else{
		 	$result = $string;
		}
		 
	 	return $result;
	}	
	
	/*
	*  ==========================================================================
	*
	*  getSContent() - Get the secure version of values returned by a getContent call.
	* 
	*  ==========================================================================
	*
	* The parameters are variable to match the same format as getContent
	* Returns what getContent returns with http replaced with https for all 
	* page content (but not link destinations)
	*
	*/
	public function getSContent(){
	
	 	$args = func_get_args();
	 	ob_start();
	 	$ret = call_user_func_array("getContent", $args);
	 	if(!$ret){
	  		//the getContent call echoed instead of returned
	  		$output = $this->toHTTPS(ob_get_contents(), true);
	  	}
	  	else{
	   		$ret = $this->toHTTPS($ret, true);
	   	}
	   	ob_end_clean();
	   	if($output){
	    	print($output);  
	    }
	    return $ret; 
	}
	

	/*
	*  ==========================================================================
	*
	*  Sinclude() - includes a file but re-writes source calls to http to https
	* 
	*  ==========================================================================
	*
	* returns what include would return, otherwise, it prints what include would print
	* but with http replaced with https for all page content (but not link destinations)
	*/
	public function Sinclude($file){
		ob_start();
		$ret = include($file);
		$output = $this->toHTTPS(ob_get_contents(), true);
		ob_end_clean();
		print($output);
		return $ret;
	}
}
?>