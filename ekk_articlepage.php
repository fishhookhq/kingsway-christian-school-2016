<?php require_once($_SERVER["DOCUMENT_ROOT"]."/_inc/config.php");
$meta = new pageMeta("index", $MCMS_SITENAME);

//billboard
$billboard = getContent("media","display:detail","find:".$_GET['nav'],"label:header","show:__imageurl maxWidth='2560'__", "noecho" );
if($billboard){$bodyClass = 'hasbillboard'; $billboardClass = 'skew skew-light'; $billboardStyle = 'style="background-image: url('.$billboard.');"';}
$nosections = getContent("page","find:".$_GET['nav'],"show:__customnosections__","show:hidesections","noecho","noedit", "nocache");

if($nosections == 'hidesections'){
  $nosections = 'show sections';
}
?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/head.php"); ?>
    <?php 
      $custom_seo_title = getContent("page","find:".$_GET['nav'],"show:__customseotitle__","noecho","noedit", "nocache");
      $page_title = ($custom_seo_title != "" && $custom_seo_title != "__customseotitle__") ? $custom_seo_title : $meta->page_title.' | Kingsway Christian Church';
    ?>
    <title><?= $page_title ?></title>
    <meta name="description" content="<?= $meta->page_description ?>"/>
    <meta name="keywords" content="<?= $meta->page_keywords ?>"/>
    
    <meta property="og:description" content="<?= $meta->page_description ?>">
    <meta property="og:title" content="<?= $page_title ?>">
    
    <meta name="twitter:description" content="<?= $meta->page_description ?>">
    <meta name="twitter:title" content="<?= $page_title ?>">
    <meta property="og:image" content="<?= $billboard ?>">
    <meta name="twitter:image:src" content="<?= $billboard ?>">
  </head>
  
  <body id="page" class="page">
    
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/header_subpage.php"); ?>
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/billboard_article.php"); ?> 
  	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/subnav.php"); ?>	


  
<div id="page_content">
  	
<!-- Page Content -->
<div id="article-summary" class="row align-center">
		<div class="small-11 medium-9 medium-centered columns">
  		<?php getContent(
    		"article",
    		"display:detail",
    		"find:".$_GET['slug'],
    		"show:__text__"
    		); ?>
		</div>
	</div>
	</div> <!-- #page_content -->    
	
 	    
	<!-- Page Footer -->
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/footer.php"); ?>
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/scripts.php"); ?>
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/analytics.php"); ?>
	<script type="text/javascript" src="/_js/jquery.accordion.js"></script>
  <script type="text/javascript" src="/_js/jquery.easing.1.3.js"></script>
	
<script type="text/javascript">

$(window).load(function(){
  // Image with caption
  $("#page_content img.wcaption").each(function(){
     var caption = $(this).attr("alt");
     $(this).wrap("<figure></figure>").after("<figcaption><span>"+caption+"</span></figcaption>");
  });
});

$(function () {
  $('.st-accordion').accordion({
    oneOpenedItem: true
  });
  if(window.location.hash) {
    var hash = window.location.hash;
    $('#st-accordion a[href="' + hash + '"]').trigger('click');
  }
      //alert(hash);

});

</script>

  </body>
</html>
