<?php require_once($_SERVER["DOCUMENT_ROOT"]."/_inc/config.php");
$meta = new pageMeta("index", $MCMS_SITENAME);

//echo $_GET['wildcard'];
$wcvars = explode("/",trim($_GET['wildcard'], "/"));
$$wcvars[0] = $wcvars[1];
if(isset($date) && $date != "") {
  $dateArray = explode("-", $date);
  $month = date("m", strtotime($dateArray[0]." 1, ".$dateArray[1]));
  $year = $dateArray[1];
}
//billboard
$billboard = getContent("media","display:detail","find:".$_GET['nav'],"label:header","show:__imageurl maxWidth='2560'__", "noecho" );
if($billboard){$bodyClass = 'hasbillboard'; $billboardClass = 'skew skew-light'; $billboardStyle = 'style="background-image: url('.$billboard.');"';}

?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/head.php"); ?>
    <?php 
      $custom_seo_title = getContent("page","find:".$_GET['nav'],"show:__customseotitle__","noecho","noedit", "nocache");
      $page_title = ($custom_seo_title != "" && $custom_seo_title != "__customseotitle__") ? $custom_seo_title : $meta->page_title.'';
    ?>
    <title><?= $page_title ?></title>
    <meta name="description" content="<?= $meta->page_description ?>"/>
    <meta name="keywords" content="<?= $meta->page_keywords ?>"/>
    
    <meta property="og:description" content="<?= $meta->page_description ?>">
    <meta property="og:title" content="<?= $page_title ?>">
    
    <meta name="twitter:description" content="<?= $meta->page_description ?>">
    <meta name="twitter:title" content="<?= $page_title ?>">
    <meta property="og:image" content="<?= $billboard ?>">
    <meta name="twitter:image:src" content="<?= $billboard ?>">
  </head>
  
  <body id="events" class="events page">
    
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/header_subpage.php"); ?>
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/billboard.php"); ?> 

<?php if ($_GET['wildcard'] == "/") { // show featured events if no filters have been applied ?>
<div id="page_content">  	
<!-- Page Content -->
  <div class="row align-center">
    <div class="medium-10 columns">
      
  <div class="content active" id ="featured_events">
    <?php  getContent(
    	   "event",
    	   "display:list",
    	   "features",
    	   //"find_group:".$meta->page_group_slug,
    	   "howmany:4",
    	   //"offset:1",
    	   'before_show:<div class="row small-up-1  medium-up-2">',
    	   "show:  <div class='feat-event column text-center align-middle'>",
    	   "show:<a href='__url__'>",
    	   "show:<div class='eventimg'><img src=\"__imageurl width='580' height='410'__\" alt=\"__title__\" class=\"overlay\" /></div>", 
    	   "show:</a>",
    	   "show:<div class='event-meta'>",
    	   "show:<div class='event-date'>__eventstart format='M j'__</div>",
    	   "show:<h2 class='event-title'><span>__title__</span></h2>",
    	   "show:<span class='icon-plus'></span>",
    	   "show: </div>",
    	   "show:</div>",
    	   "after_show:</div>"
    	);
    ?>
  </div>
	
    </div>
  </div>
</div> <!-- #page_content -->    
<?php } ?>
    	
  <!-- event filters -->  	
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/event-filters.php"); ?>


	<div class="content" id="events_list">
  	<div class="row align-center events" id="events-grid">
    	<div class="medium-9 columns eventlist">
				 
      <?php
      
      $eventmonth = getContent(
        "event",
				"display:list",
				"find_group:".$group,
				"find_category:".$category,
				"month:".$month,
				"year:".$year,
				//"howmany:".$howmany,
				"howmanydays:365",
				"groupby:month",
				"group_show:__slug__",
				"group_show:~~",
				"group_show:__date format='n'__",
				"group_show:~~",
				"group_show:__dateTwo format='Y'__",
				"group_show:||",
				"noecho"
      );
        //print_r($eventmonth);
        list($month_list, $pagination) = explode("!!",$eventmonth);
        $month_list = explode("||", trim($month_list,"||"));
      
     
			
			
        foreach($month_list as $event_info){
          list($eSlug,$eMonth,$eYear) = explode("~~", trim($event_info,"~~"));
          $eSlug = strtolower($eSlug);

          list($mMonth) = explode("-", trim($eSlug,"-"));                    
          
          echo "<div class='month' id='".$eSlug."'>";
            
            echo "<div class='calendar-month'>";
         	  echo "<span class=\"calendar-prev icon icon-cal-prev\"></span>";
            echo "<h3 class='monthName'>".$mMonth." events</h3>";
						echo "<span class=\"calendar-next icon icon-cal-next\"></span>";
            echo "</div>";
            
            
			$events = getContent(
  			"event",
				"display:list",
        "enablepast:no",
				"find_group:".$group,
				"find_category:".$category,
				"month:".$eMonth,
				"year:".$eYear,
				"howmanydays:365",
        "groupby:month",
        
				//"page:".$_GET['page'],
//				"offset:".$offset,
				"show:__slug__",
				"show:~~",
				"show:__title__",
				"show:~~",
				"show:__url__",
				"show:~~",
				"show:__summary__",
				"show:~~",
				"show:__description__",
				"show:~~",
				"show:__category__",
				"show:~~",
				"show:__eventstart format='D'__",
				"show:~~",
				"show:__startday__",
				"show:~~",
				"show:__eventstartTwo format='l, F j, Y'__",
				"show:~~",
  			"show:__eventend format='Ymd H:i:s'__",
				"show:~~",
				"show:__eventstartThree format='g:i a'__ - __eventend format='g:i a'__",
				"show:~~",
				"show:<li><span>contact</span> __coordname__</li>",
				"show:~~",
				"show:__import__",
				"show:~~",
				"show:__isrecurring__",
				"show:~~",
				"show:__isallday__",
				"show:~~",
				"show:<li><span>location</span> __location__</li>",
        "show:~~",
        "show:<li>__customregisterlink__</li>",
        "show:~~",
        "show:<li>__import__</li>",
        "show:~~",
        "show:<li>__website__</li>",
        "show:~~",
        "show:__customcustomdate__",
        "show:~~",
				"show:||",
				"noecho"
			);
			
			$eventdetail = explode("||", trim($events,"||"));
			foreach($eventdetail as $event){
      list($e_slug,$e_title,$e_url,$e_summary,$e_description,$e_category,$e_eventstart,$e_startday,$e_eventdate,$e_eventend,$e_eventtimes,$e_coordname,$e_import,$e_isrecurring,$e_isallday,$e_location,$e_register,$e_ics,$e_website,$e_customdate) = explode("~~", trim($event,"~~"));
      
        
            if($e_customdate){
              $eventdate = $e_customdate;
              $eventtime = '';
            }else{
              $eventdate = $e_eventdate;
              $eventtime = '<li><span>time</span> '. $e_eventtimes .'</li>';

            }
      
            echo "<div class='row eventRow event'>";
						echo "<div class='small-3 medium-2 columns align-middle'>";
						  echo "<p class='event-date'>".$e_eventstart."<span>".$e_startday;
              echo "</span></p>";

						echo "</div>";
								
						echo "<div class='columns title align-middle'>";
						  echo "  <a href='".$e_url."' class='showdetail'>";
              echo "    <h1>".$e_title."</h1>";
              echo "   </a>";
            echo "</div>";
            
            echo "<div class='small-2 medium-1 columns right arrow'>";
              echo"<span><i class='icon-arrow-right showdetail'></i></span>";
            echo "</div>";
            
              echo "<div class='eventdetail'>";
                echo "  <div class='content_holder'><div class='row align-center'><div class='medium-8 column'>";
                  //echo "    <a href='' class='closer'><p><span class='icon-x'></span></p></a>";
                  echo "<ul class='event-info'>";
                  echo "<li><span>date</span> ". $eventdate ."</li>";
                  echo $eventtime;
                  echo $e_location;
                  echo $e_coordname;
                  echo "</ul>";
                  echo "<p>".$e_description."</p>";
                  echo "<div class='cats'>".$e_category."</div>";
                 // echo $event['url'];
                  echo "<ul class='action-items'>";
                  
                  echo "<li><a class='button green light' href='".$e_register."' target='_blank'>Register</a></li>";
                  echo "<li><a class='button dark' href='".$e_ics."'><span></span><span class='caption'>Add to My Calendar</span></a></li>";
                  echo "<li><a class='button dark addthis_button_compact' addthis:url=\"http://www.kingswayschool.org__url__\" addthis:title=\"__title__\"><span></span><span class='caption'>Share</span></a></li>";
                  echo "</ul>";
                 
                echo "  </div></div>";//end eventdetail

              echo "</div>";
            echo "</div>";
					echo "</div>";

      }
    	echo "</div>";
      }
	?>

		  </div><!--end .eventlist -->
	  </div><!--end #events_grid -->
	</div><!-- end .events_list -->	
	
	
  <!-- Page Sections -->
<!-- 	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/sections.php"); ?> -->
	    
	<!-- Page Footer -->
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/footer.php"); ?>
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/scripts.php"); ?>
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/analytics.php"); ?>
	<script type="text/javascript" src="/_js/jquery.accordion.js"></script>
  <script type="text/javascript" src="/_js/jquery.easing.1.3.js"></script>

  <script type="text/javascript">
$(document).ready(function(){
  var eventGrid = $("#events-grid"),
      fullWidth = $(eventGrid).width(),
      rowPosition = $(eventGrid).find(".eventlist").position();
      console.log(rowPosition);
	$('.event').each(function() {
		var offsetleft = $(this).position().left*-1;
			console.log(offsetleft + 'left');
			positionLeft = offsetleft - rowPosition.left -10;
			console.log(positionLeft + 'pleft');
		$(this).find('.eventdetail').
			css({"width":fullWidth,"left":positionLeft});
	});
	$('.showdetail, .closer').click(function(e) {
		e.preventDefault();
		var currentevent = $(this).closest('div.event');
				
		var currenteventid = currentevent.attr("id")
		
		if (currentevent.hasClass('open')) { //close current event, full opacity
			currentevent.find('.eventdetail > .content_holder').slideUp();
			currentevent.find('.eventdetail').slideUp();
			currentevent.removeClass('open');

		} else { //open current event, dim all others
			//currentevent.css("opacity",1);
			$('.eventdetail > .content_holder').hide();
			$('.eventdetail').hide();
			$('.event').removeClass('open');
			currentevent.find('.eventdetail').show();
			currentevent.find('.eventdetail > .content_holder').slideDown();
			currentevent.addClass('open');
		}
		
	});	
});
</script>
<script type="text/javascript">
	/*** Calendar Nav ***/

	$("#events_list .calendar-month span.calendar-prev").each(function(){
		if($(this).parents(".month").prev().hasClass("month")) {
		  $(this).on("click", function(){
			  $(this).parents(".month").hide().prev(".month").show();
		  });
		} else {
			$(this).hide();
		}
	});
	$("#events_list .calendar-month span.calendar-next").each(function(){
		if($(this).parents(".month").next().hasClass("month")) {
		  $(this).on("click", function(){
			  $(this).parents(".month").hide().next(".month").show();
		  });
		} else {
			$(this).hide();
		}
	});
	
 	$(function(){
  	$('.sorter').change(function (event) {
          event.preventDefault();
          window.location = "/events/" + this.name + "/" + this.value;
      });
	});
</script>


  </body>
</html>
