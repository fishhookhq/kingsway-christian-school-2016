<?php require_once($_SERVER["DOCUMENT_ROOT"]."/_inc/config.php");
$meta = new pageMeta("index", $MCMS_SITENAME);

//billboard
$billboard = getContent("media","display:detail","find:".$_GET['nav'],"label:header","show:__imageurl maxWidth='2560'__", "noecho" );
if($billboard){$bodyClass = 'hasbillboard'; $billboardClass = 'skew skew-light'; $billboardStyle = 'style="background-image: url('.$billboard.');"';}
$nosections = getContent("page","find:".$_GET['nav'],"show:__customnosections__","show:yessir","noecho","noedit", "nocache");

if($nosections != 'yessir'){
  $nosections = 'blah';
}
?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/head.php"); ?>
    <?php 
      $custom_seo_title = getContent("page","find:".$_GET['nav'],"show:__customseotitle__","noecho","noedit", "nocache");
      $page_title = ($custom_seo_title != "" && $custom_seo_title != "__customseotitle__") ? $custom_seo_title : $meta->page_title.' | Kingsway Christian Church';
    ?>
    <title><?= $page_title ?></title>
    <meta name="description" content="<?= $meta->page_description ?>"/>
    <meta name="keywords" content="<?= $meta->page_keywords ?>"/>
    
    <meta property="og:description" content="<?= $meta->page_description ?>">
    <meta property="og:title" content="<?= $page_title ?>">
    
    <meta name="twitter:description" content="<?= $meta->page_description ?>">
    <meta name="twitter:title" content="<?= $page_title ?>">
    <meta property="og:image" content="<?= $billboard ?>">
    <meta name="twitter:image:src" content="<?= $billboard ?>">
  </head>
  
  <body id="page" class="page">
    
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/header_subpage.php"); ?>
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/billboard.php"); ?> 
  	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/subnav.php"); ?>	


  
<div id="page_content">
  	
<!-- Page Content -->
<?php
$page_content_grid = "medium-9 medium-centered";
$get_page_data = getContent(
	"page",
	"find:".$_GET['nav'],
	'show:__title__',
	'show:||',
	'show:__text__',
	'noecho',
	'noedit'
);

	$page_item_arr = explode('||',$get_page_data);

	list($p_title,$p_text) = $page_item_arr;

	$page_output .= "\n\t<div class='row align-center'>";
	$page_output .= "\n\t\t<div class='".$page_content_grid." columns'>";
	$page_output .= "\n\t\t\t<h2 class='page_title'>$p_title</h2>";
	$page_output .= "\n\t\t</div>";
	$page_output .= "\n\t</div>";

	$html = str_get_html($p_text);
	if($html){
		foreach($html->find('img.fullwidth') as $element) {
			$img_url = explode("?",$element->src);
			$parent_element = $element->parent();
			$prev_sibling = $element->prev_sibling();
			$next_sibling = $element->next_sibling();
				if ($img_url[1]){
				$img_query_string = explode("&",$img_url[1]);
				foreach($img_query_string as $qs) {
					$qs = str_replace("amp;", "", $qs);
					$qs = explode("=", $qs);
					$$qs[0]=$qs[1];
				}
			$img_alt = $element->alt;
			$img_slug = preg_match("/([^.]+)-[0-9]+-[0-9]+-[0-9]+-[0-9]+/i",$fileName,$match);
			$orig_img_url = getContent(
				"media",
				"display:detail",
				"find:".$match[1],
				"show:__imageurl maxWidth='1600'__",
				"noecho"
			);
			} else {
				$orig_img_url = $element->src;
			}
			$section_caption = "";
			if ($element->alt) {
				$section_caption = "\n\t\t\t<div class=\"caption text-center\"><div class=\"box\"><h1>" .$element->alt. "</h1></div></div>";
			}
			$parent_element->outertext = $prev_sibling."\n\t</div>\n</div>\n<div class=\"separator fullwidth\" style=\"background-image: url('".$orig_img_url."')\">\n\t<div class=\"row align-center\">\n\t\t<div class=\"".$page_content_grid." columns\">".$section_caption."\n\t\t</div>\n\t</div>\n</div>\n<div class=\"row align-center\">\n\t<div class=\"".$page_content_grid." columns\">".$next_sibling;
			$element->alt = null;
  	}
  	
  	foreach($html->find('#featured-events-grid') as $element) {
			$parent_element = $element->parent();
			$prev_sibling = $element->prev_sibling();
			$next_sibling = $element->next_sibling();
			$parent_element->outertext = $prev_sibling."\n\t</div>\n</div>\n".$element."\n<div class=\"row\">\n\t<div class=\"".$page_content_grid." columns\">".$next_sibling;
    }
    
		echo "\n<div class='row align-center'>";
  	echo "\n\t<div class='".$page_content_grid." columns'>";
	 	echo $html;
	 	$html->clear();
    echo "\n\t</div>";
    echo "\n</div>";
	}
?>	
	</div> <!-- #page_content -->    
	
  <!-- Page Sections -->
  
	<?php
		$isSection = getContent(
		"section",
		"display:detail",
		"find:".$_GET['nav'],
		"label:Section 1",
		"show:__title__",
		"noecho"
		);
		
		if ($isSection != ''){
			include($_SERVER["DOCUMENT_ROOT"]."/_inc/sections.php");
		}
	?>
	    
	<!-- Page Footer -->
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/footer.php"); ?>
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/scripts.php"); ?>
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/analytics.php"); ?>
	<script type="text/javascript" src="/_js/jquery.accordion.js"></script>
  <script type="text/javascript" src="/_js/jquery.easing.1.3.js"></script>
	
<script type="text/javascript">

$(window).load(function(){
  // Image with caption
  $("#page_content img.wcaption").each(function(){
     var caption = $(this).attr("alt");
     $(this).wrap("<figure></figure>").after("<figcaption><span>"+caption+"</span></figcaption>");
  });
});

$(function () {

    $('.st-accordion').accordion({
    });
  if(window.location.hash) {
    var hash = window.location.hash;
    $('#st-accordion a[href="' + hash + '"]').trigger('click');
  }
      //alert(hash);

});

</script>

  </body>
</html>
