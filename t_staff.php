<?php require_once($_SERVER["DOCUMENT_ROOT"]."/_inc/config.php");
$meta = new pageMeta("page", $MCMS_SITENAME);

//billboard
$billboard = getContent("media","display:detail","find:".$_GET['nav'],"label:header","show:__imageurl maxWidth='2560'__", "noecho" );
if($billboard){$bodyClass = 'hasbillboard'; $billboardClass = 'skew skew-light'; $billboardStyle = 'style="background-image: url('.$billboard.');"';}

?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/head.php"); ?>
    <?php 
      $custom_seo_title = getContent("page","find:".$_GET['nav'],"show:__customseotitle__","noecho","noedit", "nocache");
      $page_title = ($custom_seo_title != "" && $custom_seo_title != "__customseotitle__") ? $custom_seo_title : $meta->page_title.' | Kingsway Christian Church';
    ?>
    <title><?= $page_title ?></title>
    <meta name="description" content="<?= $meta->page_description ?>"/>
    <meta name="keywords" content="<?= $meta->page_keywords ?>"/>
    
    <meta property="og:description" content="<?= $meta->page_description ?>">
    <meta property="og:title" content="<?= $page_title ?>">
    
    <meta name="twitter:description" content="<?= $meta->page_description ?>">
    <meta name="twitter:title" content="<?= $page_title ?>">
    <meta property="og:image" content="<?= $billboard ?>">
    <meta name="twitter:image:src" content="<?= $billboard ?>">
  </head>
  
  <body id="page" class="page">
    
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/header_subpage.php"); ?>
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/billboard.php"); ?> 
  <?php $teams = getContent(
  		"member",
  		"display:list",
  		"find_group_series:staff",
  		"order:position",
  		"before_group_show:<option value='__slug__'>__title__</option>",
  		"noecho",'noedit'
  		);
		?>
<div id="filter_row" class="filters">
  <div class="row align-center">
    <div class="shrink columns">
      <span>Sort by</span>
    </div>
    <div class="medium-8 columns">
      <div class="row">
        <div class="small-11 medium-6 columns">
          <select class="filters-select sorter">
<!--           <option value="all-staff">show all</option> -->
            <?php echo $teams; ?>
         </select>
        </div>
        <div class="small-11 medium-6 columns">
          <form>
            <fieldset>
    			<input type="text" id="module-search-term" name="search" placeholder="Search" />
    			<button type="submit" name="submit" id="module-search-submit" value="" class="icon-search"></button>
<!--     			<button type="reset" name="reset" id="module-search-reset" class="icon-x"></button> -->
    		</fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="page_content">
<!-- Staff Grid -->
<?php

$get_members_data = getContent(
	"members",
	"display:list",
	"find_group:all-staff",
	"group_order:position",
	"order:position",
	'show:__id__',
	'show:||',
	"show:__photourl height='800' width='800'__",
	'show:||',
	'show:__fullname__',
	'show:||',
	'show:__position__',
	'show:||',
	'show:__groups__',
	'show:||',
	'show:__about__',
	'show:||',
	'show:__emailaddress__',
	'show:~|~|~',
	'noecho'
);

$members_data_arr = explode("~|~|~", $get_members_data);
$members_data_arr = array_filter($members_data_arr);

?>

  <div id="staff_grid">
    <div class="row expanded small-up-1 medium-up-3 large-up-4">
<?php foreach($members_data_arr as $members_item){

	list($m_id, $m_photourl, $m_fullname, $m_position, $m_groups, $m_about, $m_emailaddress) = explode('||',$members_item);

  $m_class_array = explode(",", $m_groups);
  $m_classes = '';
  foreach($m_class_array as $mc) {
    $m_classes .= ' '.Helper::createSlug($mc);
  }

  $members_output .= '<div class="member_box column'.$m_classes.'">';
	$members_output .= '<a class="portrait_box" href="#bio-'.$m_id.'">';
	$members_output .= '<div class="staff_member align-center align-bottom" style="background-image:url('.$m_photourl.')">';
	$members_output .= '<div class="member_info">';
	$members_output .= "<span class='fullname'>$m_fullname</span>";
	$members_output .= "<span class='position'>$m_position</span>";
	$members_output .= "</div>";
	$members_output .= "</div>";
	$members_output .= "</a>";
	$members_output .= "</div>";
	
}
echo $members_output;
?>
    </div>
  </div>
  
</div> <!-- #page_content -->    
	
<!-- staff overlays -->
<div id="bio_modals">
<?php 
  $get_members_data = getContent(
	"members",
	"display:list",
	"find_group:all-staff",
	"group_order:position",
	"order:position",
	'show:__id__',
	'show:||',
	"show:__photourl height='1200' width='1200'__",
	'show:||',
	'show:__fullname__',
	'show:||',
	'show:__position__',
	'show:||',
	'show:__groups__',
	'show:||',
	'show:__about__',
	'show:||',
	'show:__emailaddress__',
	'show:~|~|~',
	'noecho'
);

$members_data_arr = explode("~|~|~", $get_members_data);
$members_data_arr = array_filter($members_data_arr);

foreach($members_data_arr as $members_item){

	list($m_id, $m_photourl, $m_fullname, $m_position, $m_groups, $m_about, $m_emailaddress) = explode('||',$members_item);

	$members_output .= '<div id="bio-'.$m_id.'">';
	$members_output .= '<div class="row">';
	$members_output .= '<div class="column">';
	$members_output .= '<div class="photo_box align-right align-bottom">';
	$members_output .= '<img class="circle left" src="'.$m_photourl.'" alt="'.$m_fullname.', '.$m_position.'" />';
	$members_output .= '<div class="member_info">';
	$members_output .= "<span class='fullname'>$m_fullname</span>";
	$members_output .= "<span class='position'>$m_position</span>";
	$members_output .= "</div>";
	$members_output .= "</div>";
	$members_output .= "</div>";
	$members_output .= '<div class="column">';
	$members_output .= '<div class="bio_box">';
	$members_output .= $m_about;
	$members_output .= "<p class='emailaddress'>$m_emailaddress</p>";
	$members_output .= "</div>";
	$members_output .= "</div>";
	$members_output .= "</div>";
	$members_output .= "</div>";
	
}
echo $members_output;
?>
</div>

	<!-- Page Footer -->
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/footer.php"); ?>
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/scripts.php"); ?>
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/analytics.php"); ?>
	
<script type="text/javascript">

  $(function () {
  
    $('.portrait_box').modaal({
      fullscreen: true,
    });
  
  });
  var box = $('.member_box'),
    boxContainer = $('#staff_grid .row'),
    section = $('#staff_grid .row'),
    boxClassFilter,
    showThese;

  $('.filters-select').on( 'change', function() {
      // get filter value from option value
      var filterValue = this.value;
      // use filterFn if matches value
      //boxClassFilter = filterFns[ filterValue ] || filterValue;
      //boxClassFilter = $(this).data('filter');
      
      boxClassFilter = filterValue;
      
      showThese = boxContainer.find('.column' + '.' + boxClassFilter);
      var sectionHeight = section.height();
      
      // console.log(sectionHeight);
  
      //$('.tag-container').find('button.selected').removeClass();
      $(this).toggleClass('selected');
      
      var tl = new TimelineLite()
      .to(box, 0.5, {scale:0, opacity:0, ease: Power3.easeOut})
      .set(box, {display:'none'})
      .set(showThese, {display:'block'})
      .to(showThese, 0.8, {scale:1, opacity:1, ease: Power3.easeOut}, '+=0.1')
      .fromTo(section, 1, {height:'sectionHeight', ease: Power3.easeOut},  {height:'initial', ease: Power3.easeIn}, '-=1');
  
      if (boxClassFilter == 'all') {    
          var allTL = new TimelineLite()
              .set(section, {height:sectionHeight})
              .to(box, 0.5, {scale:0, opacity:0, ease: Power3.easeOut})
              .set(box, {display:'none'})
              .set(box, {display:'block'})
              .to(box, 0.8, {scale:1, opacity:1, ease: Power3.easeOut}, '+=0.1')
              .fromTo(section,1 , {height:'sectionHeight', ease: Power3.easeOut},  {height:'initial', ease: Power3.easeIn}, '-=1');
      }
            
  });
</script>


  </body>
</html>
