<?php require_once($_SERVER["DOCUMENT_ROOT"]."/_inc/config.php");
$meta = new pageMeta("index", $MCMS_SITENAME);

//billboard
$billboard = getContent("media","display:detail","find:".$_GET['nav'],"label:header","show:__imageurl maxWidth='2560'__", "noecho" );
if($billboard){$bodyClass = 'hasbillboard'; $billboardClass = 'skew skew-light'; $billboardStyle = 'style="background-image: url('.$billboard.');"';}

?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/head.php"); ?>
    <?php 
      $custom_seo_title = getContent("page","find:".$_GET['nav'],"show:__customseotitle__","noecho","noedit", "nocache");
      $page_title = ($custom_seo_title != "" && $custom_seo_title != "__customseotitle__") ? $custom_seo_title : $meta->page_title.'';
    ?>
    <title><?= $page_title ?></title>
    <meta name="description" content="<?= $meta->page_description ?>"/>
    <meta name="keywords" content="<?= $meta->page_keywords ?>"/>
    
    <meta property="og:description" content="<?= $meta->page_description ?>">
    <meta property="og:title" content="<?= $page_title ?>">
    
    <meta name="twitter:description" content="<?= $meta->page_description ?>">
    <meta name="twitter:title" content="<?= $page_title ?>">
    <meta property="og:image" content="<?= $billboard ?>">
    <meta name="twitter:image:src" content="<?= $billboard ?>">
  </head>
  
  <body id="events" class="events page">
    
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/header_subpage.php"); ?>
  <?php// include($_SERVER["DOCUMENT_ROOT"]."/_inc/billboard.php"); ?> 

  
<div id="page_content">  	
<!-- Page Content -->
  <div class="row align-center">
    <div class="medium-10 columns">
      
  <div class="content active" id ="featured_events">
 
  </div>
	
    </div>
  </div>
</div> <!-- #page_content -->    

	<div class="content" id="event_detail">
  	<div class="row align-center events" id="event">
    	
      <?
        $eventcustomdate = getContent(
  			"event",
  			"display:detail",
  			"find:".$_GET['slug'],
  			"show:<li><span>date</span> __customcustomdate__</li>",
  			"noecho");
  			
  			
  			$eventmonkdate = getContent(
  			"event",
  			"display:detail",
  			"find:".$_GET['slug'],
  			"show: <li><span>date</span> __eventstartTwo format='l, F j, Y'__</li>",
   				"show: <li><span>time</span> __eventstartThree format='g:i a'__ - __eventend format='g:i a'__",
  			"noecho");
        
        if($eventcustomdate){
          $eventdate = $eventcustomdate;
        }else{
          $eventdate = $eventmonkdate;
        }
  			
  			getContent(
  			"event",
  			"display:detail",
  			"find:".$_GET['slug'],
  			"show:<div class='small-12 medium-6 column eventimage'>",
  			"show: <img src='__imageurl__' class='circle' />",
  			
        "show:<a href='javascript:history.go(-1)' class='button dark backbtn' data-no-instant>Back to All Events</a>",  	

  			
        "show:</div>",
                
        "show:<div class='small-11 medium-6 column eventdetails'>",
        
        "show: <div class='date-badge'>",
        "show:  __eventstart format='D'__",
  			"show:  <span>__startday__</span>",
  			"show:</div>",
  			"show:<h1>__title__</h1>",
  			
  			"show:<div class='timeLocation'>",  
          "show:<ul class='event-info'>",
   			  "show:".$eventdate,
  			  "show: <li><span>location</span> __location__</li>",
  			  "show: <li><span>contact</span> __coordname__</li>",
  			  "show:</ul>",  			
  			"show:</div>",
  			
  			"show:<div class='details'>",
  			"show:__description__",
  			"show:</div>",
  			
        "show:<div class='cats'>__category__</div>",

  			"show:<ul class='action-items'>",
  			  "show:<li><a class='button green light' href='__customregisterlink__' target='_blank'>Register</a></li>",
  				"show:<li><a class='button dark' href='__import__'><span></span><span class='caption'>Add to My Calendar</span></a></li>",
  				"show:<li><a class='button dark addthis_button_compact' addthis:url=\"http://www.kingswayschool.org__url__\" addthis:title=\"__title__\"><span></span><span class='caption'>Share</span></a></li>",
  			"show:</ul>",
  			
  			"show:</div>"
        );
		  ?>
      
	  </div><!--end #events_grid -->
	</div><!-- end .events_list -->	
	
	
  <!-- Page Sections -->
<!-- 	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/sections.php"); ?> -->
	    
	<!-- Page Footer -->
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/footer.php"); ?>
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/scripts.php"); ?>
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/analytics.php"); ?>
	<script type="text/javascript" src="/_js/jquery.accordion.js"></script>
  <script type="text/javascript" src="/_js/jquery.easing.1.3.js"></script>
  
  


  </body>
</html>
