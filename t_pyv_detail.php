<?php require_once($_SERVER["DOCUMENT_ROOT"]."/_inc/config.php");
$meta = new pageMeta("page", $MCMS_SITENAME);

//billboard
$billboard = getContent("media","display:detail","find:".$_GET['nav'],"label:header","show:__imageurl maxWidth='2560'__", "noecho" );
if($billboard){$bodyClass = 'hasbillboard'; $billboardClass = 'skew skew-light'; $billboardStyle = 'style="background-image: url('.$billboard.');"';}

?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/head.php"); ?>
    <?php 
      $custom_seo_title = getContent("page","find:".$_GET['nav'],"show:__customseotitle__","noecho","noedit", "nocache");
      $page_title = ($custom_seo_title != "" && $custom_seo_title != "__customseotitle__") ? $custom_seo_title : $meta->page_title.' | Kingsway Christian Church';
    ?>
    <title><?= $page_title ?></title>
    <meta name="description" content="<?= $meta->page_description ?>"/>
    <meta name="keywords" content="<?= $meta->page_keywords ?>"/>
    
    <meta property="og:description" content="<?= $meta->page_description ?>">
    <meta property="og:title" content="<?= $page_title ?>">
    
    <meta name="twitter:description" content="<?= $meta->page_description ?>">
    <meta name="twitter:title" content="<?= $page_title ?>">
    <meta property="og:image" content="<?= $billboard ?>">
    <meta name="twitter:image:src" content="<?= $billboard ?>">
  </head>
  
  <body id="page" class="page pyv">
    
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/header_subpage.php");
  $header_image = getContent(
      "media",
      "display:detail",
      "find:".$_GET['nav'],
      "label:header",
      "show:__imageurl height='780' width='780'__",
      "noedit","noecho"
    );

  getContent(
    "page",
    "find:".$_GET['nav'],
  	'show:<div id="pyv_detail_header" class="blue">',
    	'show:<div class="row">',
      	'show:<div class="column text-center">',
  	        	'show:<h1 class="page_title"><span>__title__</span></h1>',
        	"show:</div>",
      	"show:</div>",
      	'show:<div class="row align-middle align-left">',
        	'show:<div class="column small-11 medium-6">',
          	'show:<div class="photo_box">',
          	'show:<img class="circle left" src="'.$header_image.'" alt="__title__" />',
          	"show:</div>",
        	"show:</div>",
        	'show:<div class="column small-12 medium-5">',
          	'show:<div class="text_box">',
          	'show:__text__',
          	"show:</div>",
        	"show:</div>",
      	"show:</div>",
    	'show:<div class="row align-center">',
      	'show:<div class="column medium-12 text-center">',
  	        	'show:__custompageheadertext__',
        	"show:</div>",
      	"show:</div>",
    	"show:</div>"
  	);
	?>
  <!-- Page Sections -->
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/sections.php"); ?>
	    
	<!-- Page Footer -->
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/footer.php"); ?>
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/scripts.php"); ?>
  
  
  <script type="text/javascript">

  $(function () {
  
    $('.form_box').modaal({
      fullscreen: true,
      type: 'iframe',
    });
  
  });
  
  $(function() {
    $('a[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top - 80
          }, 1000);
          return false;
        }
      }
    });
  });
  </script>
  
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/analytics.php"); ?>
  </body>
</html>
