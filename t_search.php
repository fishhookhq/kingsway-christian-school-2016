<?php require_once($_SERVER["DOCUMENT_ROOT"]."/_inc/config.php");
$meta = new pageMeta("index", $MCMS_SITENAME);

//billboard
$billboard = getContent("media","display:detail","find:".$_GET['nav'],"label:header","show:__imageurl maxWidth='2560'__", "noecho" );
if($billboard){$bodyClass = 'hasbillboard'; $billboardClass = 'skew skew-light'; $billboardStyle = 'style="background-image: url('.$billboard.');"';}

$defaultheadimage = getContent(
    "church",
    "display:detail",
    "find:kingsway-christian-school",
    "show:__imageurl__",
    "noecho"
    );
$nosections = getContent("page","find:".$_GET['nav'],"show:__customnosections__","show:hidesections","noecho","noedit", "nocache");

if($nosections == 'hidesections'){
  $nosections = 'show sections';
}
?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/head.php"); ?>
    <?php 
      $custom_seo_title = getContent("page","find:".$_GET['nav'],"show:__customseotitle__","noecho","noedit", "nocache");
      $page_title = ($custom_seo_title != "" && $custom_seo_title != "__customseotitle__") ? $custom_seo_title : $meta->page_title.' | Kingsway Christian Church';
    ?>
    <title><?= $page_title ?></title>
    <meta name="description" content="<?= $meta->page_description ?>"/>
    <meta name="keywords" content="<?= $meta->page_keywords ?>"/>
    
    <meta property="og:description" content="<?= $meta->page_description ?>">
    <meta property="og:title" content="<?= $page_title ?>">
    
    <meta name="twitter:description" content="<?= $meta->page_description ?>">
    <meta name="twitter:title" content="<?= $page_title ?>">
    <meta property="og:image" content="<?= $billboard ?>">
    <meta name="twitter:image:src" content="<?= $billboard ?>">
  </head>
  
  <body id="page" class="page">
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/header_subpage.php"); ?>
  
  <div class="normal static" id="billboard">
        <div class="cycle-slideshow" data-cycle-auto-height="1280:550"
        data-cycle-fx="fade" data-cycle-log="false" data-cycle-slides=
        "&gt; div.slide" data-cycle-speed="350" data-cycle-swipe="true" id=
        "cycle-ss" style="height: 607.578px;">
            <div class="slide cycle-slide cycle-slide-active" style=
            "position: absolute; top: 0px; left: 0px; z-index: 99; opacity: 1; display: block; visibility: visible; background-image: url('<?php echo $defaultheadimage; ?>');">
            <div class="overlay">
                    <div class="row align-middle">
                        <div class="column text-center">
                            <h1 class="caption"><span>Search Results</span></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


  
<div id="page_content">
  	<div class="row align-center">
    	<div class="medium-9 columns">
<div class='content'>
        <?php
          $results = getContent(  
            "search",
            "howmany:10",
            "hide_module:media",
            "display:results",
            "before_show:<h3>__resultsnumber__ results for \"__term__\"</h3>",
            "before_show:|^",
            "show:__type__",
            "show:|~",
            "show:__slug__",
            "show:|~",
            "show:__title__",
            "show:|~",
            "show:__url__",
            "show:|~",
            "show:__preview__",
            "show:|~",
            "show:__groups__",
            "show:|&",
            "after_show:|^",
            "after_show:__pagination__",
            "noecho"
          );   
        
          list($resultsTitle, $results, $pagination) = explode("|^", $results);
        
          echo $resultsTitle;
        
          $results = explode("|&", rtrim($results, "|&"));
        
          foreach($results as $result) {
        
            list($type, $slug, $title, $url, $preview, $groups) = explode("|~", $result);
        
              $link = $url;
              if(strtolower($type) == "small group"){                
                  $lifegrouppageurl = getContent("page","find:p-701098","show:__url__","noecho","noedit");
                  $type = "Life Group";
                  $link = $lifegrouppageurl."#".$slug;
              }
        
            ?>
        
            <div class='search-result'>
              <?php echo $type; ?> : <strong><a href='<?php echo $link; ?>'><?php echo $title; ?></a></strong>
              <p><i><?php echo $preview; ?></i></p>
            </div>
        
            <?php
        
        
        
          } //endforeach
        
          echo $pagination;
        ?>
        </div>
    	</div>
	</div> <!-- #page_content -->    
	
  <!-- Page Sections -->
  	<!-- Page Footer -->
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/footer.php"); ?>
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/scripts.php"); ?>
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/analytics.php"); ?>
	<script type="text/javascript" src="/_js/jquery.accordion.js"></script>
  <script type="text/javascript" src="/_js/jquery.easing.1.3.js"></script>
	
<script type="text/javascript">

$(window).load(function(){
  // Image with caption
  $("#page_content img.wcaption").each(function(){
     var caption = $(this).attr("alt");
     $(this).wrap("<figure></figure>").after("<figcaption><span>"+caption+"</span></figcaption>");
  });
});

$(function () {

    $('.st-accordion').accordion({
    });
  if(window.location.hash) {
    var hash = window.location.hash;
    $('#st-accordion a[href="' + hash + '"]').trigger('click');
  }
      //alert(hash);

});

</script>

  </body>
</html>
