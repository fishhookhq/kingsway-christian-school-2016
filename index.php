<?php require_once($_SERVER["DOCUMENT_ROOT"]."/_inc/config.php");
$meta = new pageMeta("index", $MCMS_SITENAME);

//billboard
$billboard = getContent("media","display:detail","find:".$_GET['nav'],"label:header","show:__imageurl maxWidth='2560'__", "noecho" );
if($billboard){$bodyClass = 'hasbillboard'; $billboardClass = 'skew skew-light'; $billboardStyle = 'style="background-image: url('.$billboard.');"';}

?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/head.php"); ?>
    <?php 
      $custom_seo_title = getContent("page","find:".$_GET['nav'],"show:__customseotitle__","noecho","noedit", "nocache");
      $page_title = ($custom_seo_title != "" && $custom_seo_title != "__customseotitle__") ? $custom_seo_title : $meta->page_title.' | Kingsway Christian Church & School';
    ?>
    <title><?= $page_title ?></title>
    <meta name="description" content="<?= $meta->page_description ?>"/>
    <meta name="keywords" content="<?= $meta->page_keywords ?>"/>
    
    <meta property="og:description" content="<?= $meta->page_description ?>">
    <meta property="og:title" content="<?= $page_title ?>">
    
    <meta name="twitter:description" content="<?= $meta->page_description ?>">
    <meta name="twitter:title" content="<?= $page_title ?>">
    <meta property="og:image" content="<?= $billboard ?>">
    <meta name="twitter:image:src" content="<?= $billboard ?>">
  </head>
  
  <body id="home" class="">
  <div id="fixed-toolbar" class="">
   <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/header_subpage.php"); ?>
  </div>

  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/toolbar.php"); ?>
  
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/header.php"); ?>
  
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/home-newhere.php"); ?>
  
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/home-upcoming.php"); ?>
	
		<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/home-search.php"); ?>


  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/home-blog.php"); ?>    

<!-- Page Sections -->
<!-- 	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/home-sections.php"); ?> -->
	    
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/footer.php"); ?>
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/scripts.php"); ?>
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/analytics.php"); ?>

    <script type="text/javascript">
      $(document).ready(function(){  
        $('.slide').each(function() {
          var $element = $(this);
          var hue = $element.data('hue');
          var bgColor = $element.data('background');
  
          $element.waypoint(function(direction) {
            if (direction === 'down') {
              $("body").removeClass().addClass(hue);
              $("body").css('background-color',bgColor);
            }
          }, {
            offset: '100'
          });
          $element.waypoint(function(direction) {
            if (direction === 'up') {
              $("body").removeClass().addClass(hue);
              $("body").css('background-color',bgColor);
            }
          }, {
            offset: '-100'
          });
        
        });
      });
      
      $('.fh-nav').waypoint(function(direction) {
        if (direction === 'down') {
         $("#fixed-toolbar").addClass("show").fadeIn();
        };
        if (direction === 'up') {
         $("#fixed-toolbar").removeClass("show");
        }
      });
    </script>
  </body>
</html>
