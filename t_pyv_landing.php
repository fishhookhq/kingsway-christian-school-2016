<?php require_once($_SERVER["DOCUMENT_ROOT"]."/_inc/config.php");
$meta = new pageMeta("page", $MCMS_SITENAME);

//billboard
$billboard = getContent("media","display:detail","find:".$_GET['nav'],"label:header","show:__imageurl maxWidth='2560'__", "noecho" );
if($billboard){$bodyClass = 'hasbillboard'; $billboardClass = 'skew skew-light'; $billboardStyle = 'style="background-image: url('.$billboard.');"';}

?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/head.php"); ?>
    <?php 
      $custom_seo_title = getContent("page","find:".$_GET['nav'],"show:__customseotitle__","noecho","noedit", "nocache");
      $page_title = ($custom_seo_title != "" && $custom_seo_title != "__customseotitle__") ? $custom_seo_title : $meta->page_title.' | Kingsway Christian Church';
    ?>
    <title><?= $page_title ?></title>
    <meta name="description" content="<?= $meta->page_description ?>"/>
    <meta name="keywords" content="<?= $meta->page_keywords ?>"/>
    
    <meta property="og:description" content="<?= $meta->page_description ?>">
    <meta property="og:title" content="<?= $page_title ?>">
    
    <meta name="twitter:description" content="<?= $meta->page_description ?>">
    <meta name="twitter:title" content="<?= $page_title ?>">
    <meta property="og:image" content="<?= $billboard ?>">
    <meta name="twitter:image:src" content="<?= $billboard ?>">
  </head>
  
  <body id="page" class="page">
    
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/header_subpage.php"); ?>

<?php
    $pyvText = getContent(
      "page",
      "find:".$_GET['nav'],
      "show:__text__",
      "noecho","noedit"
    );
    
    $list_raw = getContent(
      "linklist",
      "display:links",
      "find:plan-your-visit-links",
      "level1:__slug__",//0
      "level1:~~", //separator flag
      "level1:__name__",//1
      "level1:~~", //separator flag
      "level1:__description__",//2
      "level1:~~", //separator flag
      "level1:__imageurl width='780' height='780'__",//3
      "level1:~~", //separator flag
      "level1:__url__",//4
      "level1:|~", //separator flag
      "noecho"
    );
    $soutput = "<div class='row collapse align-center' id='pyv_links'>";
    $pyv_array = explode("|~", $list_raw); 
    foreach ($pyv_array as $key => $pyv_fields){
      if($pyv_fields){
        list($linkSlug, $linkName, $linkDesc, $linkImage, $linkUrl) = explode("~~", $pyv_fields);
        $soutput .= "<div class='small-8 medium-4 columns'>";
          $soutput .= '<a href="'.$linkUrl.'">';
            $soutput .= '<img src="'.$linkImage.'" alt="'.$linkName.'"  title="'.$linkName.'" width="780" height="780" class="circle">';
          $soutput .= '</a>';
          $soutput .= '<p class="linkdesc">'.$linkDesc.'</p>';
          $soutput .= '<a href="'.$linkUrl.'" class="button light">'.$linkName.'</a>';
        $soutput .= "</div>";
      }
    }
    $soutput .= "</div>";
  ?>

  <div class="plan-your-visit-header yellow">
    <div class="row align-center">
      <div class="medium-8 columns">
        <?php 
          echo $pyvText; 
          echo $soutput;
        ?>
      </div>
    </div>
  </div><!-- end .fh-top-b -->
  	    
	<!-- Page Footer -->
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/footer.php"); ?>
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/scripts.php"); ?>
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/analytics.php"); ?>
  </body>
</html>
