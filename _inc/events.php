  	<div class="row align-center" id="events-grid">
    	<div class="medium-9 columns eventlist">
<?php require_once($_SERVER["DOCUMENT_ROOT"]."/_inc/config.php");
	extract($_GET);
	$group = $campus;
	$category = $ministry;
	if (isset($year) && $year !== "" && isset($month) && $month !== "") {
		$mSlug = date($year."-".$month."-01");
	} else {
		$mSlug = date("Y-n-01");
		$enablePast = "no";
	}
	if($search == "") {
/*
			$eventlist = getContent(
				"event",
				"display:list",
				"find_group:".$group,
				"find_category:".$category,
				"enablepast:".$enablePast,
		    "month:".date("n",strtotime($mSlug)),
		    "year:".date("Y",strtotime($mSlug)),
		  	"show:<div class='row'>",
		  		"show:<div class='eventRow'>",
		  			"show:<a href='__url__'>",
							"show:<div class='medium-2 columns date'>",
								"show:<p><span>__eventstart format='m.d.y'__</span></p>",
							"show:</div>",
							"show:<div class='medium-10 columns title'>",
								"show:<h1>__title__<span><i class='icon-carat'></i></span></h1>",
							"show:</div>",
						"show:</a>",
					"show:</div>",
				"show:</div>",
				"noecho"
			);
*/
		if (isset($category) && $category!=="") {
			$howmany = 10;
			if($_GET['page']) $offset = $howmany * ($_GET['page'] - 1);
			$all_events = Content::getContentArray(array(
				'module' => 'event',
				'display' => 'list',
				'params' => array(
					"find_group:".$group,
					"find_category:".$category,
					//"page:".$_GET['page'],
					"howmany:".$howmany,
					"offset:".$offset
					//"enablepast:".$enablePast,
			    //"month:".date("n",strtotime($mSlug)),
			    //"year:".date("Y",strtotime($mSlug)),
				),
				'tags' => "slug, title, url, summary, description, category, __eventstart format='Ymd H:i:s'__, __eventend format='Ymd H:i:s'__, eventtimes, coordname, import, isrecurring, isallday, pagination"
			));
			
			getContent(
				"event",
				"display:list",
				"find_group:".$group,
				"find_category:".$category,
				"howmany:".$howmany,
				"page:".$_GET['page'],
				"before_show:<div class='month' id='".$category."'>",
				"before_show:<div class='headline'>",
					"before_show:<div class=\"calendar-month\">",
						"before_show:__pagination__",
						//"before_show:<h3>__category__</h3>",
				 	"before_show:</div>",
				"before_show:<div>"
				);

			} else {

			$all_events = Content::getContentArray(array(
				'module' => 'event',
				'display' => 'list',
				'params' => array(
					"find_group:".$group,
					"find_category:".$category,
					"enablepast:".$enablePast,
			    "month:".date("n",strtotime($mSlug)),
			    "year:".date("Y",strtotime($mSlug)),
				),
				'tags' => "slug, title, url, summary, description, category, __eventstart format='Ymd H:i:s'__, __eventend format='Ymd H:i:s'__, eventtimes, coordname, import, isrecurring, isallday"
			));

			$nextUrl = "/events/".date('Y',strtotime($mSlug."-1 month"))."/".date('n',strtotime($mSlug."-1 month"))."/";
			$prevUrl = "/events/".date('Y',strtotime($mSlug."+1 month"))."/".date('n',strtotime($mSlug."+1 month"))."/";
			if (isset($category) && $category!=="") {
				$nextUrl .= "ministry/".$category."/";
				$prevUrl .= "ministry/".$category."/";
			}
			if (isset($campus) && $campus!=="") {
				$nextUrl .= "campus/".$campus."/";
				$prevUrl .= "campus/".$campus."/";
			}
			echo "<div class='month' id='".date('MY',strtotime($mSlug))."'>";
			
				echo "<div class='headline'>";
					echo "<div class=\"calendar-month\">";
						echo "<a href='".$nextUrl."' class=\"calendar-prev icon icon-prev\"></a>";
						echo "<h3 class='monthName'>".date('F',strtotime($mSlug))." events</h3>";
						echo "<a href='".$prevUrl."' class=\"calendar-next icon icon-next\"></a>";
				 	echo "</div>";
				echo "<div>";

			}
			//print_r($all_events);
			
			if ($all_events){
				foreach($all_events as $event) {

					$startTS = strtotime($event['eventstart']); //__eventstart format='Ymd H:i:s'__//
				 	$endTS = strtotime($event['eventend']);  //__eventend format='Ymd H:i:s'__//
				 	$startDay = date("M", $startTS); 
				 	$startDay2 = date("j", $startTS); 
				 	$endDay = date("m.d.y", $endTS); 
				 	$startTime = date("g:i A", $startTS);
				 	
					if($event['isrecurring'] || $startDay == $endDay){ 
					 	//recurring flag is necessary because recurring event endtimes returns the event end. 
					 	//assume no multi-day events are recurring 
					 	//These events start and end on the same day. 
					 	$endTime = date("g:i A", $endTS);
					 	if ($event['isallday']) {
						 	$endDateTime = $startDay;
					 	} else {
						 	$endDateTime = $startDay . " " . $endTime;
						}
					} else { 
						//the event is not recurring, but the end is not the same day 
					 	if ($event['isallday']) {	
							$endTime = date("m.d.y", $endTS);
						 } else {
							$endTime = date("m.d.y g:i A", $endTS);
						 }
							$endDateTime = $endTime;
					}
					
					if ($event['isallday']) {
						$startDateTime = $startDay; 
					} else {
						$startDateTime = $startDay . " " . $startTime; 
					}
					
					echo "<div class='row eventRow event'>";
						echo "<div class='medium-2 columns align-middle'>";
						  echo "<p class='event-date'>".$startDay."<span>".$startDay2;
              echo "</span></p>";

						echo "</div>";
								
						echo "<div class='columns title align-middle'>";
						  echo "  <a href='".$event['url']."' class='showdetail'>";
              echo "    <h1>".$event['title']."<span><i class='icon-carat'></i></span></h1>";
              echo "   </a>";
              
              echo "<div class='eventdetail'>";
                echo "  <div class='content_holder'><div class='row align-center'><div class='medium-8 column'>";
                  echo "    <a href='' class='closer'><p><span class='icon-x'></span></p></a>";
                  echo "blah";
                
                echo "  </div></div></div>";//end content-holder

              echo "</div>";//end eventdetail

              
            echo "</div>";
					echo "</div>";
				}
			} else {
				echo "<p class='text-center'>No events this month.</p>";
			}
			echo "</div>";

	} else {
		
		getContent(
			"search",
			"display:results",
			"find_module:events",
			"hide_module:media",
			"hide_group:hide-from-search",
			"keywords:".$search,
			"show:<div class='row event'>",
			  		"show:<div class='eventRow'>",
			  			"show:<a href='__url__'>",
//								"show:<div class='medium-2 columns date'>",
//									"show:<p><span>__date__</span></p>",
//								"show:</div>",
								"show:<div class='medium-12 columns title'>",
									"show:<h1>__title__<span><i class='icon-carat'></i></span></h1>",
								"show:</div>",
							"show:</a>",
						"show:</div>",
	    "show:</div>",
	    "after_show:__pagination__"
		);
		
	}
	?>
    	</div>
  	</div>
  	