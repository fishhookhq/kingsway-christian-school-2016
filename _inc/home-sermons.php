<div class="fh-bottom-b slide home-sermon large" data-background="rgba(255,255,255,1.0)" data-hue="isLight">
  <div class="row align-center">
	  <div class="medium-8 columns text-center">
		  <h2 class="title">Latest Messages</h2>
			  <?php getContent(
				  "sermon",
					"display:list",
					"howmany:1",
					"features",
					"show:  <div class=''><img src='__imageurl__' /></div>",
          "show:  <h3 class='sermon_title'><a href='/series/__seriesslug__/'>__title__</a></h3>",
          "show:  <div class='row sermon_meta align-center'>",
          "show:    <div class='column shrink series_title'>__series__</div>",
          "show:    <div class='column shrink date'>__date format='F j, Y'__</div>",
          "show:    <div class='column shrink speaker'>__preacher__</div>",
          "show:  </div>",
          "show:  <div class='row sermon-media align-center align-middle'>",
          "show:    <div class='column shrink audio'><a data-reveal-id='sermonAudioModal' title='__title__' class='' data-audio='__audiourl__' target='_blank' href='#'><span class='icon-audio'></span></a></div>",
//				"show:<li class='download'><a class='' href='__audio__' target='_blank' title='Download'><span class='icon-download'></span>Download Sermon Audio</a></li>",
					"show:    <div class='column shrink notes'><a class='' href='__notes__' target='_blank' title='Manuscript'><span class='icon-notes'></span></a></div>",
          "show:    <div class='column shrink share'><a class=' addthis_button_compact' addthis:url=\"http://www.kingswaychurch.org/sermon/__slug__/\" addthis:title=\"__title__: Sermon from ".$MCMS_SITENAME."\" title='Share'><span class='icon-share'></span></a></div>",
          "show:    <div class='column shrink allseries'><a class='button dark' href='/explore/messages/'>View All Messages</a></div>",
          "show:  </div>"
        ); ?>
		</div>
  </div>
</div><!-- end .fh-bottom-b -->
    
    
