 <script src="/bower_components/jquery/dist/jquery.min.js"></script>
 <script src="/bower_components/what-input/what-input.min.js"></script>
 <script src="/bower_components/foundation-sites/dist/foundation.min.js"></script>
 <script src="/bower_components/foundation-sites/js/foundation.accordion.js"></script>
 <script src="/bower_components/foundation-sites/js/foundation.reveal.js"></script>
 <script src="/bower_components/foundation-sites/js/foundation.tabs.js"></script>
 
 <!-- Monk Dev Includes -->
 
 <script type="text/javascript" src="<?php echo getCmsUrl(); ?>/Clients/monkcms-user.js"></script>
 <script type="text/javascript" src="<?php echo getCmsUrl(); ?>/Clients/monkcms-dev.js"></script>

<!-- End Monk Dev Includes -->

 <script src="/_js/min/app-min.js" type="text/javascript"></script>
 <script src="/bower_components/jquery-cycle2/build/jquery.cycle2.min.js" type="text/javascript"></script>
 <script src="/bower_components/waypoints/lib/jquery.waypoints.min.js" type="text/javascript"></script>
 <script src="/bower_components/waypoints/lib/waypoints.debug.js" type="text/javascript"></script>
 <script src="/_js/modaal.min.js" type="text/javascript"></script>
 <script src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js" type="text/javascript"></script>
 <script src="http://vjs.zencdn.net/5.18.4/video.js"></script>


<script type="text/javascript" src="/bower_components/chosen/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="/bower_components/select2/dist/js/select2.full.js"></script>


<script src="/_js/instantclick.min.js" data-no-instant></script>
<script data-no-instant>InstantClick.init('mousedown', true);</script>

<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $(function() {
  		$('.sorter').select2({allowClear: true});
    });
  });
  $(document).ready(function () {
    // This allows the negative margin bg on accordions. It loads as overflow:hidden;
    $('.st-accordion ul li.acc').css('overflow', 'visible');
  });
</script>
	
