<?php
	
$customheadertext = getContent(
	"page",
	"find:".$_GET['nav'],
	"show:__custompageheadertext__",
	"noecho",
	"noedit"
);

$load_image = getContent(
	"rotator",
	"display:slides",
	"find:".$_GET['nav'],
	"howmany:1",
	"slide_show:__imageurl width='1280' height='550'__",
	"noecho","noedit"
);
$load_video = getContent(
	"rotator",
	"display:slides",
	"find:".$_GET['nav'],
	"howmany:1",
	"slide_show:__videourl__",
	"noecho","noedit"
);
if ($load_image or $load_video) {
	getContent(
	"rotator",
	"display:slides",
	"find:".$_GET['nav'],
	"before_show:<div id='rotator-__id__' class='rotator'>",
	"before_show:<div id=\"cycle-ss\" class='cycle-slideshow'",
	"before_show: data-cycle-slides=\"> div.slide\"",
//	"before_show: data-cycle-auto-height=\"1280:550\"",
	"before_show: data-cycle-speed=\"350\"",
	"before_show: data-cycle-swipe=\"true\"",
	"before_show: data-cycle-log=\"false\"",
	"before_show: data-cycle-fx=\"fade\"",
	"before_show: data-cycle-timeout=\"__transitionms__\"",
	"before_show: data-cycle-pager=\"#cycle-pager-__id__\"",
	"before_show: >",
//	"before_show:<img src='".$load_image."' width='1280' height='550' class='loading' />",
	"image_slide_show:<div class='slide'",
	"image_slide_show: style=\"background-image: url('__imageurl maxWidth='1600'__');\"",
	"image_slide_show:>",
	"image_slide_show:<div class='overlay'>",
	"image_slide_show:<div class='row align-middle'>",
	"image_slide_show:<div class='column text-center'>",
	"image_slide_show:".$customheadertext,
	"image_slide_show:</div>",
	"image_slide_show:</div>",
	"image_slide_show:</div>",
	"image_slide_show:</div>",
	"video_slide_show:<div class='slide'",
	"video_slide_show: style=\"background-image: url('__custommobilegif__');\"",
	"video_slide_show:>",
	"video_slide_show:<div class='overlay'><div class='container show-for-medium'>",
	"video_slide_show:<video id=\"video\" muted preload=\"auto\" autoplay loop width=\"100%\" height=\"100%\"",
	"video_slide_show: poster=\"__videopreviewimageurl width='1600' height='900'__\"",
//	'video_slide_show: onclick="this.play();"',
//	"video_slide_show: src=\"__videourl__\" type=\"video/mp4\"",
	"video_slide_show:>",
	"video_slide_show:<source src=\"__videourl__\" type=\"video/mp4\">",
	"video_slide_show:</video>",
	"video_slide_show:</div>",
	"video_slide_show:<div class='vc'>",
	"video_slide_show:".$customheadertext,
	"video_slide_show</div>",
	"video_slide_show:</div>",
	"video_slide_show:</div>",
	"after_show:</div>",
	"after_show:</div></div><!--end .rotator-->"
	);
} else {
	$load_image = getContent(
		"media",
	  "display:detail",
	  "find:".$_GET['nav'],
	  "label:header",
	  "show:__imageurl width='1280' height='550'__",
		"noecho","noedit"
	);
	echo "<div id='billboard' class='normal static'>";
//	echo "<img src='".$load_image."' width='1280' height='550' class='loading' />";
	echo "<div id=\"cycle-ss\" class='cycle-slideshow'";
	echo " data-cycle-slides=\"> div.slide\"";
	echo " data-cycle-auto-height=\"1280:550\"";
	echo " data-cycle-speed=\"350\"";
	echo " data-cycle-swipe=\"true\"";
	echo " data-cycle-log=\"false\"";
	echo " data-cycle-fx=\"fade\"";
	echo " >";
	
	echo "<div class='slide'";
	getContent(
		"media",
	  "display:detail",
	  "find:".$_GET['nav'],
	  "label:header",
		"show: style=\"background-image: url('__imageurl maxWidth='1600'__');\""
	);
	echo ">";
	echo "<div class='overlay'>";
	echo "<div class='row align-middle'>";
	echo "<div class='column text-center'>";
	echo $load_video;
	getContent(
		"page",
	  "find:".$_GET['nav'],
		"show:<h1 class='caption'><span>__title__</span></h1>",
    "show:<div class='subtitle'><span>__custompagesubtitle__</span></div>",
  	"show:<div class='header_text'><span>__custompageheadertext__</span></div>"
	);
	
	if ($page_slug == "students") {
	echo "<div class='row'>";
	echo "<div class='large-12 columns text-center social-links'>";
		echo "</div>";

	echo "</div>";
	} else {
		echo "";
	}
	echo "</div>";
	echo "</div>";
	echo "</div>";
	echo "</div>";
	echo "</div>";
	echo "</div><!--end .rotator-->";
}
?>