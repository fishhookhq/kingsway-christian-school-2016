  <div class="fh-top-c slide home-search large" data-background="rgba(255,217,0,1.0)" data-hue="isLight">
    <div class="row align-center align-middle">
      <div class="medium-10 columns text-center">
        <?php 
          getContent(
            "section",
            "display:detail",
            "find:home-looking-for-something",
            "show:__text__"
          );
        ?>
        <?php 
  				$search1 = getContent("search", "display:box","noecho","noedit","number:1"); 
  				$search1 = str_replace('class="mcmsSearch"','class="mcmsSearch icon-search"',$search1);
  				$search1 = str_replace('value="search"','value="" placeholder="Search"',$search1);
  				$search1 = str_replace('Go','',$search1);
  				$search1 = str_replace('id="search_go1"','id="search_go" class="icon-search"',$search1);
  				echo $search1;
			  ?>
      </div>
    </div>
  </div>  <!-- end .fh-top-c -->