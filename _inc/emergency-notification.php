<?php 
	getContent(
		"section",
		"find:emergency-notification",
		"order:random",
		"category:published",
		"show:<div class='sticky-anchor'></div>",
		"show:<div class='emergency'>",
			"show:<div class='row'>",
				"show:<div class='large-12 columns text-center'>",
					"show:<p>__text__</p>",
				"show:</div>",
			"show:</div>",
		"show:</div>"
	);
?>