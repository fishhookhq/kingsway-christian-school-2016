<?php
$cSections;

$cSections .= getContent("section","display:detail","find:".$_GET['nav'],"label:Section 1","show:__title__","show:~~","show:__slug__","show:~~","show:__description__","show:~~","show:__text__","show:~~","show:__customrgba__","show:~~","show:__customclass__","show:~~","show:__customimage__","show:||","noecho");
$cSections .= getContent("section","display:detail","find:".$_GET['nav'],"label:Section 2","show:__title__","show:~~","show:__slug__","show:~~","show:__description__","show:~~","show:__text__","show:~~","show:__customrgba__","show:~~","show:__customclass__","show:~~","show:__customimage__","show:||","noecho");
$cSections .= getContent("section","display:detail","find:".$_GET['nav'],"label:Section 3","show:__title__","show:~~","show:__slug__","show:~~","show:__description__","show:~~","show:__text__","show:~~","show:__customrgba__","show:~~","show:__customclass__","show:~~","show:__customimage__","show:||","noecho");
$cSections .= getContent("section","display:detail","find:".$_GET['nav'],"label:Section 4","show:__title__","show:~~","show:__slug__","show:~~","show:__description__","show:~~","show:__text__","show:~~","show:__customrgba__","show:~~","show:__customclass__","show:~~","show:__customimage__","show:||","noecho");
$cSections .= getContent("section","display:detail","find:".$_GET['nav'],"label:Section 5","show:__title__","show:~~","show:__slug__","show:~~","show:__description__","show:~~","show:__text__","show:~~","show:__customrgba__","show:~~","show:__customclass__","show:~~","show:__customimage__","show:||","noecho");
$cSections .= getContent("section","display:detail","find:".$_GET['nav'],"label:Section 6","show:__title__","show:~~","show:__slug__","show:~~","show:__description__","show:~~","show:__text__","show:~~","show:__customrgba__","show:~~","show:__customclass__","show:~~","show:__customimage__","show:||","noecho");

$cSections = explode("||", trim($cSections, "||"));
?>
<?php foreach($cSections as $section):
	 		list($sTitle, $sSlug, $sDescription, $sText, $sDark, $sLight, $sParallax, $sBgImage, $sCustomClass, $sNoPad) = explode("~~", $section);
			$sClass = '';
			$sBg = '';
			if($sLight){$sClass='light';};
			if($sDark){$sClass='dark';};
			if($sBgImage){$sBg = 'style="background-image:url('.$sBgImage.');"'; $sClass .= ' hasbg';}
			if($sCustomClass){$sClass.=' '.$sCustomClass;}
			if($sNoPad){$sClass.=' nopad';$rClass='expanded';}
			if($sParallax){$siClass.=' parallax';}
?>
<?php
$sMonklet = explode("***MONKLET***",$sText);
if($sMonklet[1]):
?>
<div class="content-section <?=$sClass?>">
<div class="content-section-inner <?=$siClass?>" <?=$sBg?>>
   	<?php
		if (strpos($sMonklet[1],'featured-layout') !== false){echo '<div class="layout-left">';}
		echo '<div class="text">';
    if($sDescription){ echo '<div class="container"><h2 class="title">'.$sDescription.'</h2></div>'; }
		if($sMonklet[0]){
			$monkletTop = $sMonklet[0];
			$opendiv = substr($sMonklet[0], -5);
			if($opendiv=='<div>'){$monkletTop = substr($sMonklet[0], 0, -5);}
			$openp = substr($sMonklet[0], -3);
			if($openp=='<p>'){$monkletTop = substr($sMonklet[0], 0, -3);}
			if($monkletTop!=''){ echo '<div class="container">'.$monkletTop.'</div>'; }
		}
		if($sMonklet[1]){ 
			echo $sMonklet[1];
		}
		if($sMonklet[2]){ 
			$monkletBtm = $sMonklet[2];
			$closediv = substr($sMonklet[2], 0,6);
			if($closediv=='</div>'){$monkletBtm = substr($sMonklet[2], 6);}
			$closep = substr($sMonklet[2], 0,4);
			if($closep=='</p>'){$monkletBtm = substr($sMonklet[2], 4);}
			if($monkletBtm!=''){ echo '<div class="container">'.$monkletBtm.'</div>'; }
		}
		echo '</div>';
		if (strpos($sMonklet[1],'featured-layout') !== false){echo '</div>';}
	?>
</div>
</div>
<?php else: ?>
<div class="content-section <?=$sClass?>">
<div class="content-section-inner <?=$siClass?>" <?=$sBg?>>
	<div class="container row <?=$rClass?>">
   	<?php
		echo '<div class="text column">';
	    if($sDescription){ echo '<h2 class="title">'.$sDescription.'</h2>'; }
		if($sText){ echo $sText; }
		echo '</div>';
	?>
    </div>
</div>
</div>
<?php endif; ?>

<?php endforeach; ?>