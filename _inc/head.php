<?php include($_SERVER["DOCUMENT_ROOT"]."/_lib/fh/simplehtmldom_1_5/simple_html_dom.php"); ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/_inc/credits.php'); ?>
<?php 
  $sitenav = 'school'; 
   if($sitenav =='school'){
     $churchsite = '';
     $schoolsite ='active';
   }else{
     $churchsite = 'active';
     $schoolsite ='';
   }
  ?>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
<meta content="yes" name="apple-mobile-web-app-capable">

<meta name="author" content="Fishhook">

<?php $site_status->robots_meta_tag(); ?>

<link rel="stylesheet" href="<?php print Helper::fileTimestamp('/_css/app.css'); ?>" />
<link href="http://vjs.zencdn.net/5.10.7/video-js.css" rel="stylesheet">
<link type="text/css" href="/bower_components/chosen/chosen/chosen.css" rel="stylesheet" />
<link type="text/css" href="/bower_components/select2/dist/css/select2.css" rel="stylesheet" />


<!-- Typekit Font Include -->
<script>
  (function(d) {
    var config = {
      kitId: 'qnr7qrh',
      scriptTimeout: 3000,
      async: true
    },
    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
  })(document);
</script>

<!-- End Typekit Font Include -->

<!-- Icon Font -->
    <link href="/_lib/fontastic/styles.css" rel="stylesheet">
<!-- End Icon Font -->

<?php
  getContent(
    'site',
    'display:css',
    'show:<style type="text/css">__code__</style>'
  );
?>
<script src="/bower_components/modernizr/modernizr.js"></script>
