    <div class="fh-toolbar">
      <div class="row">
        <div class="medium-6 small-8 columns">
          <div class="switcher">
            <a href="http://www.kingswaychurch.org" class="<?= $churchsite ?>">Church</a> / <a href="http://www.kingswayschool.org" class="<?= $schoolsite ?>">School</a>
          </div>
        </div>
        <div class="medium-6 small-4 columns text-right">
          <div class="sm">
            <?php
              getContent(
      					'linklist',
      					'find:social-media',
      					'display:links',
      					'show:<a href="__url__" target="_blank"><i class="__description__"></i></a>'
      				);
            ?>
          </div>
        </div>
      </div>
    </div><!-- end .fh-toolbar -->