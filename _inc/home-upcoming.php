  <div class="fh-bottom-a slide home-upcoming medium" data-background="rgba(51,59,65,1.0)" data-hue="isDark">
    <div class="row align-center">
      <div class="medium-10 columns text-center">
        <?php 
          getContent(
            "section",
            "display:detail",
            "find:home-upcoming",
            "show:__text__"
          );
        ?>
        <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/upcoming.php"); ?>
      </div>
    </div>
  </div><!-- end .fh-bottom-a -->