<header>
  <div id="billboard" class="normal">
        <div id="header_img" class="parallax-window slide" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="0" <?= $billboard ?>>
                <div class="row align-middle align-center isDark">
                    <div class="small-12 medium-10 columns text-center caption">
                        <h6>Latest Sermon</h6>
 
 
        <?php
          getContent(
          "sermon",
          "display:list",
          "order:recent",
          "howmany:1",
          "features",
        //"find_series:current",
          "show:  <h3 class='sermon_title'>__title__</h3>",
          "show:  <div class='row sermon-meta align-center'>",
          "show:    <div class='column shrink series_title'>__series__</div>",
          //"show:    <div class='column shrink date'>__date format='F j, Y'__</div>",
          //"show:    <div class='column shrink speaker'>__preacher__</div>",
          "show:  </div>",
          "show:  <div class='row sermon-media align-center align-middle'>",
          "show:    <div class='column shrink video'><a data-reveal-id='sermonAudioModal' title='__title__' class='' data-audio='__videourl__' target='_blank' href='#'><span class='icon-video'></span></a></div>",
          "show:    <div class='column shrink audio'><a data-reveal-id='sermonAudioModal' title='__title__' class='' data-audio='__audiourl__' target='_blank' href='#'><span class='icon-audio'></span></a></div>",
//              "show:<li class='download'><a class='' href='__audio__' target='_blank' title='Download'><span class='icon-download'></span>Download Sermon Audio</a></li>",
                    "show:    <div class='column shrink notes'><a class='' href='__notes__' target='_blank' title='Manuscript'><span class='icon-notes'></span></a></div>",
          "show:    <div class='column shrink share'><a class=' addthis_button_compact' addthis:url=\"http://www.kingswaychurch.org/sermon/__slug__/\" addthis:title=\"__title__: Sermon from ".$MCMS_SITENAME."\" title='Share'><span class='icon-share'></span></a></div>",
          "show:    <div class='column shrink allseries'><a class='button light' href='/series/__seriesslug__/'>View All Sermons in Series</a></div>",
          "show:  </div>"
          );
        ?>
 
                    </div>
                </div>
            </div>
    </div>
</header>