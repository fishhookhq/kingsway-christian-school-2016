<?php
$cSections;

$cSections .= getContent("section","display:detail","find:".$_GET['nav'],"label:Section 1",
"show:__title__",
"show:~~",
"show:__slug__",
"show:~~",
"show:__description__",
"show:~~",
"show:__text__",
"show:~~",
"show:__customsectionimage height='780' width='780'__",
"show:~~","show:__customimageleft__left","show:~~","show:__customimageright__right","show:~~","show:__customsectionclass__","show:~~","show:__customrgba__","show:||","noecho");
$cSections .= getContent("section","display:detail","find:".$_GET['nav'],"label:Section 2","show:__title__","show:~~","show:__slug__","show:~~","show:__description__","show:~~","show:__text__","show:~~","show:__customsectionimage height='780' width='780'__","show:~~","show:__customimageleft__left","show:~~","show:__customimageright__right","show:~~","show:__customsectionclass__","show:~~","show:__customrgba__","show:||","noecho");
$cSections .= getContent("section","display:detail","find:".$_GET['nav'],"label:Section 3","show:__title__","show:~~","show:__slug__","show:~~","show:__description__","show:~~","show:__text__","show:~~","show:__customsectionimage height='780' width='780'__","show:~~","show:__customimageleft__left","show:~~","show:__customimageright__right","show:~~","show:__customsectionclass__","show:~~","show:__customrgba__","show:||","noecho");
$cSections .= getContent("section","display:detail","find:".$_GET['nav'],"label:Section 4","show:__title__","show:~~","show:__slug__","show:~~","show:__description__","show:~~","show:__text__","show:~~","show:__customsectionimage height='780' width='780'__","show:~~","show:__customimageleft__left","show:~~","show:__customimageright__right","show:~~","show:__customsectionclass__","show:~~","show:__customrgba__","show:||","noecho");
$cSections .= getContent("section","display:detail","find:".$_GET['nav'],"label:Section 5","show:__title__","show:~~","show:__slug__","show:~~","show:__description__","show:~~","show:__text__","show:~~","show:__customsectionimage height='780' width='780'__","show:~~","show:__customimageleft__left","show:~~","show:__customimageright__right","show:~~","show:__customsectionclass__","show:~~","show:__customrgba__","show:||","noecho");
$cSections .= getContent("section","display:detail","find:".$_GET['nav'],"label:Section 6","show:__title__","show:~~","show:__slug__","show:~~","show:__description__","show:~~","show:__text__","show:~~","show:__customsectionimage height='780' width='780'__","show:~~","show:__customimageleft__left","show:~~","show:__customimageright__right","show:~~","show:__customsectionclass__","show:~~","show:__customrgba__","show:||","noecho");
$cSections .= getContent("section","display:detail","find:".$_GET['nav'],"label:Section 7","show:__title__","show:~~","show:__slug__","show:~~","show:__description__","show:~~","show:__text__","show:~~","show:__customsectionimage height='780' width='780'__","show:~~","show:__customimageleft__left","show:~~","show:__customimageright__right","show:~~","show:__customsectionclass__","show:~~","show:__customrgba__","show:||","noecho");
$cSections .= getContent("section","display:detail","find:".$_GET['nav'],"label:Section 8","show:__title__","show:~~","show:__slug__","show:~~","show:__description__","show:~~","show:__text__","show:~~","show:__customsectionimage height='780' width='780'__","show:~~","show:__customimageleft__left","show:~~","show:__customimageright__right","show:~~","show:__customsectionclass__","show:~~","show:__customrgba__","show:||","noecho");
$cSections .= getContent("section","display:detail","find:".$_GET['nav'],"label:Section 9","show:__title__","show:~~","show:__slug__","show:~~","show:__description__","show:~~","show:__text__","show:~~","show:__customsectionimage height='780' width='780'__","show:~~","show:__customimageleft__left","show:~~","show:__customimageright__right","show:~~","show:__customsectionclass__","show:~~","show:__customrgba__","show:||","noecho");


$cSections = explode("||", trim($cSections, "||"));
?>
<?php foreach($cSections as $section):
  //echo $section;
	 		list($sTitle, $sSlug, $sDescription, $sText, $sImage, $sImageLeft, $sImageRight, $sCustomClass, $sRgba) = explode("~~", $section);
			$sBg = '';
			if($sImageLeft){$rClass=' align-left';$sTxtAlign = " medium-6";}
			elseif($sImageRight){$rClass=' align-right';$sTxtAlign = " medium-6";}
			else { $rClass=" align-center"; $sTxtAlign = " medium-10"; }
			if(!$sImageLeft && !$sImageRight && $sImage){
  			$sBg = getContent("section","display:detail","find:".$sSlug,'show: style="background-image:url(__customsectionimage__);"','noecho','noedit'); 
  			$sCustomClass .= ' hasbg';
  		}
?>
<div id="<?=$sSlug?>" class="content-section <?=$sCustomClass?>"<?=$sBg?>>
<div class="content-section-inner">
	<div class="container row<?=$rClass?>">
   	<?php
		  if($sImageLeft!=''){
			  echo '<div class="small-11 medium-5 columns">';
			  echo '<img class="circle left bw" src='.$sImage.' alt="'.$sTitle.'" />';
			  echo '</div>';
			  
			  echo '<div class="text small-11 columns'.$sTxtAlign.'">';
		    if($sDescription){ echo '<h2 class="title">'.$sDescription.'</h2>'; }
			
			if($sText){ echo $sText; }     
	      
			echo '</div>';
		  }else
      
		  if($sImageRight!=''){
        
		  	echo '<div class="text small-11 columns'.$sTxtAlign.' small-order-2 medium-order-1">';
		  	if($sDescription){ echo '<h2 class="title">'.$sDescription.'</h2>'; }
		  	if($sText){ echo $sText; }     
		  	echo '</div>';
      
		  	echo '<div class="small-11 medium-5 columns small-order-1">';
		  	echo '<img class="circle right bw" src='.$sImage.' alt="'.$sTitle.'" />';
		  	echo '</div>';
		  }else{
			  			  echo '<div class="text small-11 columns'.$sTxtAlign.'">';
		    if($sDescription){ echo '<h2 class="title">'.$sDescription.'</h2>'; }
			
			if($sText){ echo $sText; }     
	      
			echo '</div>';

		  }
	?>
    </div>
</div>
</div>
<?php endforeach; ?>