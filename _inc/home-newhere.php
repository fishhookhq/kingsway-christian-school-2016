<?php
    $topa = getContent(
      "section",
      "display:detail",
      "find:".$_GET['nav'],
      "label:Top B",
      "show:__title__",
      "show:~~",
      "show:__slug__",
      "show:~~",
      "show:__text__",
      "show:~~",
      "show:__customrgba__",
      "show:~~",
      "show:__customsectionclass__",
      "show:~~",
      "show:__customsectionimage height='780' width='780'__",
      "noecho"
    );
    list($taTitle, $taSlug, $taText, $taRgba, $taClass, $taImage) = explode("~~", $topa);
  ?>

  <div class="fh-top-b slide slide2 <?=$taSlug ?>" data-background="<?=$taRgba ?>" data-hue="isLight">
    <div class="row align-middle align-center">
      <div class="medium-5 small-12 columns">
        <img src="<? echo $taImage; ?>" class="circle bw" alt="<?=$taTitle?>" />
      </div>
      <div class="medium-6 small-12 columns">
        <?php echo $taText; ?>
      </div>
    </div>
  </div><!-- end .fh-top-b -->