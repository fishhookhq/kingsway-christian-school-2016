<?php

// Data Attributes
$html_data = '';
$html_class = '';


if(getCmsUrl())
{
	$htmlData .= "data-cms='".getCmsUrl()."' ";
}
if(getSiteId())
{
	$htmlData .= "data-siteid='".getSiteId()."' ";
}

$htmlClass .= $site_status->wardrobe_class();
$htmlClass .= $site_status->setupguide_class();

?>
<!DOCTYPE html>
<?php
// Output Tags
print "<html class='no-js $htmlClass' lang='en' xml:lang='en' $htmlData>";
?>