<footer>
  <div class="fh-footer-a">
    <div class="row">
      <div class="column text-center">
          <img id="footer-logo" src="/_img/logo-markName-white.svg" alt="Kingsway Christian School" />
          <?php 
          getContent(
            "section",
            "display:detail",
            "find:footer",
            "show:__text__"
          );
        ?>
      </div>
    </div>
  </div>
  <div class="fh-footer-b">
    <div class="row align-middle ">
      <div class="small-12 column small-order-3 text-center">
        Designed and Powered by <a href="https://www.fishhook.us" target="_blank">Fishhook</a>
      </div>
      <div class="small-12 small-order-1 column text-center">
        <div class="sm">
          <?php
            getContent(
    					'linklist',
    					'find:social-media',
    					'display:links',
    					'show:<a href="__url__" target="_blank"><i class="__description__"></i></a>'
    				);
          ?>
        </div>
      </div>
      <div class="small-12 column small-order-2  text-center">
        &copy; Copyright <?php echo date('Y');?> Kingsway Christian School
       
      </div>
    </div>
  </div>
</footer>


<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/navigation.php"); ?>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52b3a0bd3813c681"></script>
