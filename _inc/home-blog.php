<div class="fh-bottom-c slide home-blog medium" data-background="rgba(243,148,33,1.0)" data-hue="isLight">

     <?php
      	getContent(
      	"blog",
      	"display:auto",
      	"name:the-kingsway-blog",
      	"order:recent",
      	"features",
      	"howmany:1",
        
        "show_postlist:<div class='row align-center blogimage'>",
        "show_postlist: <div class='medium-8 columns text-center'>",
        "show_postlist:   <img src='__imageurl width='400' height='400'__' class='circle bw' alt='__caption__' />",
        "show_postlist: </div>",
        "show_postlist:</div>",
        
        "show_postlist:<div class='post_info'>",
        "show_postlist: <div class='row align-center'>",
      	"show_postlist:   <div class='medium-8 columns text-center'>",
        "show_postlist:     <h3 class='blogtitle'><a href='__blogtitlelink__'>__blogposttitle__</a></h3>",
        "show_postlist:   </div>",
        "show_postlist: </div>",

      	"show_postlist: <div class='row align-center'>",
      	"show_postlist:   <div class='column shrink category'>__category__</div>",
      	"show_postlist:   <div class='column shrink date'>__blogpostdate format='F j, Y'__</div>",
      	"show_postlist:   <div class='column shrink author'>__blogauthor__</div>",
      	"show_postlist: </div>",
      	"show_postlist:</div>",
        
        "show_postlist:<div class='row align-center blogpreview small'>",
        "show_postlist: <div class='medium-8 columns text-center'>",
      	"show_postlist:   <p>__preview__</p>",
        "show_postlist: </div>",
        "show_postlist:</div>",

      	"show_postlist:<div class='row align-center'>",
      	"show_postlist: <div class='column shrink text-center'>",
      	"show_postlist:   <a href='__blogtitlelink__' class='button light'>view blog post</a>",
      	"show_postlist: </div>",
      	"show_postlist:</div>"
      	);
      
      ?>
    </div><!-- end .fh-bottom-c -->