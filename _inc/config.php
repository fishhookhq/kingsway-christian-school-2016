<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/monkcms.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/_lib/monk/status.class.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/_lib/monk/meta.class.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/_lib/monk/helper.class.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/_lib/monk/content.class.php");

class site
{
	/**
    * Get client logo campus
    *
    * @return string - logo url
    */
  	public static function logo()
	  {
	   $string = getContent(
			"site",
			"display:detail",
			"show:__logourl__",
			"noecho",
			"noedit"
		 );
		return $string;
	  }
  
 	/**
    * Get client timezone campus
    *
    * @return string         - client timezone
    */
	 public static function timezone()
	  {
	   $string = getContent(
			"site",
			"display:detail",
			"show:__timezone__",
			"noecho",
			"noedit"
		 );
		return $string;
	  }


}//end class


// Global Config
$site = new site();
$site_status = new Monk_Site_Status(array(
	'site_name' => $MCMS_SITENAME,
	'easy_edit' => isEasyEditOn()
));
//print_r($site_status);
date_default_timezone_set($site->timezone());

// Begin Document
require_once($_SERVER["DOCUMENT_ROOT"]."/_inc/doctype.php");
?>