<div id="event_filters">
  <div class="row align-center">
    
 <div class="small-12 medium-10 columns">

	<div class="filters">
  
  	<form method="get" id="module-search" name="module-search" action="">

		<div class="row align-center">
  		<div class="small-10 medium-1 column align-center align-middle sort-label">
  		  Sort by
  		</div>
			<div class="small-10 medium-3 columns">
  				<select name="date" id="select-date" class='sorter' data-placeholder="Date">
				    <option value=""></option>
				    <?php getContent(
				    	"event",
			        "display:list",
			        "groupby:month",
			        "howmanydays:365",
			        "group_show:<option value='__slug__'>__title__</option>"
				    	);
				   	?>
				</select>
			</div>
			<div class="small-10 medium-3 columns">
  				<select name="category" id="select-ctgy" class='sorter' data-placeholder="Category">
				    <option value=""></option>
				    <?php getContent(
				    	"event",
			        "display:categories",
			        "parent_category:events",
			        "level1:<option value='__slug__'>__name__</option>",
			        "level2:<option value='__slug__'>- __name__</option>",
			        "level3:<option value='__slug__'>-- __name__</option>"
				    	);
				   	?>
				</select>			</div>
				</form>
			<div class="small-10 medium-3 columns">
			 <form action="/search-results/" method="get" id="searchForm3">
          <fieldset>
            <input type="text" id="search_term3" name="keywords" value="" class="" placeholder="search">
            <button type="submit" name="submit" id="module-search-submit" value="" class="icon-search"></button>

            <input type="hidden" name="show_results" value="events"> 
          </fieldset>
        </form>
			
			</div>
			
		

		</div>
	</div>

  </div>

  </div>
</div><!-- end #filters -->
