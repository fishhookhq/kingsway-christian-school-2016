<?php
  $pyvlink = getContent(
		'linklist',
		'find:navigation-links',
		'display:links',
		'show:<a href="__url__"  class="pyv button expanded">__name__</a>',
		'noecho'
	);
?>

<div id="menu" class="content_overlay">
  <div class="sm show-for-xlarge">
    <?php
      getContent(
				'linklist',
				'find:social-media',
				'display:links',
				'show:<a href="__url__" target="_blank"><i class="__description__"></i></a>'
			);
    ?>
  </div>
			<a id="closer"><i class="icon-x"></i></a>
			<div class="centered">
				<div class="container">
					<div id="nav" class="row show-for-xlarge">
        <?php 
        // Build Meganav 
        		$output = "";
        		$navigation = "";
        		$navId = $navId ? $navId : "nav";
        
        		$level1 = getContent( "navigation","display:dropdown","open:yes","toplevel:1","bottomlevel:1","id:nav","show:~|","show:__title__","show:~|","show:__pagetitle__","show:~|","show:__url__","show:~|","show:__id__","show:~|","show:__current__","show:~|","show:target=\"_blank\"__ifnewwindow__","noecho");
        		$level1 = explode('<ul id="nav">', $level1);
        		$level1 = explode('</li>', $level1[1]); array_pop($level1);
        
        		foreach ($level1 as $item) {
        
        			list($li,$title,$pagetitle,$url,$id,$current,$target) = explode("~|",$item);
        			$counter++;
        			$meganav = "";
        
        			$title = html_entity_decode($title);
        			$slug = Helper::createSlug($title);
        
        			if($pagetitle && $title!=$pagetitle && $url!='#'){
        				$pagetitle = html_entity_decode($pagetitle);
        				$slug = Helper::createSlug($pagetitle);
        			}
        			$navSecWidth = getContent("section","display:detail","find:".$slug."-menu-content","show:__customfullscreenwidth__true","noecho","noedit");
        			$navSec1 = getContent("section","display:detail","find:".$slug."-menu-content","show:<div class='text'>__text__</div>","noecho","noedit");
        			$level2 = getContent("navigation","display:dropdown","find:$slug","toplevel:2","bottomlevel:3","id:$navId-$slug","class:","noecho");
        
        			$items = array_filter(array($navSec1,$level2));
        			$count = count($items);
        			
        			if (trim($navSecWidth)=="true") {
        				$meganav = $navSec1;
        				$mnav = $navSec1;
        			} else {
        
        				foreach ($items as $counter => $item) {
        					if($level2!=''){
        						$navclass = "hasnav";
        					}
        					//$cols = intval($counter + 1)*4;
        					//$class    = $count > 1 ? 'medium-'.$cols.' columns' : 'medium-12 columns';
        					$meganav .= "<div class='$class'>";
        					$meganav .= "<h2><a href='".$url."'>".$title."</a></h2>";
        					$meganav .= $item;
        					$meganav .= "</div>";
        					$mnav .= "<li><a href='".$url."'>".$title."</a>";
        					$mnav .= $item;
        					$mnav .= "</li>";
        				}
        			
        			}
        			$output .= "<div id='nav_".$slug."_menu' class='".$slug." column'>";
        			$output .= $meganav;
        			$output .= "</div>";
        		}
        
        		echo $output;
        		?>
        		
					</div>
					<div class="row align-center show-for-xlarge">
        		<div class="medium-6 column">
              <?php echo $pyvlink;?>
        		</div>
      		</div>
					
				<div id="mnav" class="row show-for-small hide-for-xlarge align-center">
  				<div class="row align-center">
          		<div class="small-12 columns">
                <?php echo $pyvlink;?>
          		</div>
        		</div>
        		
        		
				<ul class="vertical menu" data-accordion-menu data-multi-open="false">

         <?php 
        // Build Meganav 
        		$moutput = "";
        		$navigation = "";
        		$navId = $navId ? $navId : "nav";
        
        		$level1 = getContent( "navigation","display:dropdown","open:yes","toplevel:1","bottomlevel:1","id:nav","show:~|","show:__title__","show:~|","show:__pagetitle__","show:~|","show:__url__","show:~|","show:__id__","show:~|","show:__current__","show:~|","show:target=\"_blank\"__ifnewwindow__","noecho");
        		$level1 = explode('<ul id="nav">', $level1);
        		$level1 = explode('</li>', $level1[1]); array_pop($level1);
        		foreach ($level1 as $item) {
        
        			list($li,$title,$pagetitle,$url,$id,$current,$target) = explode("~|",$item);
        			$counter++;
        			$meganav = "";
        
        			$title = html_entity_decode($title);
        			$slug = Helper::createSlug($title);
        
        			if($pagetitle && $title!=$pagetitle && $url!='#'){
        				$pagetitle = html_entity_decode($pagetitle);
        				$slug = Helper::createSlug($pagetitle);
        			}
        			$navSecWidth = getContent("section","display:detail","find:".$slug."-menu-content","show:__customfullscreenwidth__true","noecho","noedit");
        			$navSec1 = getContent("section","display:detail","find:".$slug."-menu-content","show:<div class='text'>__text__</div>","noecho","noedit");
        			$level2 = getContent("navigation","display:dropdown","find:$slug","toplevel:2","bottomlevel:3","id:$navId-$slug","class:menu vertical nested","noecho");
        
        			$items = array_filter(array($navSec1,$level2));
        			$count = count($items);
        			
        			if (trim($navSecWidth)=="true") {
        				$meganav = $navSec1;
        				$mnav = $navSec1;
        			} else {
        
        				foreach ($items as $counter => $item) {
        					if($level2!=''){
        						$navclass = "hasnav";
        					}
        					//$cols = intval($counter + 1)*4;
        					//$class    = $count > 1 ? 'medium-'.$cols.' columns' : 'medium-12 columns';
        					//$meganav .= "<div class='".$class." blah'>";
        					$meganav .= "<a href='".$url."'>".$title."</a>";
        					$meganav .= $item;
        					//$meganav .= "</div>";
        					$mnav .= "<li><a href='".$url."'>".$title."</a>";
        					$mnav .= $item;
        					$mnav .= "</li>";
        				}
        			
        			}
        			$moutput .= "<li id='nav_".$slug."_menu' class='".$slug." column'>";
        			$moutput .= $meganav;
        			$moutput .= "</li>";
        		}
        
        		echo $moutput;
        		?>     
        		</ul>
        		
				  </div>
          <div class="row align-center show-for-small hide-for-xlarge">
        		<div class="small-12 column sm">
              <?php
                getContent(
          				'linklist',
          				'find:social-media',
          				'display:links',
          				'show:<a href="__url__" target="_blank"><i class="__description__"></i></a>'
          			);
              ?>
            </div>
          </div>
				</div>
				
			</div>
		</div>
		
		
		