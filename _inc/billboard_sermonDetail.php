<?php
	$serieshdr = getContent(
	"sermon",
	"display:list",
	"find_series:".$_GET['sermonslug'],
	"groupby:series",
	"group_show:__groupimage__",
	"noecho",
	"noedit"
	);
	$embedvideo = getContent(
	"sermon",
	"find:".$_GET['sermonslug'],
	"display:detail",
	"show:__videoembed__",
	"noecho",
	"noedit"
	);
?>
<div id='billboard' style="background-image:url(<?= $serieshdr ?>); background-size: cover;" class='static videohdr'>	
	<div class="overlay">	
		<div class='row fullWidth'>
			<div class='column'>
					<?php
						if ($embedvideo){
						getContent(
							"sermon",
							"find:".$_GET['sermonslug'],
							"display:detail",
							"show:<div id='messagevideo'>",
								"show:<div class='flex-video widescreen'>",
									"show:".$embedvideo,
								"show:</div>",
								"show:<div class='buttons'>",
									"show:<span class='left'>",
										"show:<a class='addthis_button_compact small white button'>Share</a>",
										"show:<a class='small white button' href='https://itunes.apple.com/us/podcast/northview-church-audio-podcast/id378824543?mt=2'>Subscribe to Podcast</a>",
									"show:</span>",
									"show:<span class='right'>",
										"show:__audioplayer linktext='Listen'__",
										"show:<a href='__notes__' class='small gray button' target='_blank'>Notes</a>",
										"show:<a href='__audio__' class='small gray button'>Download</a>",
									"show:</span>",
								"show:</div>",
							"show:</div>"
						);  						
						}else {
						getContent(
							"sermon",
							"find:".$_GET['sermonslug'],
							"display:detail",
							"show:<div id='messagevideo'>",
								"show:<div class='flex-video widescreen'>",
									"show:<video id=\"video-__slug__\" class=\"video-js vjs-default-skin vjs-16-9 vjs-big-play-centered\" controls preload=\"auto\" width=\"100\" height=\"450\"poster=\"__seriesimage__\" data-setup='{ \"aspectRatio\":\"16:9\" }'>",
									"show:<source src=\"__video__\" type='video/mp4'>",
										"show:<p class=\"vjs-no-js\">",
												"show:To view this video please enable JavaScript, and consider upgrading to a web browser that",
										"show:<a href=\"http://videojs.com/html5-video-support/\" target=\"_blank\">supports HTML5 video</a>",
										"show:</p>",
									"show:</video>",
								"show:</div>",
								//"show:<div class='buttons'>",
									//"show:<span class='left'>",
										//"show:<a class='addthis_button_compact small white button'>Share</a>",
										//"show:<a class='small white button' href='https://itunes.apple.com/us/podcast/northview-church-audio-podcast/id378824543?mt=2'>Subscribe to Podcast</a>",
									//"show:</span>",
									//"show:<span class='right'>",
										//"show:__audioplayer linktext='Listen'__",
										//"show:<a href='__notes__' class='small gray button' target='_blank'>Notes</a>",
										//"show:<a href='__audio__' class='small gray button'>Download</a>",
									//"show:</span>",
								//"show:</div>",
							"show:</div>"
						);  						
						}
					?>
			</div>
		</div>
	</div>
</div><!--end .rotator-->