    <div class="fh-toolbar">
      <div class="row align-justify">
        
        <div class="small-9 medium-5 column toolbar-l">
          <a href="/"><img id="logo" src="/_img/logo_mark.svg" alt="Kingsway Christian School" width="70" /></a>

          <div class="switcher">
            <a href="http://www.kingswaychurch.org" class="<?= $churchsite ?>">Church</a> / <a href="http://www.kingswayschool.org" class="<?= $schoolsite ?>">School</a>
          </div>
        </div>
        <div class="small-3 medium-5 large-4 column toolbar-r text-right">
          <nav>
            <?php
  			    	getContent(
  						"linklist",
  						"display:links",
  						"find:navigation-links",
  						"before_show:<ul class='right float-right'>",
  						"show:<li id='__slug___nav' class='hide-for-small-only'><a",
  						"show: href='__url__'",
  						"show:__ifnewwindow__target='_blank'",
  						"show: title='__name__'",
  						"show: class='__customlinkclass__'",
  						"show:>__name__",
  						"show:</a></li>",
  						'after_show:<li id="menu_nav" class="show-for-small"><a title="Menu" class="icon-menu menu-opener"></a></li>',
  						"after_show:</ul>"
    					);
    					?>
  					</nav>
          </div>
      </div>
    </div><!-- end .fh-toolbar -->