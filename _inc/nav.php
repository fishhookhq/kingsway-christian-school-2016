<?php
  $level1 = getContent( 
    "navigation",
    "display:dropdown",
    "open:yes",
    "toplevel:1",
    "bottomlevel:1",
    "id:nav",
    "show:~|",
    "show:__title__",
    "show:~|",
    "show:__pagetitle__",
    "show:~|",
    "show:__url__",
    "show:~|",
    "show:__id__",
    "show:~|",
    "show:__current__",
    "show:~|",
    "show:target=\"_blank\"__ifnewwindow__",
    "noecho");
    
    $level1 = explode('<ul id="nav">',$level1);
 		$level1 = explode('</li>', $level1[1]); array_pop($level1);
    
    foreach ($level1 as $item){
    list($li,$title,$pagetitle,$url,$id,$current,$target) = explode("~|",$item);
    
    $title = html_entity_decode($title);
		$slug = Helper::createSlug($title);
    $buttonnav .= "<button type='button' class='n-".$slug." large button menu-opener' data-equalizer-watch><span>".$title."</span></button>"; 
    }
?>

<div class="fh-nav">
  <div class="row align-center">      
    <nav data-equalizer>
     <?php echo $buttonnav; ?>
    </nav>
  </div>
</div>
