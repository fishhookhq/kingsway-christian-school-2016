<div id="upcoming-grid" class="">
  <?php
    $list_raw = getContent(
        "linklist",
        "display:links",
        "find:home-upcoming",
        "level1:__slug__",//0
        "level1:~~", //separator flag
        "level1:__name__",//1
        "level1:~~", //separator flag
        "level1:__description__",//2
        "level1:~~", //separator flag
        "level1:__imageurl width='780'__",//3
        "level1:~~", //separator flag
        "level1:__url__",//4
        "level1:|~", //separator flag
        "noecho"
      );
    $upcoming = explode("|~", $list_raw); 
    foreach ($upcoming as $key => $upcoming_fields){
      if($upcoming_fields){
        $item = explode("~~", $upcoming_fields);
        if($key===0){
          $soutput .= '<div class="small-12 medium-8 columns">';//column 1
          $soutput .= '<a href="'.$item[4].'"><div class="upimage feature" style="background-image:url('.$item[3].')">';
          $soutput .= '<div class="overlay">';
          $soutput .= '<h5>'.$item[1].'</h5>';
          $soutput .= '<p>'.$item[2].'</p>';
          $soutput .= '</div>';
          $soutput .= '</div></a>';
            
          $soutput .= '</div>';
            
          $soutput .= '<div class="small-12 medium-4 column">';//column 2

        }else{
               
          $soutput .= '<a class="uplink" href="'.$item[4].'"><div class="upimage" style="background-image:url('.$item[3].')">';
          $soutput .= '<div class="overlay">';
          $soutput .= '<h5>'.$item[1].'</h5>';
          $soutput .= '<p>'.$item[2].'</p>';
          $soutput .= '</div>';

          $soutput .= '</div></a>';
        }
      }
    }
    echo '<div class="row">';
    echo $soutput;  
    
    echo '</div>';

    echo '</div>';
  ?>
</div><!-- end #upcoming-grid -->