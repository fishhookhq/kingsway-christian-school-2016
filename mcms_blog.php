<?php require_once($_SERVER["DOCUMENT_ROOT"]."/_inc/config.php");
$meta = new pageMeta("blog", $MCMS_SITENAME);

//blog
$blogHome = getContent(
    "blog",
    "display:auto",
    "howmany:1",
    "before_show_postlist:/__blogslug__",
    "show_detail:/__blogslug__",
    "noecho"
);
//billboard
$billboard = getContent("media","display:detail","find:".$_GET['nav'],"label:header","show:__imageurl maxWidth='2560'__", "noecho" );
if($billboard){$bodyClass = 'hasbillboard'; $billboardClass = 'skew skew-light'; $billboardStyle = 'style="background-image: url('.$billboard.');"';}
$view = getContent("blog","display:auto","before_show_postlist:list","show_detail:detail","noecho","noedit");

?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/head.php"); ?>
    <?php 
      $custom_seo_title = getContent("page","find:".$_GET['nav'],"show:__customseotitle__","noecho","noedit", "nocache");
      $page_title = ($custom_seo_title != "" && $custom_seo_title != "__customseotitle__") ? $custom_seo_title : $meta->page_title.' | Kingsway Christian Church';
    ?>
    <title><?= $page_title ?></title>
    <meta name="description" content="<?= $meta->page_description ?>"/>
    <meta name="keywords" content="<?= $meta->page_keywords ?>"/>
    
    <meta property="og:description" content="<?= $meta->page_description ?>">
    <meta property="og:title" content="<?= $page_title ?>">
    
    <meta name="twitter:description" content="<?= $meta->page_description ?>">
    <meta name="twitter:title" content="<?= $page_title ?>">
    <meta property="og:image" content="<?= $billboard ?>">
    <meta name="twitter:image:src" content="<?= $billboard ?>">
  </head>
  
  <body id="blog" class="page" data-blog-home="<?php echo $blogHome; ?>">
    
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/header_subpage.php"); ?>

  <?php if ($view == "list") {
    getContent(
      "blog",
      "display:auto",
      "howmany:1",
      "order:recent",
      'before_show_postlist:<div class="fh-bottom-c slide home-blog medium">',
      
      "before_show_postlist: <div class='row align-center'>",
    	"before_show_postlist:   <div class='medium-8 columns text-center align-self-bottom'>",
      "before_show_postlist:<h1 class='caption'><span>__blogtitle__</span></h1>",
      "before_show_postlist: </div>",
      "before_show_postlist:</div>",
      
    	"after_show_postlist:</div>"
    );?>
    
<div id="filter_row" class="filters">
  <div class="row align-center">
    <div class="medium-11 columns">
      <div class="row align-center">
        <div class="small-11 medium-1 medium-text-right align-self-middle columns">
          <span>Sort by</span>
        </div>
        <div class="small-11 medium-4 columns">
            <select name="category" id="select-category" class='sorter'>
              <option value="">Category</option>
              <option value="">All</option>
              <? getContent(
                  "blog",
                  "name:$blogslug",
                  "display:list",
                  "groupby:category",
                  "group_show_postlist:<option value='__slug__'>__title__</option>"
              );
              ?>
          </select>
        </div>
        <div class="small-11 medium-4 columns">
          <select name="archive" id="select-archive" class='sorter'>
              <option value="">Archive</option>
              <option value="">All</option>
              <? getContent(
                  "blog",
                  "name:$blogslug",
                  "display:list",
                  "groupby:month",
                  "group_show_postlist:<option value='__slug__'>__title__</option>"
              );
              ?>
          </select>
        </div>
        <div class="small-11 medium-3 columns">
          <form action="/search-results/" method="get" id="searchForm3">
          <fieldset>
            <input type="text" id="search_term3" name="keywords" value="" class="" placeholder="search">
            <button type="submit" name="submit" id="module-search-submit" value="" class="icon-search"></button>

            <input type="hidden" name="show_results" value="blogs"> 
          </fieldset>
        </form>

        </div>
      </div>
    </div>
  </div>
</div>
<div id="page_content">
<div id="upcoming-grid" class="blog_posts">
  <div class="grid-sizer"></div>
    <div class="bloggrid" id="bloggrid">
  <?php
    $soutput = '';
    $column_widths = array('eight','four','four','twelve','four','eight','four','twelve');
    $list_raw = getContent(
        "blog",
        "display:auto",
        "order:recent",
        "howmany:8",
        "show_postlist:__blogpostslug__",
        "show_postlist:~~", //separator flag
        "show_postlist:__blogposttitle__",
        "show_postlist:~~", //separator flag
        "show_postlist:__preview__",
        "show_postlist:~~", //separator flag
        "show_postlist:__imageurl width='780'__",
        "show_postlist:~~", //separator flag
        "show_postlist:__blogtitlelink__",
        "show_postlist:~~", //separator flag
        "show_postlist:__category__",
        "show_postlist:|~", //separator flag
        "after_show_postlist:~|~__pagination__",
        "noecho"
      );
    list($bp,$pagination) = explode("~|~", $list_raw);
    $blogposts = explode("|~",trim($bp));
    
    foreach($blogposts as $key => $blogpost_fields){
      if($blogpost_fields){
        list($bp_slug,$bp_title,$bp_preview,$bp_image,$bp_url,$bp_ctgys) = explode("~~", trim($blogpost_fields,"~~"));
        $bp_class_array = explode(",", trim($bp_ctgys));
        $modulus8 = ($key % 8);
        $bp_classes = $column_widths[$modulus8];
        foreach($bp_class_array as $bpc) {
          $bp_classes .= ' '.Helper::createSlug($bpc);
        }
        
        if(!$bp_image){
          //defaulte blog image
          $bp_image = $billboard;
        }
        
          $soutput .= '    <div class="upimage '.$bp_classes.'">';
          $soutput .= '      <a href="'.$bp_url.'" style="background-image:url('.$bp_image.')" data-no-instant>';
          $soutput .= '       <h5>'.$bp_title.'</h5>';
          $soutput .= "       <span class='button light'>view blog post</span>";
          $soutput .= '      </a>';
          $soutput .= '    </div>';
          
      }
    }
   
    echo $soutput;
  ?>
  </div><!-- end #upcoming-grid -->	
  
</div> <!-- #page_content -->
<div class="row pagination-holder align-center">
    <div class="small-10 column">
  <?php
      getContent(
        "blog",
        "display:auto",
        "order:recent",
        "howmany:8",
        "after_show_postlist:__pagination__"
      );
	?>
    </div>
  </div>
<?php } //end list view
else {
    
  $get_post_data = getContent(
  	"blog",
  	"display:auto",
    "before_show_detail:__customdefaultblogimage__",

  	'show_detail:__blogslug__',
  	'show_detail:||',
  	'show_detail:__blogposttitle__',
  	'show_detail:||',
  	'show_detail:__blogtext__',
  	'show_detail:||',
  	"show_detail:__imageurl maxWidth='1600'__",
  	'show_detail:||',
  	'show_detail:__category__',
  	'show_detail:||',
  	"show_detail:__blogpostdate format='F j, Y'__",
  	'show_detail:||',
  	'show_detail:__blogauthor__',
  	'show_detail:||',
  	'show_detail:__blogauthorimage__',
  	'show_detail:||',
  	'show_detail:__blogauthorbio__',
  	'show_detail:||',
  	'show_detail:__tags__',
  	'show_detail:||',
  	'show_detail:__blogposttitleslug__',
  	'noecho',
  	'noedit'
  );
	$page_item_arr = explode('||',$get_post_data);

	list($blog_slug,$bp_title,$bp_text,$bp_image,$bp_ctgys,$bp_date,$bp_author,$bp_author_img,$bp_author_bio,$bp_tags,$bp_slug) = $page_item_arr;
  
  $page_content_grid = "medium-10 medium-centered";
  
  if(!$bp_image){
    //defaulte blog image
    $bp_image = $billboard;
  }
  
	echo "<div id='billboard' class='normal static bpheader'>";
  	echo "<div id=\"cycle-ss\" class='cycle-slideshow'";
  	echo " data-cycle-slides=\"> div.slide\"";
  	echo " data-cycle-auto-height=\"1280:550\"";
  	echo " data-cycle-speed=\"350\"";
  	echo " data-cycle-swipe=\"true\"";
  	echo " data-cycle-log=\"false\"";
  	echo " data-cycle-fx=\"fade\"";
  	echo " >";
    	echo "<div class='slide'";
    	echo " style=\"background-image: url('".$bp_image."');\"";
    	echo ">";
      	echo "<div class='overlay'>";
          echo "<div class='row align-bottom align-center'>";
          	echo "<div class='".$page_content_grid." column bpdetails'>";
            	echo "<h1>".$bp_title."</h1>";
            	echo " <div class='row'>";
//             	echo "   <div class='column shrink category'>".$bp_ctgys."</div>";
            	echo "   <div class='column shrink date'>".$bp_date."</div>";
            	echo "   <div class='column shrink author'>".$bp_author."</div>";
            	echo " </div>";
          	echo "</div>";
        	echo "</div>";
      	echo "</div>";
    	echo "</div>";
  	echo "</div>";
	echo "</div><!--end .rotator-->";
	echo "<a href='/".$blog_slug."/' class='button dark back'>back to blog</a>";

?>
<div id="page_content">  	
<!-- Page Content -->
<?php
	$html = str_get_html($bp_text);
	if($html){
		foreach($html->find('img.fullwidth') as $element) {
			$img_url = explode("?",$element->src);
			$parent_element = $element->parent();
			$prev_sibling = $element->prev_sibling();
			$next_sibling = $element->next_sibling();
				if ($img_url[1]){
				$img_query_string = explode("&",$img_url[1]);
				foreach($img_query_string as $qs) {
					$qs = str_replace("amp;", "", $qs);
					$qs = explode("=", $qs);
					$$qs[0]=$qs[1];
				}
			$img_alt = $element->alt;
			$img_slug = preg_match("/([^.]+)-[0-9]+-[0-9]+-[0-9]+-[0-9]+/i",$fileName,$match);
			$orig_img_url = getContent(
				"media",
				"display:detail",
				"find:".$match[1],
				"show:__imageurl maxWidth='1600'__",
				"noecho"
			);
			} else {
				$orig_img_url = $element->src;
			}
			$section_caption = "";
			if ($element->alt) {
				$section_caption = "\n\t\t\t<div class=\"caption text-center\"><div class=\"box\"><h1>" .$element->alt. "</h1></div></div>";
			}
			$parent_element->outertext = $prev_sibling."\n\t</div>\n</div>\n<div class=\"separator fullwidth\" style=\"background-image: url('".$orig_img_url."')\">\n\t<div class=\"row\">\n\t\t<div class=\"".$page_content_grid." columns\">".$section_caption."\n\t\t</div>\n\t</div>\n</div>\n<div class=\"row\">\n\t<div class=\"".$page_content_grid." columns\">".$next_sibling;
			$element->alt = null;
  	}
  	
  	foreach($html->find('#featured-events-grid') as $element) {
			$parent_element = $element->parent();
			$prev_sibling = $element->prev_sibling();
			$next_sibling = $element->next_sibling();
			$parent_element->outertext = $prev_sibling."\n\t</div>\n</div>\n".$element."\n<div class=\"row\">\n\t<div class=\"".$page_content_grid." columns\">".$next_sibling;
    }
    
		echo "\n<div class='row align-center'>";
  	echo "\n\t<div class='".$page_content_grid." columns'>";
	 	echo $html;
	 	$html->clear();
    echo "\n\t</div>";
    echo "\n</div>";
	}
	  echo "\n<div class='row align-center'><div class='medium-10 columns'>";
		echo "\n<div class='row blogsuffix'>";
  	echo "\n\t<div class='columns tags'>";
  	  echo $bp_tags;
    echo "\n\t</div>";
  	echo "\n\t<div class='columns text-right'>";
  	  echo "<a class='button dark stbutton addthis_button' addthis:url=\"http://www.kingswaychurch.org/the-kingsway-blog/".$bp_slug."\" addthis:title=\"".$bp_title.": From The Kingsway Blog\" title='Share'>share</a>";
  	  	  
    echo "\n\t</div>";
    echo "\n</div>";
    
    echo "\n</div>";
?>	
</div> <!-- #page_content -->
<div class="content-section bpauthor blue">
  <div class="content-section-inner">
	  <div class="container row align-center">
   	<?php
		  if($bp_author){

        echo '<div class="text column text-center">';
          echo '<h6>Author</h6>';
          echo '<h2 class="title">'.$bp_author.'</h2>';
          //echo $bp_author_bio;
        echo '</div>';
      }
    ?>
    </div>
  </div>
</div>
<?php } //end detail view ?>	

</div> <!-- #page_content -->

	<!-- Page Footer -->
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/footer.php"); ?>
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/scripts.php"); ?>
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/analytics.php"); ?>

<script src="/_js/min/isotope.pkgd.min.js"></script>
  <script src="/_js/min/imagesloaded.pkgd.min.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">
$( document ).ready(function() {

  $(function(){

    // init Isotope
    var $grid = $('#upcoming-grid').imagesLoaded( function(){
      $grid.isotope({
        // options
        itemSelector: '.upimage',
        percentPosition: true,
        masonry: {
          // use outer width of grid-sizer for columnWidth
          columnWidth: '.grid-sizer'
        }
      });
    });
          
    var blogHome = $('body').data('blog-home');
    
    //sorter
    $('.sorter').change(function (event) {
        event.preventDefault();
        window.location = blogHome + "/" + this.name + "/" + this.value;
    });

		function get_blog_data() {
			$('#upcoming-grid').html('<div class="loader">Loading...</div>');
/*
      $target = $('#blog_filters .icon-list');
      // Make sure panel is not already active            
      if (!$target.parents('li').hasClass('active')) {
          // Trigger a click on item to change panel
          $target.trigger('click');
      }
			var_form_data += "blogname=<?=$blogname?>&";
			var_form_data += "category=<?=$current_ctgy?>&";
*/
			var_form_data += $('#blog_search').serialize();

/*
			function ReinitializeAddThis(){
				if (window.addthis){
					window.addthis.ost = 0;
					window.addthis.ready();
				}
			}					
			$.ajax({
				url: "http://s7.addthis.com/js/300/addthis_widget.js#ra-55e9892decbbf6a6",
				dataType: 'script',
				success: function(){
					ReinitializeAddThis();	
				}
			});
*/

		$.get('/_js/ajax/ajax-blog.php', var_form_data, function(response) {
				$('#upcoming-grid').html(response).css({
					height:'auto',
					background:'none'
				});			
			});
		}

		$('#blog_search input').change(function(){
			var_form_data = "changed="+$(this).attr("id")+"&";
			get_blog_data();
		});
		$('#blog_search').submit(function(e){
			var_form_data = "changed="+$(this).attr("id")+"&";			
			e.preventDefault();
			get_blog_data();
		});		
		
  });
    });

</script>
  </body>
</html>
