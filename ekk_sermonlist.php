<?php require_once($_SERVER["DOCUMENT_ROOT"]."/_inc/config.php");
$meta = new pageMeta("index", $MCMS_SITENAME);

//latest sermon from current series
$fbimage = getContent("sermon","display:list","features","order:recent","howmany:1","show:__seriesimage width='1280' height='850'__","noecho");
$billboard = getContent("sermon","display:list","features","order:recent","howmany:1","show:style=\"background-image: url(__seriesimage width='1280' height='850'__);\"","noecho");
?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/head.php"); ?>
    <?php 
      $custom_seo_title = getContent("page","find:".$_GET['nav'],"show:__customseotitle__","noecho","noedit", "nocache");
      $page_title = ($custom_seo_title != "" && $custom_seo_title != "__customseotitle__") ? $custom_seo_title : $meta->page_title.' | REPLACE WITH CHURCH NAME';
    ?>
    <title><?= $page_title ?></title>
    <meta name="description" content="<?= $meta->page_description ?>"/>
    <meta name="keywords" content="<?= $meta->page_keywords ?>"/>
    
    <meta property="og:description" content="<?= $meta->page_description ?>">
    <meta property="og:title" content="<?= $page_title ?>">
    
    <meta name="twitter:description" content="<?= $meta->page_description ?>">
    <meta name="twitter:title" content="<?= $page_title ?>">
    <meta property="og:image" content="<?= $billboard ?>">
    <meta name="twitter:image:src" content="<?= $billboard ?>">
  </head>
  
  <body id="sermons" class="page">
    
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/header_subpage.php"); ?>
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/billboard_sermons.php"); ?> 

  
<div id="page_content">
  	
<!-- Page Content -->
<div id="sermon-series-grid">
		<div class="row small-up-1 medium-up-2 sermons">
			
			<?php
        $series = getContent(
          "sermon",
					"display:list",
					"order:recent",
					"howmany:12",
					"page:".$_GET['page'],
					"groupby:series",
					"group_show:__slug__",
					"group_show:~~",
					"group_show:<img src=\"__groupimage width='780' height='425'__\" />",
					"group_show:~~",
					"group_show:__description__",
					"group_show:||",
					"after_show:!!__pagination__",
					"noecho","noedit"
	            );
	//              echo $series;
	              list($series_list,$series_pagination) = explode("!!", $series);              
	              $series_list = explode("||", trim($series_list,"||"));
	                            
	              foreach($series_list as $series_info) {
		              list($sSlug,$sImage,$sDescription) = explode("~~", $series_info);
	
			            $sIcon = getContent(
			            	"sermon",
      							"display:list",
      							"order:oldest",
      							"find_series:".$sSlug,
      							"howmany:1",
      							"show:<div class='vc sermon-icon'>__image maxWidth='780' maxHeight='425'__</div>",
      							"noecho","noedit"
      						);	
					echo"<div class='column sermon'>";
					echo"<div class='sermon-holder'>";
	                echo"<a href='/series/".$sSlug."' class='image-link showdetail'>";
	                echo $sIcon;
	                echo"</a>";
	                echo"<div class='sermondetail'>";
		                echo "<div class='content_holder'><div class='row align-center'><div class='medium-8 column'>";
		                echo "<a href='' class='closer'><p><span class='icon-x'></span></p></a>";
		                echo "<div class='sermon-description'>".$sDescription."</div>";
      								echo"<div class='sermons-list'>";
      	                
      	                $sermons = getContent(
          	            	"sermon",
                					"display:list",
                					"order:recent",
                					"find_series:".$sSlug,
                					"show:<p class='sermon-date'>__date format='M'__ <span>__dateTwo format='j'__</span></p>",
                					"show:~~",
                					"show:<h1 class='sermon-title'>__title__</h1>",
                					"show:~~",
                					"show:<a href='/message/__slug__/' class='button dark'>View Message</a>",
                					"show:~~",
                					"show:||",
                					"noecho","noedit"
            	            );
      	             //echo $sermons;
      	             $sermon_list = explode("||", trim($sermons,"||"));
      	              foreach($sermon_list as $sermon_info) {
      		              list($sDate,$sTitle,$sURL) = explode("~~", $sermon_info);
      		        
      	                  
                        echo"<div class='individual-sermon'>";
                          echo"<div class='row align-center align-middle'>";
        								
        									echo"<div class='medium-2 column'>";
        										echo $sDate;
        									echo"</div>";
        									
        									echo"<div class='column'>";
        										echo $sTitle;
        									echo"</div>";
  /*
        									echo"<div class='column'>";
        										echo $sSeriesTitle;
        									echo"</div>";
  */
        									echo"<div class='medium-3 columns text-right'>";
        										echo $sURL;
        									echo"</div>";
        									
                         echo"</div>";
        	             echo"</div>";
      	              }
	              			
	              		echo"</div>";
	              	echo"</div></div>";
	              echo"</div>";
	            echo"</div>";
            echo "</div></div>";	              
	        }	
          echo"</div>";
          echo $series_pagination;				
			?>
		
	</div>

</div> <!-- #page_content -->    
	
  <!-- Page Sections -->
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/sections.php"); ?>
	    
	<!-- Page Footer -->
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/footer.php"); ?>
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/scripts.php"); ?>
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/analytics.php"); ?>
<script type="text/javascript">
$(document).ready(function(){
  var seriesGrid = $("#sermon-series-grid"),
      fullWidth = $(seriesGrid).width(),
      rowPosition = $(seriesGrid).find(".sermons").position();
      console.log(rowPosition);
	$('.sermon').each(function() {
		var offsetleft = $(this).position().left*-1;
			console.log(offsetleft);
			positionLeft = offsetleft - rowPosition.left -10;
			console.log(positionLeft);
		$(this).find('.sermondetail').
			css({"width":fullWidth,"left":positionLeft});
	});
 
	$('a.showdetail').click(function(e) {
		e.preventDefault();
		var currentsermon = $(this).closest('div.sermon');
				
		var currentsermonid = currentsermon.attr("id")
		
		if (currentsermon.hasClass('open')) { //close current sermon, full opacity
			currentsermon.find('.sermondetail > .content_holder').slideUp();
			currentsermon.find('.sermondetail').slideUp();
			currentsermon.removeClass('open');
/*
			$(this).closest('.sermons').find('div.sermondetail').each(function() {
					$(this).css("opacity",1);
					$(this).removeClass("open");
			});
*/
		} else { //open current sermon, dim all others
			//currentsermon.css("opacity",1);
			$('.sermondetail > .content_holder').hide();
			$('.sermondetail').hide();
			$('.sermon').removeClass('open');
			currentsermon.find('.sermondetail').show();
			currentsermon.find('.sermondetail > .content_holder').slideDown();
			currentsermon.addClass('open');
/*
			$(this).closest('.sermons').find('div.sermon').each(function() {
				if ($(this).attr("id") != currentsermonid) {
					$(this).css("opacity",".6");
					$(this).removeClass("open");
					$(this).find('.sermondetail').hide();
				}
			});
			$(this).closest('.sermons').find('div.sermon').each(function() {
				if ($(this).attr("id") == currentsermonid) {
					$(this).parent().addClass('open');

				}
			});
*/
		}
		
	});	
});
</script>
  </body>
</html>
