<?php require_once($_SERVER["DOCUMENT_ROOT"]."/_inc/config.php");
$meta = new pageMeta("index", $MCMS_SITENAME);

//latest sermon from current series
$fbimage = getContent("sermon","display:list","features","order:recent","howmany:1","show:__seriesimage width='1280' height='850'__","noecho");
$billboard = getContent("sermon","display:list","features","order:recent","howmany:1","show:style=\"background-image: url(__seriesimage width='1280' height='850'__);\"","noecho");
?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/head.php"); ?>
    <?php 
      $custom_seo_title = getContent("page","find:".$_GET['nav'],"show:__customseotitle__","noecho","noedit", "nocache");
      $page_title = ($custom_seo_title != "" && $custom_seo_title != "__customseotitle__") ? $custom_seo_title : $meta->page_title.' | Kingsway Christian Church';
    ?>
    <title><?= $page_title ?></title>
    <meta name="description" content="<?= $meta->page_description ?>"/>
    <meta name="keywords" content="<?= $meta->page_keywords ?>"/>
    
    <meta property="og:description" content="<?= $meta->page_description ?>">
    <meta property="og:title" content="<?= $page_title ?>">
    
    <meta name="twitter:description" content="<?= $meta->page_description ?>">
    <meta name="twitter:title" content="<?= $page_title ?>">
    <meta property="og:image" content="<?= $billboard ?>">
    <meta name="twitter:image:src" content="<?= $billboard ?>">
    
    <meta name="twitter:card" content="summary">
    <meta name="twitter:url" content="<? $_GET['nav'] ?>">
    <meta name="twitter:title" content="<?= $page_title ?>">
    <meta name="twitter:description" content="<?= $meta->page_description ?>">
    <meta name="twitter:image" content="http://kingswaychurch.org/_img/logo.svg">
  </head>
  
  <body id="sermons" class="page">
    
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/header_subpage.php"); ?>
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/billboard_sermonDetail.php"); ?> 

  
<div id="page_content">
<a href='/resources/messages/' class='button dark backbtn'>Back to All Messages</a>  	
<!-- Page Content -->
<div id="sermon-summary" class="row align-center">
		<div class="medium-10 columns text-center">
			<?php
				
				$seriesTitle = 
					getContent (
					"sermon",
					"find:".$_GET['sermonslug'],
					"display:detail",
					"show:__seriesslug__",
					"noecho","noedit"
					);
				
				$sermonTitle =
					getContent (
					"sermon",
					"find:".$_GET['sermonslug'],
					"display:detail",
					"show:__slug__",
					"noecho","noedit"
					);	
				
				if ($seriesTitle == $sermonTitle) {
				
				getContent(
					"sermon",
					"find:".$_GET['sermonslug'],
					"display:detail",
					"show:<div class='row align-center'>",
					"show:<div class='medium-8 columns'>",
					"show:<h1 class='sermon-title'>__title__</h1>",
					//"show:<h2 class='series-title'>__series__</h2>",
					"show:</div>",
					"show:<div class='large-4 columns'>",
					"show:<a href='/resources/messages/' class='button dark'>Back to Series</a>",
					"show:</div>",
					"show:<div class='row'>",
					"show:<div class='large-12 columns'>",
					"show:<p>__text__</p>",
					"show:</div>",
					"show:</div>",
					"show:</div>"	
				);
				
				} else {
					getContent(
						"sermon",
						"find:".$_GET['sermonslug'],
						"display:detail",
						"show:<h1 class='sermon-title'>__title__</h1>",
						"show:<h2 class='series-title'>__series__</h2>",
						"show:  <div class='row sermon_meta align-center'>",
            "show:    <div class='column shrink date'>__date format='F j, Y'__</div>",
            "show:    <div class='column shrink speaker'>__preacher__</div>",
            "show:    <div class='column shrink passage'>__passage__</div>",

            "show:  </div>",
            "show:  <div class='row sermon-media align-center align-middle'>",
            "show:    <div class='column shrink audio'><a title='__title__' class='' data-audio='__audiourl__' target='_blank' href='#'><span class='icon-video'></span></a></div>",
            "show:    <div class='column shrink audio'><a data-reveal-id='sermonAudioModal' title='__title__' class='' data-audio='__audiourl__' target='_blank' href='#'><span class='icon-audio'></span></a></div>",
//				"show:<li class='download'><a class='' href='__audio__' target='_blank' title='Download'><span class='icon-download'></span>Download Sermon Audio</a></li>",
					"show:    <div class='column shrink notes'><a class='' href='__notes__' target='_blank' title='Manuscript'><span class='icon-notes'></span></a></div>",
          "show:    <div class='column shrink share'><a class=' addthis_button_compact' addthis:url=\"http://www.kingswaychurch.org/sermon/__slug__/\" addthis:title=\"__title__: Sermon from ".$MCMS_SITENAME."\" title='Share'><span class='icon-share'></span></a></div>",
          "show:  </div>",


						"show:<p>__text__</p>"
					);
				}
			
			?>
		</div>
	</div>
	<!-- END Sermon Summary -->	
	
	
	<!-- Related Sermons -->
	<?php
	$sermon_tags = getContent(
		"sermon",
		"find:".$_GET['sermonslug'],
		"display:list",
		"show:__tags__",
		"noecho",
		"noedit"
	);
	

  //echo $b_tags;
  if ($seriesTitle) $get_related_sermons = getContent(
  	"sermon",
  	"display:list",
  	"howmany:4",
  	"find_series:".$seriesTitle,
  	"show:<p class='sermon-date'>__date format='M'__ <span>__dateTwo format='j'__</span></p>",  // 0
  	'show:||',
  	"show:<h1 class='sermon-title'>__title__</h1>",   // 1
  	'show:||',
  	"show:__slug__",   // 2
  	'show:||',
  	"show:__tags__",   // 3
  	'show:||',
  	'show:~|~|~',
  	'after_show:~!~',
  	"after_show:__totalpossible__",   // 0
  	'noecho'
  );
  
  $sermon_data_arr = explode("~!~", $get_related_sermons);
  $sermon_data_arr = array_filter($sermon_data_arr);
  list($get_related_sermons,$total_related_sermons) = $sermon_data_arr;

if($total_related_sermons > 1) {
  
  $sermon_data_arr = explode("~|~|~", $get_related_sermons);
  $sermon_data_arr = array_filter($sermon_data_arr);

	$sermon_output .= "<div id='related-messages'>";
	$sermon_output .= "<div class='row align-center'>";
	$sermon_output .= "<div class='medium-11 columns text-center'>";
	$sermon_output .= "<h1>Related Messages</h1>";
	$sermon_output .= "</div>";
	$sermon_output .= "</div>";

  foreach($sermon_data_arr as $s_ctr => $sermon_item){
    
  	$sermon_item_arr = explode('||',$sermon_item);
  
  	list($s_displaydate, $s_sermontitle, $s_viewbutton, $s_categories) = $sermon_item_arr;

    //echo $ctr;
    //echo $r_blogtitlelink;
    //echo $b_blogtitlelink;
  	
  	if (($_GET['sermonslug'] != $s_viewbutton) && ($s_ctr < 3)) {
      $sermon_output .= "<div class='row align-center'>";
      $sermon_output .= "<div class='medium-11 columns'>";

      $sermon_output .= "<div class='sermons-list'>";

    	$sermon_output .= "<div class='individual-sermon'>";
    	$sermon_output .= "<div class='row align-center align-middle'>";
    	$sermon_output .= "<div class='medium-2 column'>";
    	$sermon_output .= $s_displaydate;
    	$sermon_output .= "</div>";

    	$sermon_output .= "<div class='column'>";
    	$sermon_output .= $s_sermontitle;
    	$sermon_output .= "</div>";
    	
    	$sermon_output .= "<div class='medium-3 columns text-right'>";
    	$sermon_output .= "<a href='/message/".$s_viewbutton."/' class='button dark'>View Message</a>";
    	$sermon_output .= "</div>";
    	
    	$sermon_output .= "</div></div></div></div></div>";
  	
  	}
  
  }

	$sermon_output .= "</div>";


}

echo $sermon_output;

?>
	
<!-- End Page Content -->
	</div>

	    
	<!-- Page Footer -->
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/footer.php"); ?>
  <?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/scripts.php"); ?>
	<?php include($_SERVER["DOCUMENT_ROOT"]."/_inc/analytics.php"); ?>
<script type="text/javascript">
$(document).ready(function(){
  var seriesGrid = $("#sermon-series-grid"),
      fullWidth = $(seriesGrid).width(),
      rowPosition = $(seriesGrid).find(".sermons").position();
      console.log(rowPosition);
	$('.sermon').each(function() {
		var offsetleft = $(this).position().left*-1;
			console.log(offsetleft);
			positionLeft = offsetleft - rowPosition.left -10;
			console.log(positionLeft);
		$(this).find('.sermondetail').
			css({"width":fullWidth,"left":positionLeft});
	});
 
	$('a.showdetail').click(function(e) {
		e.preventDefault();
		var currentsermon = $(this).closest('div.sermon');
				
		var currentsermonid = currentsermon.attr("id")
		
		if (currentsermon.hasClass('open')) { //close current sermon, full opacity
			currentsermon.find('.sermondetail > .content_holder').slideUp();
			currentsermon.find('.sermondetail').slideUp();
			currentsermon.removeClass('open');
/*
			$(this).closest('.sermons').find('div.sermondetail').each(function() {
					$(this).css("opacity",1);
					$(this).removeClass("open");
			});
*/
		} else { //open current sermon, dim all others
			//currentsermon.css("opacity",1);
			$('.sermondetail > .content_holder').hide();
			$('.sermondetail').hide();
			$('.sermon').removeClass('open');
			currentsermon.find('.sermondetail').show();
			currentsermon.find('.sermondetail > .content_holder').slideDown();
			currentsermon.addClass('open');
/*
			$(this).closest('.sermons').find('div.sermon').each(function() {
				if ($(this).attr("id") != currentsermonid) {
					$(this).css("opacity",".6");
					$(this).removeClass("open");
					$(this).find('.sermondetail').hide();
				}
			});
			$(this).closest('.sermons').find('div.sermon').each(function() {
				if ($(this).attr("id") == currentsermonid) {
					$(this).parent().addClass('open');

				}
			});
*/
		}
		
	});	
});
</script>
  </body>
</html>
